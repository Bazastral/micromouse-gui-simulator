

Note that this project is under development, so there are many gaps both in 
documentation and sometimes in code.

# Micromouse gui simulator

Tool for developing micromouse project. Provides real-world simulator.


# Building

You need a Qt and all dependencies. Installing it for me is a pain in the ass.
Some random notes I have collected last time I wanted to install Qt locally on Unbuntu Jellyfix 22.04:
```
export ARG QT_VERSION=6.5.0
export ARG QT_PATH=${HOME}/Qt
aqt install-qt -O "$QT_PATH" linux desktop "$QT_VERSION" gcc_64
apt-get install libxcb-cursor-dev
```

After cloning whole repo, remmber to clone also submodules:
` git submodule update --init --recursive `
Create build folder and run in it:
` qmake6 ../mm/mm.pro  && make all && ./mm_test/mm_test`

# API

See communication.md

# Notes

- All values in SI units:
  - (exception) dimensions in millimeters
  - time in seconds
  - angles in radians


- Maze dimensions
  - Maze width and height is equal to
  MAZE_N*MAZE_DIM_SQUARE + MAZE_DIM_WALL = 16*18[cm] + 1.2[cm] = 289.2[cm]
  - coordinate system start at the South-West corner of the South-West stick.



# useful links
https://github.com/rm5248/qt-qtest-project-setup/

https://github.com/ComputationalPhysics/qtcreator-project-structure/