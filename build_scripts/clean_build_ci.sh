#!/bin/bash
SCRIPT_PATH=`dirname "$(readlink -f "$0")"`

# path must be relative, as it is in docker, which has different paths
${SCRIPT_PATH}/../builder/docker.sh ./build_scripts/clean_build.sh
