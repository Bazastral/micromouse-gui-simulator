#!/bin/bash

# exit when error occurs
set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`
BUILD_DIR="${BUILD_DIR:=${SCRIPT_PATH}/../build}"

cd ${SCRIPT_PATH}/..
mkdir -p ${BUILD_DIR} && cd ${BUILD_DIR}
qmake ../mm/mm.pro
make -j 1
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:./mm_sim/
./mm_test/mm_test

echo "${0##*/} DONE!"
