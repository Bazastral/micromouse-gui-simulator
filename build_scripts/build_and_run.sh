#!/bin/bash

# exit when error occurs
set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`
BUILD_DIR="${BUILD_DIR:=${SCRIPT_PATH}/../build}"

${SCRIPT_PATH}/build.sh
# run app with given parameters
${BUILD_DIR}/mm_sim_runner/mm_sim_runner $@


echo "${0##*/} DONE!"
