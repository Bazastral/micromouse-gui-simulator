

## useful scripts
Hope that names are descriptive.

### run in docker
| script                      | what it does | comment |
| -                           | - | - |
| build.sh                    | incrementally build | - |
| build_and_run.sh            | incrementally build and run application | useful for developing, params are forwarded |
| clean_build.sh              | clean build | you should start here |
| clean_build_and_test.sh     |clean build & test it| - |



### run from OS
| script                      | what it does | comment |
| -                           | - | - |
| clean_build_and_test_ci.sh  | clean build & make tests | this command is run by CI machine |
| clean_build_ci.sh           | clean build in docker |-|
| run_headless_ci.sh          | builds simulator and runs it in detatched docker | use it for developing mouse program. usage: ./run_headless_ci.sh --port 60123|
| stop_headless_ci.sh         | stops detached docker | remember to call it when you are done|

