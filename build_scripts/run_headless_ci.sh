#!/bin/bash
SCRIPT_PATH=`dirname "$(readlink -f "$0")"`


export DOCKER_OTHER_COMMANDS=--detach
${SCRIPT_PATH}/../builder/docker.sh ./build_scripts/run_headless.sh

# !!! remember to stop the docker with stop_headless_ci.sh script !!! #
