#!/bin/bash

# exit when error occurs
set -ex

SCRIPT_PATH=`dirname "$(readlink -f "$0")"`
BUILD_DIR="${BUILD_DIR:=${SCRIPT_PATH}/../build}"

rm -rf ${BUILD_DIR}
${SCRIPT_PATH}/build.sh

echo "${0##*/} DONE!"
