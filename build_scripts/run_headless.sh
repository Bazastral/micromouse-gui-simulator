#!/bin/bash
SCRIPT_PATH=`dirname "$(readlink -f "$0")"`

export DISPLAY=:95
Xvfb :95 -screen 0 1280x1024x24 -ac -nolisten tcp -nolisten unix &
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${SCRIPT_PATH}/../build/mm_sim/
${SCRIPT_PATH}/../build/mm_sim_runner/mm_sim_runner --platform offscreen
