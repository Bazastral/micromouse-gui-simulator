# Communication overview

Micromouse-gui-simulator is a server. You can communicate with it using JSON frames. The communication is synchronous. Client connects to the server, sends a JSON and waits for the response. Maybe JSON format over network will be a buttleneck taking into account the speed of the simulation, but when this time will occurr, I will come up with new idea. 
For now the communication shoud be:
- easy to understand
- easy to implement
- easy to debug

## Must

Each JSON in communication has a field "type".
Server always responds. Always has the field "error" which is equal to "success". If some errors occured, this value will have the description of what went wrong.

## SET

Must: "type" = "set"

And then you can set variables:

| param | value | example |
| ------ | ------ | --------|
| map_name | name of map* | {"param":"map_name","type":"set","value":"classic/a.txt"}
| path_plan | array of JSON points | {"param":"path_plan","type":"set","value":[{"x":0,"y":0},{"x":1,"y":0.5},{"x":2,"y":0.5},{"x":1,"y":1.5}]}
| state | JSON object with optional fields "x", "y", "t", "w", "v" | {"param":"state","type":"set","value":{"t":0,"v":0.1,"w":0.2,"x":1,"y":2}}
|next will come... 

\* see https://github.com/micromouseonline/mazefiles

## GET
Must: "type" = "get"

And then you can choose variables:

| param | value | example |
| ------ | ------ | --------|
|next will come... 



# notes

useful tool:
https://dillinger.io/
