
#include <QTest>
#include <QPoint>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <vector>

#include "test_helper.h"
#include "utils_test.h"


double const ALLOWED_ERROR = 0.0001;

UtilsTester::UtilsTester(QObject *parent) : QObject(parent)
{
    srand(time(NULL));
}
UtilsTester::~UtilsTester()
{
}

void UtilsTester::coordsLocalToGlobal_When_LocalIsGlobal_Then_ReturnTheSamePosition(void)
{
    Position localSystemOffset(0, 0, 0);
    Position coordsInLocal(rand(), rand(), rand());

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result, coordsInLocal);
}

void UtilsTester::coordsLocalToGlobal_When_LocalIsShiftedByX_Then_ReturnShiftedByX(void)
{
    double x_shift = rand();
    Position localSystemOffset(x_shift, 0, 0);
    Position coordsInLocal(rand(), rand(), rand());

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result.x(), coordsInLocal.x() + x_shift);
    QCOMPARE(result.y(), coordsInLocal.y());
    QCOMPARE(result.t(), coordsInLocal.t());
}

void UtilsTester::coordsLocalToGlobal_When_LocalIsShiftedByY_Then_ReturnShiftedByY(void)
{
    double Y_shift = rand();
    Position localSystemOffset(0, Y_shift, 0);
    Position coordsInLocal(rand(), rand(), rand());

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result.x(), coordsInLocal.x());
    QCOMPARE(result.y(), coordsInLocal.y() + Y_shift);
    QCOMPARE(result.t(), coordsInLocal.t());
}

void UtilsTester::coordsLocalToGlobal_Given_OnOriginInLocal_When_LocalIsRotated_Then_ReturnRotated(void)
{
    double rotation = rand();
    Position localSystemOffset(0, 0, rotation);
    Position coordsInLocal(0, 0, 0);

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result.x(), coordsInLocal.x());
    QCOMPARE(result.y(), coordsInLocal.y());
    QCOMPARE(result.t(), Utils::normalizeRadians(coordsInLocal.t() + rotation));
}

void UtilsTester::coordsLocalToGlobal_Given_XchangedInLocal_When_LocalRotatedBy45degs_Then_ReturnRotated(void)
{
    double rotation = Utils::degreesToRadians(45);
    double x_shift_in_local = 1;
    Position localSystemOffset(0, 0, rotation);
    Position coordsInLocal(x_shift_in_local, 0, 0);

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result.x(), sqrt(2)/2);
    QCOMPARE(result.y(), sqrt(2)/2);
    QCOMPARE(result.t(), rotation);
}

void UtilsTester::coordsLocalToGlobal_Given_YchangedInLocal_When_LocalRotatedBy45degs_Then_ReturnRotated(void)
{
    double rotation = Utils::degreesToRadians(45);
    double y_shift_in_local = 1;
    Position localSystemOffset(0, 0, rotation);
    Position coordsInLocal(0, y_shift_in_local, 0);

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE(result.x(), -sqrt(2)/2);
    QCOMPARE(result.y(), sqrt(2)/2);
    QCOMPARE(result.t(), rotation);
}

void UtilsTester::coordsLocalToGlobal_Given_XandYchangedInLocal_When_LocalRotatedBy90degs_Then_ReturnRotated(void)
{
    double rotation = Utils::degreesToRadians(90);
    Position localSystemOffset(0, 0, rotation);
    Position coordsInLocal(1, -2, 0);

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE_DOUBLE(2, result.x(), ALLOWED_ERROR);
    QCOMPARE_DOUBLE(1, result.y(), ALLOWED_ERROR);
    QCOMPARE_DOUBLE(rotation, result.t(), ALLOWED_ERROR);
}

void UtilsTester::coordsLocalToGlobal_Given_XandYchangedInLocal_When_LocalRotatedBy90degsAndShiftedXandY_Then_Calcultate(void)
{
    double rotation = Utils::degreesToRadians(90);
    Position localSystemOffset(10, 20, rotation);
    Position coordsInLocal(-2, -3, 0);

    Position result = Utils::coordsLocalToGlobal(localSystemOffset, coordsInLocal);

    QCOMPARE_DOUBLE(3+10, result.x(), ALLOWED_ERROR);
    QCOMPARE_DOUBLE(-2+20, result.y(), ALLOWED_ERROR);
    QCOMPARE_DOUBLE(rotation, result.t(), ALLOWED_ERROR);
}