
#include <QTest>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "encoder_test.h"
#include "encoder.h"


double deg2rad(double deg)
{
    return M_PI*deg/180;
}


EncoderTester::EncoderTester(QObject *parent) : QObject(parent)
{

}

void EncoderTester::When_NoMove_Then_NoEncoderTicks(void)
{
    Encoder enc;
    enc.update(0);
    QCOMPARE(enc.get_ticks(), 0);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved90Deg_Then_OneTick(void)
{
    Encoder enc(4);
    enc.update(M_PI_2);
    QCOMPARE(enc.get_ticks(), 1);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved89Deg_Then_NoTick(void)
{
    Encoder enc(4);
    enc.update(M_PI_2 - 0.001);
    QCOMPARE(enc.get_ticks(), 0);
}

void EncoderTester::Given_8TicksPerRotation_When_Moved90Deg_Then_TwoTicks(void)
{
    Encoder enc(8);
    enc.update(M_PI_2);
    QCOMPARE(enc.get_ticks(), 2);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved90DegBackwards_Then_MinusOneTick(void)
{
    Encoder enc(4);
    enc.update(-M_PI_2);
    QCOMPARE(enc.get_ticks(), -1);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved360Deg_Then_4Ticks(void)
{
    Encoder enc(4);
    enc.update(2*M_PI);
    QCOMPARE(enc.get_ticks(), 4);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved90DegAnd90Deg_Then_TwoTicks(void)
{
    Encoder enc(4);
    enc.update(M_PI_2);
    enc.update(M_PI_2);
    QCOMPARE(enc.get_ticks(), 2);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved1DegAnd89Deg_Then_OneTick(void)
{
    Encoder enc(4);
    enc.update(deg2rad(1));
    enc.update(deg2rad(89));
    QCOMPARE(enc.get_ticks(), 1);
}

void EncoderTester::Given_4TicksPerRotation_When_Moved1DegAnd88Deg_Then_NoTick(void)
{
    Encoder enc(4);
    enc.update(deg2rad(1));
    enc.update(deg2rad(88));
    QCOMPARE(enc.get_ticks(), 0);
}

void EncoderTester::Given_4TicksPerRotation_When_MovedBackward10And79degs_Then_MinusOneTick(void)
{
    Encoder enc(4);
    enc.update(deg2rad(-10));
    enc.update(deg2rad(-79));
    QCOMPARE(enc.get_ticks(), 0);
}

void EncoderTester::Given_4TicksPerRotation_When_MovedBackward10And80degs_Then_MinusOneTick(void)
{
    Encoder enc(4);
    enc.update(deg2rad(-10));
    enc.update(deg2rad(-80));
    QCOMPARE(enc.get_ticks(), -1);
}

void EncoderTester::Given_2TicksPerRotation_When_Moved30DegSevenTimes_Then_OneTick(void)
{
    Encoder enc(2);
    for(uint8_t i = 0; i<7; i++)
    {
        enc.update(deg2rad(30));
    }
    QCOMPARE(enc.get_ticks(), 1);
}

void EncoderTester::Given_1TicksPerRotation_When_RotatedTwiceAndAskedTwice_Then_OneTickEveryAsk(void)
{
    Encoder enc(1);
    enc.update(deg2rad(360));
    //this get should clear current counter
    enc.get_ticks();
    enc.update(deg2rad(360));
    QCOMPARE(enc.get_ticks(), 1);
}
