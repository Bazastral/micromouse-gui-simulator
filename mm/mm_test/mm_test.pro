TEMPLATE = app

TARGET = mm_test

QT += core testlib
QT -= gui

CONFIG += c++17
CONFIG += console testcase
CONFIG -= app_bundle

SOURCES += main.cpp \
    encoder_test.cpp \
    motor_test.cpp \
    drive_test.cpp \
    diffDrive_test.cpp \
    env_test.cpp \
    utils_test.cpp \
    ./mocks/drive_mock.cpp \

HEADERS += \
    encoder_test.h \
    motor_test.h \
    drive_test.h \
    diffDrive_test.h \
    env_test.h \
    utils_test.h \
    ./mocks/drive_mock.h \

# For the unit tests, we need to be able to include anything in the main code
INCLUDEPATH += ../mm_sim
DEPENDPATH  += ../mm_sim
# This ensures, that when library changes, and this is incremental build, then
# target will contain the latest library
PRE_TARGETDEPS += ../mm_sim

INCLUDEPATH += mocks/

LIBS += -L../mm_sim -lmm_sim


