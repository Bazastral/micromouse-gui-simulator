#pragma once

#include <QObject>

#include "drive.h"

class DiffDriveTester : public QObject
{
    Q_OBJECT
public:
    explicit DiffDriveTester(QObject *parent = 0);

private slots:
    void When_Initialized_Then_StateZero(void);
    void When_SetSpecificState_Then_GetAlsoWorks(void);

    void Given_HeadingXaxis_When_EqualSpeedLeftRight_Then_Xrises(void);
    void Given_HeadingYaxis_When_EqualSpeedLeftRight_Then_Yrises(void);
    void Given_Heading40degs_When_EqualSpeedLeftRightAndMultipleIterations_Then_BothAxisRiseAccordingly(void);
    void Given_Heading40degs_When_SpeedsDifferByMarginalValue_Then_TreatItAsStraightMovement(void);
    void When_SpeedsDiffersNotMarginallyRight_Then_MakeATurn(void);
    void When_SpeedsDiffersNotMarginallyLeft_Then_MakeATurn(void);

    void When_EqualSpeedLeftRightButDifferentSign_Then_Rotate(void);
    void When_DroveAbit_Then_GetProperEncoderTicks(void);

private:
    const double VALID_RADIUS = 0.01;
    const uint32_t VALID_TICKS_PER_TURN = 100;
};