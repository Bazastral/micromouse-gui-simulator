
#include <QTest>
#include <QPoint>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>
#include <vector>

#include "env_test.h"
#include "env.h"
#include "sensorDist.h"
#include "test_helper.h"

// Useful size: https://www.mathwarehouse.com/triangle-calculator/online.php


const double ALLOWED_DISTANCE_MM_ERROR = 0.1;
const double ALLOWED_ANGLE_RAD_ERROR = Utils::degreesToRadians(0.1);
// test distance always with some offset
const double DIST_OFFSET = 1;


// returns random integer from range [a, b] - including a and b
static int randint(int a, int b)
{
    return a + rand()%(1+b-a);
}

EnvTester::EnvTester(QObject *parent) : QObject(parent)
{
    tested_ds = new SensorDist(DEFAULT_DS_X_OFFSET,
                                             DEFAULT_DS_Y_OFFSET,
                                             DEFAULT_DS_T_OFFSET,
                                             DEFAULT_DS_MIN_RANGE,
                                             TESTED_DS_MAX_RANGE);
    srand(time(NULL));
    maze_mock.currMazesetAll();
}
EnvTester::~EnvTester()
{
    delete tested_ds;
}

void EnvTester::DS_Given_OutsideMap_When_OnNorth_Then_ReturnError(void)
{
    Position ds_real_position(MAZE_DIM_SIDE_LEN / 2, MAZE_DIM_SIDE_LEN + DIM_SMALL_OFFSET , 0);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}

void EnvTester::DS_Given_OutsideMap_When_OnSouth_Then_ReturnError(void)
{
    Position ds_real_position(MAZE_DIM_SIDE_LEN / 2, -DIM_SMALL_OFFSET, 0);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}

void EnvTester::DS_Given_OutsideMap_When_OnEast_Then_ReturnError(void)
{
    Position ds_real_position(MAZE_DIM_SIDE_LEN + DIM_SMALL_OFFSET, MAZE_DIM_SIDE_LEN / 2, 0);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}
void EnvTester::DS_Given_OutsideMap_When_OnWest_Then_ReturnError(void)
{
    Position ds_real_position(-DIM_SMALL_OFFSET, MAZE_DIM_SIDE_LEN / 2, 0);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}

void EnvTester::DS_Given_90degress_When_NorthAndHitsFirstWall_Then_ProperlyMeasure(void)
{
    double distance_from_wall = randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_SQUARE_HALF, MAZE_DIM_SQUARE - distance_from_wall, Utils::ORIENTATION_NORTH);
    maze_mock.currMazesetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_SouthAndHitsFirstWall_Then_ProperlyMeasure(void)
{
    double distance_from_wall = randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_SQUARE_HALF, MAZE_DIM_WALL + distance_from_wall, Utils::ORIENTATION_SOUTH);
    maze_mock.currMazesetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_EastAndHitsFirstWall_Then_ProperlyMeasure(void)
{
    double distance_from_wall = randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_SQUARE - distance_from_wall, MAZE_DIM_SQUARE_HALF, Utils::ORIENTATION_EAST);
    maze_mock.currMazesetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_WestAndHitsFirstWall_Then_ProperlyMeasure(void)
{
    double distance_from_wall = randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_WALL + distance_from_wall, MAZE_DIM_SQUARE_HALF, Utils::ORIENTATION_WEST);
    maze_mock.currMazesetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_90degress_When_NorthAndHitsSecondWall_Then_ProperlyMeasure(void)
{
    double position_y = MAZE_DIM_WALL + randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_SQUARE_HALF, position_y, Utils::ORIENTATION_NORTH);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    double distance_from_wall = 2 * MAZE_DIM_SQUARE - position_y;
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_SouthAndHitsSecondWall_Then_ProperlyMeasure(void)
{
    double position_y = MAZE_DIM_WALL + MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(MAZE_DIM_SQUARE_HALF, position_y, Utils::ORIENTATION_SOUTH);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    double distance_from_wall = -MAZE_DIM_WALL + position_y;
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_EastAndHitsSecondWall_Then_ProperlyMeasure(void)
{
    double position_x = MAZE_DIM_WALL + randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(position_x, MAZE_DIM_SQUARE_HALF, Utils::ORIENTATION_EAST);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    double distance_from_wall = 2 * MAZE_DIM_SQUARE - position_x;
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_90degress_When_WestAndHitsSecondWall_Then_ProperlyMeasure(void)
{
    double position_x = MAZE_DIM_SQUARE + MAZE_DIM_WALL + randint(DIST_OFFSET, MAZE_DIM_CORRIDOR-DIST_OFFSET);
    Position ds_real_position(position_x, MAZE_DIM_SQUARE_HALF, Utils::ORIENTATION_WEST);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    double distance_from_wall = position_x - MAZE_DIM_WALL;
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_85or95degress_When_HitsFirstWallAndAllConfigurations_Then_Angle85degAndProperDistance(void)
{
    std::vector<double> sensor_orientation_testing_values =
        {
            // north
            Utils::degreesToRadians(95),
            Utils::degreesToRadians(85),
            //south
            Utils::degreesToRadians(-95),
            Utils::degreesToRadians(-85),
            //east
            Utils::degreesToRadians(5),
            Utils::degreesToRadians(-5),
            //west
            Utils::degreesToRadians(365),
            Utils::degreesToRadians(355),
        };

    maze_mock.currMazesetAll();
    const double EXPTECTED_DISTANCE = MAZE_DIM_CORRIDOR_HALF / cos(Utils::degreesToRadians(5));

    for (std::vector<double>::iterator  it = sensor_orientation_testing_values.begin(); it != sensor_orientation_testing_values.end(); it++)
    {
        Position ds_real_position(StartingPoint, *it);

        int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                                tested_ds, _measurement);

        QCOMPARE(status, RET_OK);
        QCOMPARE_DOUBLE(EXPTECTED_DISTANCE, _measurement.distance, ALLOWED_DISTANCE_MM_ERROR);
        QCOMPARE_DOUBLE(Utils::degreesToRadians(85), _measurement.angle_rad, ALLOWED_ANGLE_RAD_ERROR);
    }
}

/**
 * All walls are set
 * Distance sensor is close to wall (half wall thickness) under specific angle
 * Tested from both ends of the wall
 */
void EnvTester::DS_Given_CloseToWallAnd10degress_When_HitsFirstWallAndAllConfigurations_Then_Angle10degAndProperDistance(void)
{
    const double STRAIGHT_DISTANCE_TO_WALL = MAZE_DIM_WALL_HALF;
    const double ANGLE_DEGS = 10;
    const double EXPTECTED_DISTANCE = STRAIGHT_DISTANCE_TO_WALL / sin(Utils::degreesToRadians(ANGLE_DEGS));

    std::vector<Position> testing_values =
        {
            // north
            {MAZE_DIM_SQUARE - MAZE_DIM_WALL_HALF, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_WALL, Utils::degreesToRadians(180-ANGLE_DEGS)},
            {MAZE_DIM_WALL + MAZE_DIM_WALL_HALF, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_WALL, Utils::degreesToRadians(ANGLE_DEGS)},
            // south
            {MAZE_DIM_SQUARE - MAZE_DIM_WALL_HALF, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_WALL, Utils::degreesToRadians(-180+ANGLE_DEGS)},
            {MAZE_DIM_WALL + MAZE_DIM_WALL_HALF, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_WALL, Utils::degreesToRadians(-ANGLE_DEGS)},
            // east
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_WALL, MAZE_DIM_WALL + MAZE_DIM_WALL_HALF, Utils::degreesToRadians(90-ANGLE_DEGS)},
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_WALL, MAZE_DIM_SQUARE - MAZE_DIM_WALL_HALF, Utils::degreesToRadians(-90+ANGLE_DEGS)},
            // west
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_WALL, MAZE_DIM_WALL + MAZE_DIM_WALL_HALF, Utils::degreesToRadians(90+ANGLE_DEGS)},
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_WALL, MAZE_DIM_SQUARE - MAZE_DIM_WALL_HALF, Utils::degreesToRadians(-90-ANGLE_DEGS)},
        };

    maze_mock.currMazesetAll();

    for (std::vector<Position>::iterator it = testing_values.begin(); it != testing_values.end(); it++)
    {
        int status = Env::measureSensorDistance(&maze_mock, &(*it),
                                                tested_ds, _measurement);

        QCOMPARE(status, RET_OK);
        QCOMPARE_DOUBLE(EXPTECTED_DISTANCE, _measurement.distance, ALLOWED_DISTANCE_MM_ERROR);
        QCOMPARE_DOUBLE(Utils::degreesToRadians(ANGLE_DEGS), _measurement.angle_rad, ALLOWED_ANGLE_RAD_ERROR);
    }
}

/**
 * Pointing post when there are no walls.
 * Test all four posts, from both sides
 */
void EnvTester::DS_Given_CloseToPostAnd15degress_When_HitsFirstPostAndAllConfigurations_Then_MeasureProperly(void)
{
    const double ANGLE_DEGS = 15;
    const double STRAIGHT_DISTANCE_TO_CLOSER_WALL = MAZE_DIM_WALL_HALF;
    const double EXPTECTED_DISTANCE = STRAIGHT_DISTANCE_TO_CLOSER_WALL / sin(Utils::degreesToRadians(ANGLE_DEGS));
    // this distance should be proper, so that the laser is pointing exactly on the surface of the post
    const double STRAIGHT_DISTANCE_TO_FURTHER_WALL = STRAIGHT_DISTANCE_TO_CLOSER_WALL/tan(Utils::degreesToRadians(ANGLE_DEGS)) + 0.1 -MAZE_DIM_WALL;
    maze_mock.currMazeResetAll();

    std::vector<Position> testing_values =
        {
            // SW post, N side
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_FURTHER_WALL, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_CLOSER_WALL, Utils::degreesToRadians(-180+ANGLE_DEGS)},
            // SW post, E side
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_CLOSER_WALL, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_FURTHER_WALL, Utils::degreesToRadians(-90-ANGLE_DEGS)},

            // SE post, W side
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_CLOSER_WALL, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_FURTHER_WALL, Utils::degreesToRadians(-90+ANGLE_DEGS)},
            // SE post, N side
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_FURTHER_WALL, MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_CLOSER_WALL, Utils::degreesToRadians(-ANGLE_DEGS)},

            // NE post, S side
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_FURTHER_WALL, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_CLOSER_WALL, Utils::degreesToRadians(ANGLE_DEGS)},
            // NE post, W side
            {MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_CLOSER_WALL, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_FURTHER_WALL, Utils::degreesToRadians(90-ANGLE_DEGS)},

            // NW post, E side
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_CLOSER_WALL, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_FURTHER_WALL, Utils::degreesToRadians(90+ANGLE_DEGS)},
            // NW post, S side
            {MAZE_DIM_WALL + STRAIGHT_DISTANCE_TO_FURTHER_WALL, MAZE_DIM_SQUARE - STRAIGHT_DISTANCE_TO_CLOSER_WALL, Utils::degreesToRadians(180-ANGLE_DEGS)},
        };


    for (std::vector<Position>::iterator it = testing_values.begin(); it != testing_values.end(); it++)
    {
        int status = Env::measureSensorDistance(&maze_mock, &(*it),
                                                tested_ds, _measurement);

        QCOMPARE(status, RET_OK);
        QCOMPARE_DOUBLE(EXPTECTED_DISTANCE, _measurement.distance, ALLOWED_DISTANCE_MM_ERROR);
        QCOMPARE_DOUBLE(Utils::degreesToRadians(ANGLE_DEGS), _measurement.angle_rad, ALLOWED_ANGLE_RAD_ERROR);
    }
}

void EnvTester::DS_Given_NotStartingCell_When_NorthAnd86degsAnd3Cells_Then_Measure(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(86);
    Position ds_real_position(
        3*MAZE_DIM_SQUARE + MAZE_DIM_WALL + MAZE_DIM_CORRIDOR_HALF,
        2*MAZE_DIM_SQUARE + MAZE_DIM_WALL + MAZE_DIM_CORRIDOR_HALF,
        ANGLE_RADS);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(3, 2, DIR_NORTH);
    maze_mock.currMazeResetWall(3, 3, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, (2*MAZE_DIM_SQUARE + MAZE_DIM_CORRIDOR_HALF)/sin(ANGLE_RADS) );
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_NotStartingCell_When_PointingSwAndWeirdStartAndHittingPost_Then_Measure(void)
{
    // triangle with dimensions 270, 195 and one angle = 90
    //should hit 2 mm from the bottom of the E side of SW post
    const double ANGLE_RADS = Utils::degreesToRadians(+35.8376529542783);
    const int X_OFFSET = 195+MAZE_DIM_WALL-MAZE_DIM_SQUARE;
    const int Y_OFFSET = 270-MAZE_DIM_SQUARE + 2;
    Position ds_real_position(
        5*MAZE_DIM_SQUARE + X_OFFSET,
        8*MAZE_DIM_SQUARE + Y_OFFSET,
        -M_PI_2-ANGLE_RADS);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(5, 8, DIR_WEST);
    maze_mock.currMazeResetWall(4, 8, DIR_SOUTH);
    maze_mock.currMazeResetWall(4, 7, DIR_SOUTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, 333.0540496676178);
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}
void EnvTester::DS_Given_StartingPoint_When_PointingNeAndHittingPostTwoCellsAbove_Then_Measure(void)
{
    // triangle with dimensions 264(square dim + corridor_half), 278.91934, 90 (corridorhalf + wall_half)
    // should hit NW post in (1, 1) cell from its S side
    const double ANGLE_RADS = Utils::degreesToRadians(71.175);
    Position ds_real_position( StartingPoint, ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE_DOUBLE(_measurement.distance, 278.91934, ALLOWED_DISTANCE_MM_ERROR);
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_N_90degs_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR/2;
    Position ds_real_position(
        MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different x position
        MAZE_DIM_SQUARE - distance_from_wall,
        Utils::ORIENTATION_NORTH);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_N_90degsAndAnotherCell_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE*2 + MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different x position
        MAZE_DIM_SQUARE*4 + MAZE_DIM_SQUARE - distance_from_wall,
        Utils::ORIENTATION_NORTH);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}
void EnvTester::DS_Given_InsideEmptyWallNS_When_S_90degs_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR/2;
    Position ds_real_position(
        MAZE_DIM_SQUARE +randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different x position
        MAZE_DIM_WALL + distance_from_wall,
        Utils::ORIENTATION_SOUTH);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_S_90degsAndAnotherCell_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE*3 + MAZE_DIM_SQUARE +randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different x position
        MAZE_DIM_SQUARE*7 + MAZE_DIM_WALL + distance_from_wall,
        Utils::ORIENTATION_SOUTH);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallEW_When_E_90degs_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR/2;
    Position ds_real_position(
        MAZE_DIM_SQUARE - distance_from_wall,
        MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different y position
        Utils::ORIENTATION_EAST);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallEW_When_E_90degsAndAnotherCell_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE*4 + MAZE_DIM_SQUARE - distance_from_wall,
        MAZE_DIM_SQUARE*2 + MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different y position
        Utils::ORIENTATION_EAST);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallEW_When_W_90degs_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR/2;
    Position ds_real_position(
        MAZE_DIM_WALL + distance_from_wall,
        MAZE_DIM_SQUARE +randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different y position
        Utils::ORIENTATION_WEST);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallEW_When_W_90degsAndAnotherCell_Then_Measure(void)
{
    double distance_from_wall = MAZE_DIM_CORRIDOR*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE*7 + MAZE_DIM_WALL + distance_from_wall,
        MAZE_DIM_SQUARE*3 + MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET), // different y position
        Utils::ORIENTATION_WEST);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall);
    QCOMPARE(_measurement.angle_rad, M_PI_2);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_SE_above45degsAndPointsPost_Then_HitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(45.0001);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF/2,
        MAZE_DIM_WALL + distance_from_wall,
        -ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}
void EnvTester::DS_Given_InsideEmptyWallNS_When_SE_44degsAndPointsNearToPost_Then_DoNotHitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(44);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF/2,
        MAZE_DIM_WALL + distance_from_wall,
        -ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(true, _measurement.distance > distance_from_wall/sin(ANGLE_RADS));
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_SW_above45degsAndPointsPost_Then_HitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(45.0001);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL*3/4,
        MAZE_DIM_WALL + distance_from_wall,
        -M_PI+ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}
void EnvTester::DS_Given_InsideEmptyWallNS_When_SW_44degsAndPointsNearToPost_Then_DoNotHitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(44);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL*3/4,
        MAZE_DIM_WALL + distance_from_wall,
        -M_PI+ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(true, _measurement.distance > distance_from_wall/sin(ANGLE_RADS));
}
void EnvTester::DS_Given_InsideEmptyWallNS_When_NE_above45degsAndPointsPost_Then_HitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(45.0001);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF/2,
        MAZE_DIM_SQUARE - distance_from_wall,
        ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_NE_44degsAndPointsNearToPost_Then_DoNotHitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(44);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF/2,
        MAZE_DIM_SQUARE - distance_from_wall,
        ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(true, _measurement.distance > distance_from_wall/sin(ANGLE_RADS));
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_NW_above45degsAndPointsPost_Then_HitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(45.0001);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL*3/4,
        MAZE_DIM_SQUARE - distance_from_wall,
        +M_PI-ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, distance_from_wall/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_NW_44degsAndPointsNearToPost_Then_DoNotHitPost(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(44);
    double distance_from_wall = MAZE_DIM_WALL*3/4;
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL*3/4,
        MAZE_DIM_SQUARE - distance_from_wall,
        +M_PI-ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(true, _measurement.distance > distance_from_wall/sin(ANGLE_RADS));
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_NW_89degsAndPointsCornerOfPost_Then_HitPost(void)
{
    // Side a = 114.59738
    // Side b = 114.57992
    // Side c = 2
    // Angle ∠A = 90° = 1.5708 rad = π/2
    // Angle ∠B = 89° = 1.55334 rad
    // Angle ∠C = 1° = 0.017453 rad
    const double ANGLE_RADS = Utils::degreesToRadians(89);
    double distance_from_wall = 114.57992;
    Position ds_real_position(
        MAZE_DIM_SQUARE + 1 + 2,
        MAZE_DIM_SQUARE - distance_from_wall,
        +M_PI - ANGLE_RADS);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE_DOUBLE(114.59738, _measurement.distance, ALLOWED_DISTANCE_MM_ERROR);
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_NW_85degsAndPointsCornerOfPost_Then_HitPost(void)
{
    // Side a = 91.78971
    // Side b = 91.44042
    // Side c = 8

    // Angle ∠A = 90° = 1.5708 rad = π/2
    // Angle ∠B = 85° = 1.48353 rad
    // Angle ∠C = 5° = 0.087266 rad
    const double ANGLE_RADS = Utils::degreesToRadians(85);
    double distance_from_wall = 91.44042;
    Position ds_real_position(
        MAZE_DIM_SQUARE + 1 + 8,
        MAZE_DIM_SQUARE - distance_from_wall,
        +M_PI - ANGLE_RADS);
    maze_mock.currMazeResetAll();
    // maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE_DOUBLE(91.78971, _measurement.distance, ALLOWED_DISTANCE_MM_ERROR);
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_E_80degs_Then_Measure(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(80);
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF,
        MAZE_DIM_SQUARE_HALF,
        M_PI_2-ANGLE_RADS);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, (MAZE_DIM_SQUARE-MAZE_DIM_WALL_HALF)/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallNS_When_W_81degs_Then_Measure(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(81);
    Position ds_real_position(
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF,
        MAZE_DIM_SQUARE_HALF,
        M_PI_2+ANGLE_RADS);
    maze_mock.currMazesetAll();
    maze_mock.currMazeResetWall(0, 0, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, (MAZE_DIM_SQUARE-MAZE_DIM_WALL_HALF)/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}

void EnvTester::DS_Given_InsideEmptyWallEW_When_SE_40degs_Then_Measure(void)
{
    const double ANGLE_RADS = Utils::degreesToRadians(40);
    Position ds_real_position(
        MAZE_DIM_SQUARE_HALF,
        MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF,
        M_PI_2-ANGLE_RADS);
    maze_mock.currMazeResetWall(0, 0, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, (MAZE_DIM_SQUARE_HALF)/sin(ANGLE_RADS));
    QCOMPARE(_measurement.angle_rad, ANGLE_RADS);
}
void EnvTester::DS_Given_NotStartingPoint_When_InsideExistingWallEW_Then_ReturnZeroDistance(void)
{
    const double ANGLE_RADS = rand();
    Position ds_real_position(
        3*MAZE_DIM_SQUARE + MAZE_DIM_SQUARE_HALF,
        4*MAZE_DIM_SQUARE + MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF,
        ANGLE_RADS);
    maze_mock.currMazeSetWall(3, 4, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, 0);
}

void EnvTester::DS_Given_NotStartingPoint_When_InsideExistingWallNS_Then_ReturnZeroDistance(void)
{
    const double ANGLE_RADS = rand();
    Position ds_real_position(
        2*MAZE_DIM_SQUARE + MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF,
        9*MAZE_DIM_SQUARE + MAZE_DIM_SQUARE_HALF,
        ANGLE_RADS);
    maze_mock.currMazeSetWall(2, 9, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, 0);
}

void EnvTester::DS_Given_MaxXCell_When_InsideBorderWall_Then_ReturnError(void)
{
    const double ANGLE_RADS = rand();
    Position ds_real_position(
        MAZE_DIM_SIDE_LEN - MAZE_DIM_WALL_HALF,
        9 * MAZE_DIM_SQUARE + MAZE_DIM_SQUARE_HALF,
        ANGLE_RADS);
    maze_mock.currMazeSetWall(MAZE_N, 9, DIR_EAST);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}

void EnvTester::DS_Given_MaxYCell_When_InsideBorderWall_Then_ReturnError(void)
{
    const double ANGLE_RADS = rand();
    Position ds_real_position(
        5 * MAZE_DIM_SQUARE + MAZE_DIM_SQUARE_HALF,
        MAZE_DIM_SIDE_LEN - MAZE_DIM_WALL_HALF,
        ANGLE_RADS);
    maze_mock.currMazeSetWall(5, MAZE_N, DIR_NORTH);

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_ERR);
}

void EnvTester::DS_Given_90degress_When_EastAndNoWalls_Then_ReturnMaxRange(void)
{
    const double ANGLE_RADS = 0;
    const double MAX_RANGE = randint(DIST_OFFSET, MAZE_DIM_SIDE_LEN - MAZE_DIM_SQUARE-DIST_OFFSET);
    SensorDist* tested_ds = new SensorDist(DEFAULT_DS_X_OFFSET,
                                             DEFAULT_DS_Y_OFFSET,
                                             DEFAULT_DS_T_OFFSET,
                                             DEFAULT_DS_MIN_RANGE,
                                             MAX_RANGE);
    Position ds_real_position(StartingPoint, ANGLE_RADS);
    maze_mock.currMazeResetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, MAX_RANGE);
    delete tested_ds;
}

void EnvTester::DS_Given_StartingPoint_When_RandomMinRange_Then_ReturnThisMinRange(void)
{
    const double ANGLE_RADS = 0;
    const double MIN_RANGE = MAZE_DIM_SQUARE + randint(DIST_OFFSET, MAZE_DIM_WALL-DIST_OFFSET);
    SensorDist* tested_ds = new SensorDist(DEFAULT_DS_X_OFFSET,
                                             DEFAULT_DS_Y_OFFSET,
                                             DEFAULT_DS_T_OFFSET,
                                             MIN_RANGE,
                                             TESTED_DS_MAX_RANGE);
    Position ds_real_position(StartingPoint, ANGLE_RADS);
    maze_mock.currMazesetAll();

    int status = Env::measureSensorDistance(&maze_mock, &ds_real_position,
                                            tested_ds, _measurement);

    QCOMPARE(status, RET_OK);
    QCOMPARE(_measurement.distance, MIN_RANGE);
    delete tested_ds;
}