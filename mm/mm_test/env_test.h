#pragma once

#include <QObject>

#include "env.h"
#include "sensorDist.h"
#include "maze.h"

class EnvTester : public QObject
{
    Q_OBJECT
public:
    explicit EnvTester(QObject *parent = 0);
    ~EnvTester();

private slots:
    //*******************************  DS -> distance sensor (other naming: laser)
    // sensor is outside of the map
    void DS_Given_OutsideMap_When_OnNorth_Then_ReturnError(void);
    void DS_Given_OutsideMap_When_OnSouth_Then_ReturnError(void);
    void DS_Given_OutsideMap_When_OnEast_Then_ReturnError(void);
    void DS_Given_OutsideMap_When_OnWest_Then_ReturnError(void);

    // sensor points directly the first wall
    void DS_Given_90degress_When_NorthAndHitsFirstWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_SouthAndHitsFirstWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_EastAndHitsFirstWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_WestAndHitsFirstWall_Then_ProperlyMeasure(void);

    // sensor points directly the second wall
    void DS_Given_90degress_When_NorthAndHitsSecondWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_SouthAndHitsSecondWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_EastAndHitsSecondWall_Then_ProperlyMeasure(void);
    void DS_Given_90degress_When_WestAndHitsSecondWall_Then_ProperlyMeasure(void);

    // different sensor locations and angles. Sensor is inside the cell (not on the wall)
    // and points to first wall which exists
    void DS_Given_85or95degress_When_HitsFirstWallAndAllConfigurations_Then_Angle85degAndProperDistance(void);
    void DS_Given_CloseToWallAnd10degress_When_HitsFirstWallAndAllConfigurations_Then_Angle10degAndProperDistance(void);
    void DS_Given_CloseToPostAnd15degress_When_HitsFirstPostAndAllConfigurations_Then_MeasureProperly(void);

    // ******** Sensor is on the line where wall *can* be
    // inside walls - point posts directly
    void DS_Given_InsideEmptyWallNS_When_N_90degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallNS_When_N_90degsAndAnotherCell_Then_Measure(void);
    void DS_Given_InsideEmptyWallNS_When_S_90degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallNS_When_S_90degsAndAnotherCell_Then_Measure(void);

    void DS_Given_InsideEmptyWallEW_When_E_90degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallEW_When_E_90degsAndAnotherCell_Then_Measure(void);
    void DS_Given_InsideEmptyWallEW_When_W_90degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallEW_When_W_90degsAndAnotherCell_Then_Measure(void);

    // inside walls - the laser points the corner of the post.
    // Once should hit the post, once should omit it
    void DS_Given_InsideEmptyWallNS_When_SE_above45degsAndPointsPost_Then_HitPost(void);
    void DS_Given_InsideEmptyWallNS_When_SE_44degsAndPointsNearToPost_Then_DoNotHitPost(void);
    void DS_Given_InsideEmptyWallNS_When_SW_above45degsAndPointsPost_Then_HitPost(void);
    void DS_Given_InsideEmptyWallNS_When_SW_44degsAndPointsNearToPost_Then_DoNotHitPost(void);

    void DS_Given_InsideEmptyWallNS_When_NE_above45degsAndPointsPost_Then_HitPost(void);
    void DS_Given_InsideEmptyWallNS_When_NE_44degsAndPointsNearToPost_Then_DoNotHitPost(void);
    void DS_Given_InsideEmptyWallNS_When_NW_above45degsAndPointsPost_Then_HitPost(void);
    void DS_Given_InsideEmptyWallNS_When_NW_44degsAndPointsNearToPost_Then_DoNotHitPost(void);

    void DS_Given_InsideEmptyWallNS_When_NW_89degsAndPointsCornerOfPost_Then_HitPost(void);
    void DS_Given_InsideEmptyWallNS_When_NW_85degsAndPointsCornerOfPost_Then_HitPost(void);

    // inside walls - different UTs, pointing other walls
    void DS_Given_InsideEmptyWallNS_When_E_80degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallNS_When_W_81degs_Then_Measure(void);
    void DS_Given_InsideEmptyWallEW_When_SE_40degs_Then_Measure(void);

    // inside walls that exist
    void DS_Given_NotStartingPoint_When_InsideExistingWallEW_Then_ReturnZeroDistance(void);
    void DS_Given_NotStartingPoint_When_InsideExistingWallNS_Then_ReturnZeroDistance(void);
    void DS_Given_MaxXCell_When_InsideBorderWall_Then_ReturnError(void);
    void DS_Given_MaxYCell_When_InsideBorderWall_Then_ReturnError(void);
    // ******** Sensor is on the line where wall *can* be (end)

    // more complex UTs when the laser is longer and the sensor position is not obvious
    void DS_Given_NotStartingCell_When_NorthAnd86degsAnd3Cells_Then_Measure(void);
    void DS_Given_NotStartingCell_When_PointingSwAndWeirdStartAndHittingPost_Then_Measure(void);
    void DS_Given_StartingPoint_When_PointingNeAndHittingPostTwoCellsAbove_Then_Measure(void);


    // ranges of the sensor
    void DS_Given_90degress_When_EastAndNoWalls_Then_ReturnMaxRange(void);
    void DS_Given_StartingPoint_When_RandomMinRange_Then_ReturnThisMinRange(void);


private:
    const double DEFAULT_DS_X_OFFSET = 0;
    const double DEFAULT_DS_Y_OFFSET = 0;
    const double DEFAULT_DS_T_OFFSET = 0;
    const double DEFAULT_DS_MIN_RANGE = 0;
    const double TESTED_DS_MAX_RANGE = 3000;

    const double DIM_SMALL_OFFSET = 0.0001;


    SensorDist * tested_ds;
    SensorDist::Measurement _measurement;
    Maze maze_mock;
};