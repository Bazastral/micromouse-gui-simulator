#include <QCoreApplication>
#include <QTest>

#include "encoder_test.h"
#include "motor_test.h"
#include "drive_test.h"
#include "diffDrive_test.h"
#include "env_test.h"
#include "utils_test.h"

#define LOG_COLOR_RED "\033[0;31m"
#define LOG_COLOR_GREEN "\033[0;32m"
#define LOG_COLOR_RESET "\033[0m"

int main(int argc, char *argv[])
{
    int errors = 0;
    Q_UNUSED(argc);
    Q_UNUSED(argv);

    EncoderTester encoder;
    MotorTester motor;
    MotorTester motor_not_default(NULL, 5, 12);
    DriveTester drive;
    DiffDriveTester diff_drive;
    EnvTester environment;
    UtilsTester utils;

    errors += QTest::qExec( &encoder );
    errors += QTest::qExec( &motor );
    errors += QTest::qExec( &motor_not_default );
    errors += QTest::qExec( &drive );
    errors += QTest::qExec( &diff_drive );
    errors += QTest::qExec( &environment );
    errors += QTest::qExec( &utils );

    if (errors)
    {
        printf(LOG_COLOR_RED "FAIL\n" LOG_COLOR_RESET);
        printf(LOG_COLOR_RED "%d error(s)\n" LOG_COLOR_RESET, errors);
    }
    else
    {
        printf(LOG_COLOR_GREEN "PASS\n" LOG_COLOR_RESET);
    }

    return errors;
}