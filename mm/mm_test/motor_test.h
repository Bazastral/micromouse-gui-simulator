#pragma once

#include <QObject>

#include "motor.h"

class MotorTester : public QObject
{
    Q_OBJECT
public:
    explicit MotorTester(QObject *parent = 0, double k = 1.0, double t = 1.0);

private slots:
    void Given_Still_When_NoMove_Then_StateDoesNotChange(void);
    void When_InitedWithDifferentK_Then_MaxVelocityChanges(void);
    void Given_Still_When_SetMax_Then_AchieveMaxVelocityAfterInfiniteTime(void);
    void Given_Still_When_SetMax_Then_AccelerationDecreasesOverTime(void);
    void Given_Still_When_SetMax_Then_Achieve0dot632MaxVelocityAfterTimeConstant(void);

private:
    double _param_k;
    double _param_t;
};