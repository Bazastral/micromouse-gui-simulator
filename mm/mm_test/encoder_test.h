#pragma once

#include <QObject>

#include "encoder.h"

class EncoderTester : public QObject
{
    Q_OBJECT
public:
    explicit EncoderTester(QObject *parent = 0);

private slots:
    void When_NoMove_Then_NoEncoderTicks(void);
    void Given_4TicksPerRotation_When_Moved90Deg_Then_OneTick(void);
    void Given_4TicksPerRotation_When_Moved89Deg_Then_NoTick(void);
    void Given_8TicksPerRotation_When_Moved90Deg_Then_TwoTicks(void);
    void Given_4TicksPerRotation_When_Moved90DegBackwards_Then_MinusOneTick(void);
    void Given_4TicksPerRotation_When_Moved360Deg_Then_4Ticks(void);
    void Given_4TicksPerRotation_When_Moved90DegAnd90Deg_Then_TwoTicks(void);
    void Given_4TicksPerRotation_When_Moved1DegAnd89Deg_Then_OneTick(void);
    void Given_4TicksPerRotation_When_Moved1DegAnd88Deg_Then_NoTick(void);
    void Given_4TicksPerRotation_When_MovedBackward10And79degs_Then_MinusOneTick(void);
    void Given_4TicksPerRotation_When_MovedBackward10And80degs_Then_MinusOneTick(void);
    void Given_2TicksPerRotation_When_Moved30DegSevenTimes_Then_OneTick(void);
    void Given_1TicksPerRotation_When_RotatedTwiceAndAskedTwice_Then_OneTickEveryAsk(void);
};