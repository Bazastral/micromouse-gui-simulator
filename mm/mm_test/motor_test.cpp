
#include <QTest>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "motor_test.h"
#include "motor.h"
#include "test_helper.h"



MotorTester::MotorTester(QObject *parent, double k, double t) : QObject(parent)
{
    _param_k = k;
    _param_t = t;
    printf("Unit Test for motor, params: t=%f, k=%f\n", t, k);
}

void MotorTester::Given_Still_When_NoMove_Then_StateDoesNotChange(void)
{
    Motor motor("UT", _param_t, _param_k);

    motor.step();

    QCOMPARE_DOUBLE(0, motor.getRotationVelocity(), 0.0001);
}

void MotorTester::When_InitedWithDifferentK_Then_MaxVelocityChanges(void)
{
    Motor motor("UT", _param_t, _param_k);
    QCOMPARE_DOUBLE(0, motor.getMaxPossibleSpeed(), _param_k);
}

void MotorTester::Given_Still_When_SetMax_Then_AchieveMaxVelocityAfterInfiniteTime(void)
{
    Motor motor("UT", _param_t, _param_k);
    motor.setTargetSpeed(1.0);

    for(int i = 0; i < 1e3; i++)
    {
        motor.step(0.1);
    }

    QCOMPARE_DOUBLE(motor.getMaxPossibleSpeed(), motor.getRotationVelocity(), 0.01);
}


void MotorTester::Given_Still_When_SetMax_Then_AccelerationDecreasesOverTime(void)
{
    Motor motor("UT", _param_t, _param_k);
    motor.setTargetSpeed(1.0);
    int iterations = 10;

    double prev_velocity = motor.getRotationVelocity();
    double prev_velocity_diff;
    double velocity_diff;

    motor.step();
    prev_velocity_diff = motor.getRotationVelocity() - prev_velocity;
    prev_velocity = motor.getRotationVelocity();

    do
    {
        motor.step();
        velocity_diff = motor.getRotationVelocity() - prev_velocity;
        QCOMPARE(1, velocity_diff <= prev_velocity_diff );

        prev_velocity = motor.getRotationVelocity();
        prev_velocity_diff = velocity_diff;
    } while(iterations -- > 0);
}

void MotorTester::Given_Still_When_SetMax_Then_Achieve0dot632MaxVelocityAfterTimeConstant(void)
{
    const double step_time = 0.1;
    Motor motor("UT", _param_t, 1.0);
    motor.setTargetSpeed(1.0);

    double time = 0;

    for(time = step_time; time < _param_t; time += step_time )
    {
        motor.step(step_time);
    }

    QCOMPARE_DOUBLE(0.632*motor.getMaxPossibleSpeed(), motor.getRotationVelocity(), 0.3);
}