#pragma once

#include "drive.h"

class DriveMock : public Drive
{
    public:
    void step_return(double ret);
    double step(double time) override;


    private:
    double step_return_ = 0;
};