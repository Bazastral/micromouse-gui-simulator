
#include "drive.h"
#include "drive_mock.h"
#include "logger.h"



void DriveMock::step_return(double ret)
{
    step_return_ = ret;
}

double DriveMock::step(double time)
{
    (void) time;
    return step_return_;
}