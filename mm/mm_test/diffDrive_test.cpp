
#include <QTest>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "drive_test.h"
#include "diffDrive_test.h"
#include "drive.h"
#include "diffDrive.h"
#include "test_helper.h"
#include "utils.h"

const double SMALL_ERROR = 0.000001;

DiffDriveTester::DiffDriveTester(QObject *parent) : QObject(parent)
{
}

void DiffDriveTester::When_Initialized_Then_StateZero(void)
{
    const double radius = 0.01;
    const uint32_t ticks_per_turn = 100;

    DiffDrive dd("UT", radius, ticks_per_turn);
    QCOMPARE(dd.getX(), 0);
    QCOMPARE(dd.getY(), 0);
    QCOMPARE(dd.getT(), 0);
    QCOMPARE(dd.getV(), 0);
    QCOMPARE(dd.getW(), 0);
}

void DiffDriveTester::When_SetSpecificState_Then_GetAlsoWorks(void)
{
    DiffDrive dd("UT", 1, 100);
    double x = rand();
    double y = rand();
    double t = rand();
    double v = rand();
    double w = rand();

    dd.setState(x, y, t, v, w);

    QCOMPARE(x, dd.getX());
    QCOMPARE(y, dd.getY());
    QCOMPARE(t, dd.getT());
    QCOMPARE(v, dd.getV());
    QCOMPARE(w, dd.getW());
}

void DiffDriveTester::Given_HeadingXaxis_When_EqualSpeedLeftRight_Then_Xrises(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    dd.setState(0, 0, 0, 0, 0);
    dd.setTargetSpeed(1, 1);

    dd.step();

    QVERIFY(dd.getX() > 0);
    QCOMPARE_DOUBLE(dd.getY(), 0, SMALL_ERROR);
    QCOMPARE_DOUBLE(dd.getT(), 0, SMALL_ERROR);
    QCOMPARE_DOUBLE(dd.getW(), 0, SMALL_ERROR);
    QVERIFY(dd.getV() > 0);
}
void DiffDriveTester::Given_HeadingYaxis_When_EqualSpeedLeftRight_Then_Yrises(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    dd.setState(0, 0, M_PI_2, 0, 0);
    dd.setTargetSpeed(1, 1);

    dd.step();

    QCOMPARE_DOUBLE(dd.getX(), 0, SMALL_ERROR);
    QVERIFY(dd.getY() > 0);
    QCOMPARE_DOUBLE(dd.getT(), M_PI_2, SMALL_ERROR);
    QCOMPARE_DOUBLE(dd.getW(), 0, SMALL_ERROR);
    QVERIFY(dd.getV() > 0);
}

void DiffDriveTester::Given_Heading40degs_When_EqualSpeedLeftRightAndMultipleIterations_Then_BothAxisRiseAccordingly(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    double angle_rad = Utils::degreesToRadians(40);
    dd.setState(0, 0, angle_rad, 0, 0);
    dd.setTargetSpeed(1, 1);

    for(uint16_t i = 0; i<10000; i++)
    {
        dd.step();
    }

    QVERIFY(dd.getX() > 0);
    QVERIFY(dd.getY() > 0);
    QCOMPARE_DOUBLE(dd.getX(), dd.getY() / tan(angle_rad) , SMALL_ERROR);
    QCOMPARE(dd.getT(), angle_rad);
    QCOMPARE(dd.getW(), 0);
    QVERIFY(dd.getV() > 0);
}

void DiffDriveTester::Given_Heading40degs_When_SpeedsDifferByMarginalValue_Then_TreatItAsStraightMovement(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    double angle_rad = Utils::degreesToRadians(40);
    dd.setState(0, 0, angle_rad, 0, 0);
    dd.setTargetSpeed(1, 0.99999999999);

    dd.step();

    QVERIFY(dd.getX() > 0);
    QVERIFY(dd.getY() > 0);
    QCOMPARE_DOUBLE(dd.getX(), dd.getY() / tan(angle_rad) , SMALL_ERROR);
    QCOMPARE(dd.getT(), angle_rad);
    QCOMPARE(dd.getW(), 0);
    QVERIFY(dd.getV() > 0);
}

void DiffDriveTester::When_SpeedsDiffersNotMarginallyRight_Then_MakeATurn(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    dd.setState(0, 0, 0, 0, 0);
    dd.setTargetSpeed(1, 0.98);

    dd.step();

    QVERIFY(dd.getX() > 0);
    QVERIFY(dd.getY() < 0);
    QVERIFY(dd.getT()< 0);
    QVERIFY(dd.getW() < 0);
    QVERIFY(dd.getV() > 0);
}

void DiffDriveTester::When_SpeedsDiffersNotMarginallyLeft_Then_MakeATurn(void)
{
    DiffDrive dd("UT", VALID_RADIUS, VALID_TICKS_PER_TURN);
    dd.setState(0, 0, 0, 0, 0);
    dd.setTargetSpeed(0.98, 1);

    dd.step();

    QVERIFY(dd.getX() > 0);
    QVERIFY(dd.getY() > 0);
    QVERIFY(dd.getT() > 0);
    QVERIFY(dd.getW() > 0);
    QVERIFY(dd.getV() > 0);
}

void DiffDriveTester::When_EqualSpeedLeftRightButDifferentSign_Then_Rotate(void)
{
    DiffDrive dd("UT", 0.01, 100);
    dd.setTargetSpeed(-1, 1);

    dd.step();

    QCOMPARE(dd.getX(), 0);
    QCOMPARE(dd.getY(), 0);
    QVERIFY(dd.getT() != 0);
    QCOMPARE(dd.getV(), 0);
    QVERIFY(dd.getW() > 0);
}

void DiffDriveTester::When_DroveAbit_Then_GetProperEncoderTicks(void)
{
    DiffDrive dd("UT", 0.01, 100);
    dd.setTargetSpeed(.4, 1);

    dd.step(1);

    int32_t l = dd.getEncoderTicksLeft();
    int32_t r = dd.getEncoderTicksRight();

    QVERIFY(l > 0);
    QVERIFY(r > 0);
    QVERIFY(r > l);
}
