#pragma once

#include <QObject>

#include "drive.h"

class DriveTester : public QObject
{
    Q_OBJECT
public:
    explicit DriveTester(QObject *parent = 0);

private slots:
    void When_DrivenSpecificDistance_Then_TickEncoderAccordingly(void);
};