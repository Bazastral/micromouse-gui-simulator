
#include <QTest>

#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <math.h>

#include "drive_test.h"
#include "drive.h"
#include "test_helper.h"



DriveTester::DriveTester(QObject *parent) : QObject(parent)
{
}

void DriveTester::When_DrivenSpecificDistance_Then_TickEncoderAccordingly(void)
{
    const double radius = 0.01;
    const uint32_t ticks_per_turn = 100;
    const uint32_t ticks_per_meter = ticks_per_turn / (2 * M_PI * radius);
    const double target_distance = (radius * 2 * M_PI);

    Drive drive("UT", radius, ticks_per_turn);
    drive.setTargetSpeed(0.1);


    while(drive.getTotalDistance() < target_distance)
    {
        printf("total distance %f\n", drive.getTotalDistance());
        drive.step();
    }

    QCOMPARE_DOUBLE(ticks_per_meter * target_distance, drive.getTicks(), 5);
}
