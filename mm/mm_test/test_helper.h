
#include <QTest>
#include <QPoint>


#define QCOMPARE_DOUBLE(expected, result, allowed_error) \
    QVERIFY(result >= (expected - allowed_error));         \
    QVERIFY(result <= (expected + allowed_error));