#pragma once

#include <QObject>

#include "utils.h"

class UtilsTester : public QObject
{
    Q_OBJECT
public:
    explicit UtilsTester(QObject *parent = 0);
    ~UtilsTester();

private slots:
    void coordsLocalToGlobal_When_LocalIsGlobal_Then_ReturnTheSamePosition(void);
    void coordsLocalToGlobal_When_LocalIsShiftedByX_Then_ReturnShiftedByX(void);
    void coordsLocalToGlobal_When_LocalIsShiftedByY_Then_ReturnShiftedByY(void);
    void coordsLocalToGlobal_Given_OnOriginInLocal_When_LocalIsRotated_Then_ReturnRotated(void);
    void coordsLocalToGlobal_Given_XchangedInLocal_When_LocalRotatedBy45degs_Then_ReturnRotated(void);
    void coordsLocalToGlobal_Given_YchangedInLocal_When_LocalRotatedBy45degs_Then_ReturnRotated(void);
    void coordsLocalToGlobal_Given_XandYchangedInLocal_When_LocalRotatedBy90degs_Then_ReturnRotated(void);
    void coordsLocalToGlobal_Given_XandYchangedInLocal_When_LocalRotatedBy90degsAndShiftedXandY_Then_Calcultate(void);

private:
};