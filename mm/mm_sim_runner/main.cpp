#include <QApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include <QCommandLineParser>
#include <QFont>

#include "maze.h"
#include "mazeText.h"
#include "config.h"
#include "env.h"
#include "broker.h"
#include "sensorDist.h"

int main(int argc, char *argv[])
{
  QApplication app(argc, argv);
  app.setApplicationName("mm");
  app.setApplicationVersion("1.0");

  QFont f = app.font();
  f.setPointSize(20);
  app.setFont(f);

  QCommandLineParser parser;
  parser.setApplicationDescription("micromouse gui simulator");
  parser.addHelpOption();
  parser.addVersionOption();

  QCommandLineOption portOption(QStringList() << "p"
                                              << "port",
                                "TCP port for communication", "port", "57233");
  parser.addOption(portOption);

  parser.process(app);

  Config config(NULL);
  Maze maze;
  MMouse menv(nullptr);
  MMouse mprogram(nullptr);

  mprogram.mMeta.name = "lili";
  mprogram.mMeta.color = "#AA0000FF";
  mprogram.mMeta.opacity = 0.9;

  Env environment(nullptr, &maze, &menv);
  Broker broker(nullptr, parser.value(portOption),
                &environment, &maze, &mprogram, &menv);

  QQmlApplicationEngine engine;
  qRegisterMetaType<uint8_t>("uint8_t");
  qRegisterMetaType<maze_cell_t>("maze_cell_t");
  qRegisterMetaType<MouseCfg>("MouseCfg");
  qRegisterMetaType<MouseState>("MouseState");
  qRegisterMetaType<MotorState>("MotorState");
  qRegisterMetaType<EnvState>("EnvState");

  engine.rootContext()->setContextProperty("_maze", &maze);
  engine.rootContext()->setContextProperty("_config", &config);
  engine.rootContext()->setContextProperty("_env", &environment);
  engine.rootContext()->setContextProperty("_m_env", &menv);
  engine.rootContext()->setContextProperty("_m_prog", &mprogram);
  engine.rootContext()->setContextProperty("_maze_texts", MazeTexts::getInstance());

  Q_INIT_RESOURCE(qml);
  const QUrl url(QStringLiteral("qrc:/main.qml"));
  QObject::connect(
      &engine, &QQmlApplicationEngine::objectCreated,
      &app, [url](QObject *obj, const QUrl &objUrl)
      {
      if (!obj && url == objUrl)
      QCoreApplication::exit(-1); },
      Qt::QueuedConnection);
  engine.load(url);

  return app.exec();
}
