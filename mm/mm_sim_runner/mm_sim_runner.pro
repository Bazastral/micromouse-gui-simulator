QT += core
QT += qml
QT += widgets

QT -= gui

CONFIG += c++17

TARGET = mm_sim_runner
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += main.cpp

DEFINES += QT_DEPRECATED_WARNINGS

INCLUDEPATH += ../mm_sim
DEPENDPATH  += ../mm_sim
# This ensures, that when library changes, and this is incremental build, then
# target will contain the latest library
PRE_TARGETDEPS += ../mm_sim

#include what libraries we have to link with
#include( ../mm_sim/mm_sim_libs.pri )
LIBS += -L../mm_sim
LIBS += -lmm_sim
