
#include <math.h>
#include <stdio.h>

#include "encoder.h"
#include "logger.h"

Encoder::Encoder(uint32_t ticks_per_turn) : _ticks_per_turn(ticks_per_turn)
{
    _ticks = 0;
    _alpha = 0;
    // Logger::getInstance()->dbg("Encoder", "New encoder, resolution: " + QString::number(ticks_per_turn));
}

/**
 * @brief notify encoder that the wheel rotated, so that it can internally
 * calculate ticks properly
 *
 * @param rotation_radians
 */
void Encoder::update(double rotation_radians)
{
    double rad_per_tick = 2 * M_PI / _ticks_per_turn;

    _ticks += _ticks_per_turn * (rotation_radians + _alpha) / (2 * M_PI);
    _alpha = fmod(_alpha + rotation_radians, rad_per_tick);
    // Logger::getInstance()->dbg("Encoder",
    //                            QString("rotated %1, alpha %2, ticks %3")
    //                                .arg(rotation_radians, 10, 'f', 6, ' ')
    //                                .arg(_alpha, 10, 'f', 6, ' ')
    //                                .arg(_ticks, 10, 'f', 6, ' '));
}

/**
 * @brief Get how many ticks where since last call of this function
 *
 * @return uint32_t
 */
int32_t Encoder::get_ticks(void)
{
    int32_t ret = _ticks;
    // Logger::getInstance()->dbg("Encoder", "Get ticks: " + QString::number(_ticks));
    _ticks = 0;
    return ret;
}

int Encoder::setTicksPerTurn(uint32_t ticks_per_turn)
{
    if(ticks_per_turn == 0 || ticks_per_turn > 2000)
    {
        Logger::getInstance()->err("Encoder", "ticks per turn invalid");
        return -1;
    }
    Logger::getInstance()->inf("Encoder", "Setting ticks per turn to: " + QString::number(ticks_per_turn));
    _ticks_per_turn = ticks_per_turn;
    _ticks = 0;
    return 0;
}

uint32_t Encoder::getTicksPerTurn(void)
{
    return _ticks_per_turn;
}