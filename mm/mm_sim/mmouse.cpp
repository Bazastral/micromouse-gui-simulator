#include "mmouse.h"
#include "config.h"

MMouse::MMouse(QObject* parent) : QObject(parent)
{
    drive = new DiffDrive("unnamed", 10, 1000, 80);
    this->init();
    upGuiState();
    upGuiMeta();
    drive->setTargetSpeed(0.1, 0.1);
    // used 3 leds
    _leds_on.append(0);
    _leds_on.append(0);
    _leds_on.append(0);
}

MMouse::~MMouse(){
    while(mMeta.sensorsDist.length())
    {
        delete mMeta.sensorsDist.takeFirst();
    }
    delete drive;
}

void MMouse::setTargetSpeed(double left, double right)
{
    drive->setTargetSpeed(left, right);
}

void MMouse::upGuiState(){
    emit stateChanged();
}
void MMouse::upGuiMeta(){
    emit metaChanged();
}


int MMouse::init(){
    int ret = RET_OK;
    mMeta.width = 100;
    mMeta.length = 120;
    mMeta.name = "MouseName";
    mMeta.color = "#33AA33";
    mMeta.showBorder = true;
    mMeta.opacity = 0.7;

    double minlen = 0.0;
    double maxlen = 300;

    SensorDist* ds = new SensorDist();
    ds->setExists(false);
    ds->setXoffset(-40);
    ds->setYoffset(30);
    ds->setTrad(M_PI_2);
    ds->setMinDist(minlen);
    ds->setMaxDist(maxlen);
    mMeta.sensorsDist.append(ds);

    ds = new SensorDist();
    ds->setExists(false);
    ds->setXoffset(-25);
    ds->setYoffset(50);
    ds->setTrad(M_PI_2/2);
    ds->setMinDist(minlen);
    ds->setMaxDist(maxlen);
    mMeta.sensorsDist.append(ds);

    ds = new SensorDist();
    ds->setExists(false);
    ds->setXoffset(0.0);
    ds->setYoffset(54);
    ds->setTrad(0);
    ds->setMinDist(minlen);
    ds->setMaxDist(maxlen);
    mMeta.sensorsDist.append(ds);

    ds = new SensorDist();
    ds->setExists(false);
    ds->setXoffset(25);
    ds->setYoffset(50);
    ds->setTrad(-M_PI_2/2);
    ds->setMinDist(minlen);
    ds->setMaxDist(maxlen);
    mMeta.sensorsDist.append(ds);

    ds = new SensorDist();
    ds->setExists(false);
    ds->setXoffset(40);
    ds->setYoffset(30);
    ds->setTrad(-M_PI_2);
    ds->setMinDist(minlen);
    ds->setMaxDist(maxlen);
    mMeta.sensorsDist.append(ds);

    for(int i=0; i<mMeta.sensorsDist.length(); i++){
        sensorsDistances.append(maxlen);
    }
    sensorsEncoders.append(0.0);
    sensorsEncoders.append(0.0);


    // default initial state
    setState(MMouse::getInitialState());

    return ret;
}

void MMouse::showDistanceSensors(void){
    SensorDist *sDist;
    for(int i=0; i<mMeta.sensorsDist.length(); i++){
        sDist = (SensorDist *)mMeta.sensorsDist.at(i);
        sDist->setExists(true);
        sDist->updateGUI();
    }
}



int MMouse::setWheelsTrack(double track)
{
    return drive->setDistanceBetweenWheels(track);
}

int MMouse::setWheelsRadius(double radius)
{
    return drive->setRadius(radius);
}

int MMouse::setWheelsTicksPerTurn(double ticksPerTurn)
{
    return drive->setTicksPerTurn(ticksPerTurn);
}

double MMouse::getWheelsTrack(void)
{
    return drive->getDistanceBetweenWheels();
}
double MMouse::getWheelsRadius(void)
{
    return drive->getWheelsRadius();
}
double MMouse::getWheelsTicksPerTurn(void)
{
    return drive->getTicksPerTurn();
}
void MMouse::step(double time)
{
    MouseState s = getState();
    drive->step(time);
    s = getState();
    // s.dump(this->mMeta.name);
}

int32_t MMouse::getEncoderTicksLeft(void)
{
    return drive->getEncoderTicksLeft();
}

int32_t MMouse::getEncoderTicksRight(void)
{
    return drive->getEncoderTicksRight();
}

MouseState MMouse::getState()
{
    MouseState s;
    s.x = drive->getX();
    s.y = drive->getY();
    s.t = drive->getT();
    s.v = drive->getV();
    s.w = drive->getW();
    return s;
}

int MMouse::setState(MouseState state)
{
    if (
        (state.x < 0) ||
        (state.x > MAZE_DIM_SIDE_LEN) ||
        (state.y < 0) ||
        (state.y > MAZE_DIM_SIDE_LEN) ||
        ((state.t < -M_PI) && (state.t > M_PI)) ||
        (state.v < -MAX_VELOCITY_LINEAR) ||
        (state.v > MAX_VELOCITY_LINEAR) ||
        (state.w < -MAX_VELOCITY_ANGULAR) ||
        (state.w > +MAX_VELOCITY_ANGULAR)
        )
    {
        Logger::getInstance()->err(mMeta.name, "Set state invalid");
        Logger::getInstance()->err(mMeta.name, QString("x must be in <%1, %2>. Trying to set %3")
                                       .arg(0, 10, 'f', 3, ' ')
                                       .arg(MAZE_DIM_SIDE_LEN, 10, 'f', 3, ' ')
                                       .arg(state.x, 10, 'f', 3, ' '));
        Logger::getInstance()->err(mMeta.name, QString("y must be in <%1, %2>. Trying to set %3")
                                       .arg(0, 10, 'f', 3, ' ')
                                       .arg(MAZE_DIM_SIDE_LEN, 10, 'f', 3, ' ')
                                       .arg(state.y, 10, 'f', 3, ' '));
        Logger::getInstance()->err(mMeta.name, QString("t must be in (%1, %2). Trying to set %3")
                                       .arg(-M_PI, 10, 'f', 3, ' ')
                                       .arg(+M_PI, 10, 'f', 3, ' ')
                                       .arg(state.t, 10, 'f', 3, ' '));
        Logger::getInstance()->err(mMeta.name, QString("v must be in <%1, %2>. Trying to set %3")
                                       .arg(-MAX_VELOCITY_LINEAR, 10, 'f', 3, ' ')
                                       .arg(MAX_VELOCITY_LINEAR, 10, 'f', 3, ' ')
                                       .arg(state.v, 10, 'f', 3, ' '));
        Logger::getInstance()->err(mMeta.name, QString("w must be in <%1, %2>. Trying to set %3")
                                       .arg(-MAX_VELOCITY_ANGULAR, 10, 'f', 3, ' ')
                                       .arg(MAX_VELOCITY_ANGULAR, 10, 'f', 3, ' ')
                                       .arg(state.w, 10, 'f', 3, ' '));
        state.dump("invalid");
        return -1;
    }

    drive->setState(state.x, state.y, state.t, state.v, state.w);

    upGuiState();
    return 0;
}

int MMouse::setState(double x, double y, double t, double v, double w)
{
    MouseState s;
    s.x = x;
    s.y = y;
    s.t = t;
    s.v = v;
    s.w = w;
    return setState(s);
}

void MouseState::dump(QString name)
{
        Logger::getInstance()->dbg("mousestate " + name,
        QString("x %1 y %2 t %3 v %4 w %5")
        .arg(x, 10, 'f', 6, ' ')
        .arg(y, 10, 'f', 6, ' ')
        .arg(t, 10, 'f', 6, ' ')
        .arg(v, 10, 'f', 6, ' ')
        .arg(w, 10, 'f', 6, ' ')
        );
}

MouseState MMouse::getInitialState(void){
    MouseState s;
    s.x = MAZE_DIM_SQUARE_HALF + 0*MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF;
    s.y = MAZE_DIM_SQUARE_HALF + 0*MAZE_DIM_SQUARE + MAZE_DIM_WALL_HALF;
    s.t = qDegreesToRadians((double)90);
    s.v = 0;
    s.w = 0;
    return s;
}

int MMouse::setDistanceSensors(QJsonArray json)
{
    int ret = 0;
    uint8_t cnt = 0;
    SensorDist *sDist;

    for (uint8_t i = 0; i < mMeta.sensorsDist.length(); i++)
    {
        sDist = (SensorDist *)mMeta.sensorsDist.at(i);
        sDist->setExists(false);
        sDist->updateGUI();
    }

    foreach (const QJsonValue &v, json)
    {
        sDist = (SensorDist *)mMeta.sensorsDist.at(cnt);
        sDist->setExists(true);
        sDist->setXoffset(v["x"].toDouble());
        sDist->setYoffset(v["y"].toDouble());
        sDist->setTrad(v["t"].toDouble());
        sDist->setMinDist(v["min"].toDouble());
        sDist->setMaxDist(v["max"].toDouble());
        sDist->updateGUI();
        cnt++;
    }

    return ret;
}

QJsonArray MMouse::getDistanceSensors(void)
{
    QJsonArray arr;
    for (uint8_t i = 0; i < mMeta.sensorsDist.length(); i++)
    {
        QJsonObject cfg_one;
        SensorDist *sDist = (SensorDist *)mMeta.sensorsDist.at(i);
        if(sDist->getExists())
        {
            cfg_one["x"] = sDist->getXoffset();
            cfg_one["y"] = sDist->getYoffset();
            cfg_one["t"] = sDist->getTrad();
            cfg_one["min"] = sDist->getMinDist();
            cfg_one["max"] = sDist->getMaxDist();
            arr.push_back(cfg_one);
        }
    }
    return arr;
}

Position MouseState::toPosition(void)
{
    return Position(x, y, t);
}

Position MMouse::localToGlobal(const Position pos)
{
    MouseState state = getState();
    // TODO...
    // theta is in fact angle between OY on mouse and OX on the maze
    // it should be the same: OX here and here
    state.t -= M_PI_2;
    Position global = Utils::coordsLocalToGlobal(state.toPosition(), pos);
    global.setT(global.t() +  M_PI_2);
    return global;
}

void MMouse::buttonPressed(void)
{
    _button_is_down = true;
}
void MMouse::buttonReleased(void)
{
    _button_is_down = false;
}
bool MMouse::buttonIsBeingPushed(void)
{
    return _button_is_down;
}
void MMouse::setLedState(int led, int state)
{
    _leds_on[led] = state;
    emit ledsChanged();
}