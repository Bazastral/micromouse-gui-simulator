#ifndef BROKER_H
#define BROKER_H

#include <QTcpServer>
#include <QTcpSocket>
#include <QThread>

#include "env.h"
#include "maze.h"

class Broker : public QTcpServer
{
  Q_OBJECT
  public:
    Broker(QObject *parent=nullptr,
        QString port="57233",
        Env* env=nullptr,
        Maze* maze=nullptr,
        MMouse* mouse=nullptr,
        MMouse* mouse_env=nullptr
        );

    int getSenDistances(float *distances);
    int getSenEncoders(float *enc);
    int getSenGyro(float *gyro);
    int getSenAcc(float *acc);
    int getSenCompass(float *compass);
    int getBatteryLevel(float *battery);

  public slots:
    void processJson(const QJsonObject &json);

  private slots:
    void handleNewConnection(void);
    void clientDisconnected(void);

  private:
    QJsonObject processSet(const QJsonObject &json);
    QJsonObject processGet(const QJsonObject &json);
    void sendJson(const QJsonObject &json);
    void receiveData();

    QTcpSocket *client;

    char data[500];
    Env *env;
    Maze *maze;
    MMouse* mouse;
    MMouse* mouse_env;
    const QString NAME="Broker";
};




#endif // BROKER_H
