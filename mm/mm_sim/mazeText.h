#pragma once

#include <QObject>
#include <QPointF>
#include <QJsonArray>
#include <mutex>

#include "config.h"
#include "maze.h"


class mazeText : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QString text MEMBER _text NOTIFY textChanged);
    Q_PROPERTY(double x MEMBER _x NOTIFY xChanged);
    Q_PROPERTY(double y MEMBER _y NOTIFY yChanged);

public:
    mazeText(QString text, double x, double y):
     _text(text), _x(x), _y(y)
    {

    }

signals:
    void textChanged();
    void xChanged();
    void yChanged();

private:
    QString _text;
    double _x;
    double _y;
};


// this is a singleton
class MazeTexts : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QVariant pathModel READ getPathModel NOTIFY pathModelChanged);
    Q_PROPERTY(QVariant cellTextsModel READ getCellTextsModel NOTIFY cellTextsModelChanged);

public:
    MazeTexts(MazeTexts &other) = delete;
    void operator=(const MazeTexts &) = delete;

    static MazeTexts *getInstance(void);
    QVariant getPathModel(void);
    QVariant getCellTextsModel(void);

    /**
     * @brief Set the Path Plan From Json object
     *
     * @param in Json array with x and y positions
     * @return 0 for success
     */
    int setPathPlanFromJson(QJsonArray in);
    /**
     * @brief Clear path plan
     *
     * @return 0 for success
     */
    int resetPathPlan();



    /**
     * @brief Set Text in the middle of the cell
     *
     * @param cellx, celly, cell_id coords of the cell
     * @param text                  text to show
     * @return 0 for success
     */
    int setCellText(uint8_t cellx, uint8_t celly, QString text);
    int setCellText(maze_cell_id_t cell_id, QString text);
    /**
     * @brief Clear all texts in cells
     *
     * @return 0 for success
     */
    int resetAllCellTexts(void);


private:
    static MazeTexts * _instance;
    static std::mutex _mutex;
    QList<QObject*> _path_list;
    QList<QObject*> _cell_texts_list;

signals:
    void pathModelChanged();
    void cellTextsModelChanged();


protected:
    MazeTexts(void) { }
    ~MazeTexts() {}

};


