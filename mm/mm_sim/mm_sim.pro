TEMPLATE = lib

QT += core
QT += qml
QT += widgets

QT -= gui

CONFIG += c++17
CONFIG += staticlib

SOURCES += \
        broker.cpp \
        config.cpp \
        env.cpp \
        logger.cpp \
        maze.cpp \
        mmouse.cpp \
        sensorDist.cpp \
        utils.cpp \
        encoder.cpp \
        motor.cpp \
        drive.cpp \
        diffDrive.cpp \
        mazeText.cpp \

HEADERS += \
    broker.h \
    config.h \
    env.h \
    logger.h \
    maze.h \
    mmouse.h \
    sensorDist.h \
    utils.h \
    encoder.h \
    motor.h \
    drive.h \
    diffDrive.h \
    mazeText.h \


RESOURCES += qml/qml.qrc

RC_ICONS = pic/icon.ico
