#include "logger.h"
#include "config.h"
#include "qdebug.h"

Logger* Logger::_instance = nullptr;
std::mutex Logger::_mutex;

Logger::Logger()
{
    inf("Logger", "I'm ready!");
    dbg("Logger", "I'm ready!");
    wrn("Logger", "I'm ready!");
    err("Logger", "I'm ready!");
}

Logger* Logger::getInstance()
{
  std::lock_guard<std::mutex> lock(_mutex);
  if(!_instance)
  {
    _instance = new Logger();
  }
  return _instance;
}

void Logger::dbg(QString subject, QString msg){
    log(LOG_DBG, subject, msg);
}
void Logger::inf(QString subject, QString msg){
    log(LOG_INFO, subject, msg);
}
void Logger::wrn(QString subject, QString msg){
    log(LOG_WARN, subject, msg);
}
void Logger::err(QString subject, QString msg){
    log(LOG_ERR, subject, msg);
}

void Logger::log(log_enum_t t, QString subject, QString msg){

    if(t == LOG_DBG) return;

    QString type = "[" + levels[t].typeStr + "]";
    QString color = levels[t].color;
    while(type.length() < minTypeLen){type.append(" ");}
    QString sub = subject + ":";
    while(sub.length() < minSubjectLen){sub.append(" ");}

    qDebug().noquote() << color << type << sub << msg << colorDefault;
}

