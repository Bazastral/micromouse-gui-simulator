#pragma once

#include <string>
#include <string.h>

#include "drive.h"

/**
 * @brief Differential drive is build from two drives which are next to each other
 */

class DiffDrive
{
public:
    DiffDrive(std::string name = "", double radius = 100,
              double ticks_per_turn = 100, double distance_between_wheels = 100);
    ~DiffDrive();

    void step(double time = 0.1);
    void setTargetSpeed(double left, double right);
    int32_t getEncoderTicksLeft(void);
    int32_t getEncoderTicksRight(void);

    void setState(double x, double y, double t, double v, double w);
    int setRadius(double radius);
    double getWheelsRadius(void);
    int setDistanceBetweenWheels(double distance);
    double getDistanceBetweenWheels(void);
    int setTicksPerTurn(uint32_t ticks_per_turn);
    uint32_t getTicksPerTurn(void);

    double getX(void);
    double getY(void);
    double getT(void);
    double getV(void);
    double getW(void);

private:
    Drive *_drive_left, *_drive_right;
    double _x, _y, _t, _v, _w;
    double _dx, _dy, _dt;
    // construction parameter - distance between wheels
    double _D;
    std::string _name;
};

