#include <QPointF>

#include "mazeText.h"
#include "maze.h"
#include "logger.h"

using namespace std;

MazeTexts *MazeTexts::_instance{nullptr};
std::mutex MazeTexts::_mutex;

MazeTexts *MazeTexts::getInstance(void)
{
  std::lock_guard<std::mutex> lock(_mutex);
  if (_instance == nullptr)
  {
    _instance = new MazeTexts();
    // example how to use it manually
    // _instance->_path_list.append(
    //     new mazeText("sample text",
    //                  MAZE_DIM_SQUARE,
    //                  MAZE_DIM_SQUARE));
  }
  return _instance;
}
QVariant MazeTexts::getPathModel(void)
{
  return QVariant::fromValue(_path_list);
}
QVariant MazeTexts::getCellTextsModel(void)
{
  return QVariant::fromValue(_cell_texts_list);
}

int MazeTexts::setPathPlanFromJson(QJsonArray in)
{
  // clear the list
  while(_path_list.isEmpty() == false)
  {
    delete _path_list.first();
    _path_list.removeFirst();
  }
  // clear the UI
  emit pathModelChanged();

  // add data to list
  int i = 0;
  foreach (const QJsonValue &v, in)
  {
    QPointF point = QPointF(v["x"].toDouble(), v["y"].toDouble());
    _instance->_path_list.append(
      new mazeText(QString::number(i), point.x(), point.y()));
      i++;
  }
  // update UI
  emit pathModelChanged();
  return 0;
}

int MazeTexts::resetAllCellTexts(void)
{
  // clear the list
  while(_cell_texts_list.isEmpty() == false)
  {
    delete _cell_texts_list.first();
    _cell_texts_list.removeFirst();
  }
  // clear the UI
  emit cellTextsModelChanged();
  return 0;
}

int MazeTexts::setCellText(uint8_t cellx, uint8_t celly, QString text)
{
  maze_cell_id_t cell_id = {.x = cellx, .y = celly};
  if(cell_id.isValid() == false)
  {
    return -1;
  }
  Point p = cell_id.getMiddleCoords();
  mazeText *t = new mazeText(text, p.x, p.y);
  if(_cell_texts_list.contains(t) == true)
  {
    delete t;
  }
  else
  {
    _cell_texts_list.append(t);
    emit cellTextsModelChanged();
  }
  return 0;
}

