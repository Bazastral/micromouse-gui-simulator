#pragma once

#include <stdint.h>

class Encoder{
    public:
    Encoder(uint32_t ticks_per_turn = 500);

    /**
     * @brief notify encoder that the wheel rotated, so that it can internally
     * calculate ticks properly
     *
     * @param rotation_radians
     */
    void update(double rotation_radians);

    /**
     * @brief Get how many ticks where since last call of this function
     *
     * @return int32_t: minus means that the wheel rotated backwards
     */
    int32_t get_ticks(void);

    int setTicksPerTurn(uint32_t ticks_per_turn);
    uint32_t getTicksPerTurn(void);

    private:
    uint32_t _ticks_per_turn; // encoder resolution
    int32_t _ticks;           // cumulation of ticks that encoder did
    double _alpha;            // Remaining angle after calculating ticks. It is accumulated.
};