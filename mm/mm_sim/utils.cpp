#include "utils.h"

const double Utils::ORIENTATION_NORTH = M_PI/2;
const double Utils::ORIENTATION_SOUTH = -M_PI/2;
const double Utils::ORIENTATION_WEST = M_PI;
const double Utils::ORIENTATION_EAST = 0;

Utils::Utils()
{

}

void Utils::normalizeRadians(double *radians)
{
    if(radians != nullptr)
    {
        while (*radians > M_PI)
        {
            *radians -= 2 * M_PI;
        }
        while (*radians <= -M_PI)
        {
            *radians += 2 * M_PI;
        }
    }
}

double Utils::degreesToRadians(double degrees)
{
    return normalizeRadians(M_PI*degrees/180);
}
double Utils::radiansToDegrees(double radians)
{
    return 180*normalizeRadians(radians)/M_PI;
}

double Utils::normalizeRadians(double radians)
{
    double radiansNormalized = radians;
    Utils::normalizeRadians(&radiansNormalized);
    return radiansNormalized;
}

Position::Position(double x, double y, double t):
    _x(x), _y(y), _t(Utils::normalizeRadians(t))
{
}

Position::Position(Point point, double t):
    _x(point.x), _y(point.y), _t(Utils::normalizeRadians(t))
{
}

Point Position::ToPoint(void)
{
    Point p = {.x = _x, .y = _y};
    return p;
}

Position Utils::coordsLocalToGlobal(Position localSystemInGlobal, Position coordsInLocal)
{
    Position coordsInGlobal(
        localSystemInGlobal.x() +
        coordsInLocal.x() * qCos(localSystemInGlobal.t()) -
        coordsInLocal.y() * qSin(localSystemInGlobal.t()),
        localSystemInGlobal.y() +
        coordsInLocal.x() * qSin(localSystemInGlobal.t()) +
        coordsInLocal.y() * qCos(localSystemInGlobal.t()),
        Utils::normalizeRadians(localSystemInGlobal.t() + coordsInLocal.t()));

    return coordsInGlobal;
}

Position Utils::coordsSystemGlobalToLocal(Position localSystemInGlobal, Position coordsInGlobal)
{
    (void) localSystemInGlobal;
    (void) coordsInGlobal;
    // TODO in the future? Not needed now.
    return Position(0, 0, 0);
}

bool operator==(const Position& a, const Position& b)
{
    if(
        (a.x() == b.x())&&
        (a.y() == b.y())&&
        (a.t() == b.t())
    )
    {
        return true;
    }
    else return false;
}