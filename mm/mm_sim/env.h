#ifndef ENV_H
#define ENV_H

#include <QList>
#include <QObject>
#include <QTime>
#include <QtMath>
#include <QElapsedTimer>

#include "config.h"
#include "maze.h"
#include "mmouse.h"
#include "utils.h"

typedef struct EnvMeta{
  double step_s;
  double time_div;
} EnvMeta_t;

struct EnvState{
  Q_GADGET
    Q_PROPERTY(double time MEMBER time);
  Q_PROPERTY(int iter MEMBER iter);
  public:
  double time;
  int iter;
};

class Env : public QObject
{
  Q_OBJECT

  public:
    explicit Env(QObject *parent = nullptr,
        Maze* maze=nullptr,
        MMouse* mouse=nullptr);

    Q_PROPERTY(bool running READ running NOTIFY runningChanged);
    Q_PROPERTY(double time_div READ getTimeDiv NOTIFY time_divChanged);
    Q_PROPERTY(double targetSpeedLinear READ getTargetSpeedLinear NOTIFY targetSpeedChanged);
    Q_PROPERTY(double targetSpeedAngular READ getTargetSpeedAngular NOTIFY targetSpeedChanged);
    Q_PROPERTY(EnvState state READ getState NOTIFY stateChanged);

    //API
    Q_INVOKABLE int start(void);
    Q_INVOKABLE int stop(void);
    Q_INVOKABLE int reset(void);
    Q_INVOKABLE int step(void);
    Q_INVOKABLE int setTimeDiv(double td);
    Q_INVOKABLE double getTargetSpeedAngular(void);
    Q_INVOKABLE double getTargetSpeedLinear(void);
    Q_INVOKABLE void setTargetSpeedAngular(double speed);
    Q_INVOKABLE void setTargetSpeedLinear(double speed);
    int setMotorSpeed(double left, double right);
    int setStep(double s);
    int execute(double milliseconds);

    Q_INVOKABLE double getTimeDivMin(void);
    Q_INVOKABLE double getTimeDivMax(void);
    double getTimeDiv(void);
    int32_t getEncoderDataLeft();
    int32_t getEncoderDataRight();
    static double time_div_min;
    static double time_div_max;
    static int timer_resolution_ms;
    EnvState getState(void) { return eSt; }
    QList<double> getMouseSensorDistances(void);
    QJsonArray getMouseSensorDistancesJson(void);

    void initMState(MouseState * s);
    void getSensorMeasurements();

    /**
     * @brief Given maze construction and sensor position, measures distance from sensor to closest
     * wall and angle under which the sensor points the wall.
     *
     * @param maze              maze which is simulated
     * @param sensor_position   position of the distance sensor
     * @param sensor            configuration of the sensor
     * @param measurement       returned measurement. Distance in mm, angle in radians in range (0, M_PI_2]
     * @return int              zero for success
     */
    static int measureSensorDistance(const Maze *maze, const Position *sensor_position,
                                     const SensorDist *sensor, SensorDist::Measurement &measurement);
    //API end

    MMouse *mouse_env;

    public slots:
      void timeFlows(void);

signals:
    void runningChanged();
    void time_divChanged();
    void targetSpeedChanged();
    void stateChanged();

  private:

    EnvMeta_t eMeta;
    EnvState eSt;
    double m_tx, m_ty;

    Maze   *m_maze;
    MouseValues_t m_val;

    bool m_running;
    bool running(void){return m_running;};

    int init(void);
    void upGui();

    void getDistances();
    void getMeasurementsEncoders();

    static bool IsSensorDistanceInsideExistingWall(const Maze *maze, const Position *sensor_position);

    void log_info(QString);
    void log_dbg(QString);
    void log_warn(QString);
    void log_error(QString);

    QElapsedTimer real_time_elapsed_timer = QElapsedTimer();
    const QString NAME = "env";
    const double SPEED_LINEAR_MAX = 1.0;
    const double SPEED_ANGULAR_MAX = 1.0;

};


#endif // ENV_H



