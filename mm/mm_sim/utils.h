#ifndef UTILS_H
#define UTILS_H

#include <QObject>

struct Point
{
    double x;
    double y;
};

class Position
{
public:
    Position(double x, double y, double t);
    Position(Point point, double t);
    Point ToPoint(void);
    double x(void) const { return _x; }
    double y(void) const { return _y; }
    double t(void) const { return _t; }
    void setT(double t) { _t = t; }

private:
    double _x, _y, _t;
};
bool operator==(const Position&, const Position&);


class Utils
{
public:
    Utils();

    /**
     * @brief Normalizes radians to be in range (-pi ... +pi]
     * 0 means pointing to East. PI/2 means pointing to North
     *
     * @param radians
     */
    static void normalizeRadians(double *radians);
    static double normalizeRadians(double radians);

    /**
     * @brief Useful degrees<->radians conversions
     */
    static double degreesToRadians(double degrees);
    static double radiansToDegrees(double radians);

    /**
     * @brief Positions in global vs local coordinate system
     *
     * @param localSystemInGlobal position of the (0,0,0) local system in the global system
     * @param coordsInLocal       position of the object in local coordinate system
     * @param coordsInGlobal      position of the object in the global coordinate system
     * @return Position in the apropriate system
     */
    static Position coordsLocalToGlobal(Position localSystemInGlobal, Position coordsInLocal);
    static Position coordsSystemGlobalToLocal(Position localSystemInGlobal, Position coordsInGlobal);

    static const double ORIENTATION_NORTH;
    static const double ORIENTATION_SOUTH;
    static const double ORIENTATION_WEST;
    static const double ORIENTATION_EAST;
};

#endif // UTILS_H
