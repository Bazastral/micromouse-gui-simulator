#include "sensorDist.h"
#include "config.h"


SensorDist::SensorDist(double x, double y, double t, double min, double max)
{
    m_exists = true;
    m_x_offset = x;
    m_y_offset = y;
    m_t_deg = qRadiansToDegrees(t);
    m_min_dist = min;
    m_max_dist = max;
    dump("New sensor with configuration set");
}

SensorDist::SensorDist()
{
}

SensorDist::~SensorDist()
{
}

void SensorDist::setMinDist(double d){
    if(m_max_dist && m_max_dist<=d){
      Logger::getInstance()->wrn(NAME, "Min dist is bigger than max! Min: "+
                  QString::number(d) + ", max: " +
                  QString::number(m_max_dist));
    }
    else if(d<0){
      Logger::getInstance()->err(NAME, "Min dist smaller than 0. Setting to 0.");
        m_min_dist = 0;
        return;
    }
    m_min_dist = d;
}

void SensorDist::setMaxDist(double d){
    if(m_min_dist && m_min_dist >= d){
      Logger::getInstance()->wrn(NAME, "Max dist is bigger than max! Min: "+
                  QString::number(m_min_dist) + ", max: " +
                  QString::number(d));
    }
    else if(d<0){
      Logger::getInstance()->err(NAME, "Max dist smaller than 0. Setting to 100.");
        m_max_dist = 100;
        return;
    }
    m_max_dist = d;
}


void SensorDist::dump(QString description)
{
  Logger::getInstance()->dbg(NAME, description +
              " x="+ QString::number(m_x_offset) +
              ", y="+ QString::number(m_y_offset) +
              ", t="+ QString::number(m_t_deg) + "'");

}

Position SensorDist::getPositionOffset(void)
{
  return Position(m_x_offset, m_y_offset, Utils::degreesToRadians(m_t_deg));
}