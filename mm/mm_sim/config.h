#ifndef CONFIG_H
#define CONFIG_H

#include "QString"
#include <qobject.h>
#include "utils.h"

#define RET_OK 0
#define RET_ERR -1
#define RET_WARN -2

#define MAZE_N                 16
#define MAZE_N_DEF             16
#define MAZE_DIM_SQUARE        ((double)180)
#define MAZE_DIM_SQUARE_HALF   (MAZE_DIM_SQUARE/2)
#define MAZE_DIM_WALL          ((double)12)
#define MAZE_DIM_WALL_HALF     (MAZE_DIM_WALL/2)
#define MAZE_DIM_CORRIDOR      (MAZE_DIM_SQUARE - MAZE_DIM_WALL)
#define MAZE_DIM_CORRIDOR_HALF (MAZE_DIM_CORRIDOR/2)
#define MAZE_DIM_SIDE_LEN      (MAZE_N * MAZE_DIM_SQUARE + MAZE_DIM_WALL)

#define MASK_N 0x01
#define MASK_E 0x02
#define MASK_S 0x04
#define MASK_W 0x08
#define MASK_ALL 0x0f
#define MASK_visited 0x10

#define MAX_SENSOR_NUM 8
#define MOUSE_NAME_MAX_LEN 10
#define MOUSE_MAX_LEN ((double)MAZE_DIM_SQUARE*0.9)
#define MOUSE_MAX_WID ((double)MAZE_DIM_SQUARE*0.9)

typedef enum{
  DIR_NONE = 0,
  DIR_NORTH = MASK_N,
  DIR_EAST = MASK_E,
  DIR_SOUTH = MASK_S,
  DIR_WEST = MASK_W,
  DIR_A = MASK_ALL,
} dir_t;

class Config : public QObject
{
  Q_OBJECT
    Q_PROPERTY(int _MAZE_N MEMBER maze_n CONSTANT);
  Q_PROPERTY(double _MAZE_DIM_SQUARE MEMBER maze_dim_square CONSTANT);
  Q_PROPERTY(double _MAZE_DIM_WALL MEMBER maze_dim_wall CONSTANT);
  Q_PROPERTY(int _MASK_N MEMBER mask_n CONSTANT);
  Q_PROPERTY(int _MASK_S MEMBER mask_s CONSTANT);
  Q_PROPERTY(int _MASK_E MEMBER mask_e CONSTANT);
  Q_PROPERTY(int _MASK_W MEMBER mask_w CONSTANT);
  Q_PROPERTY(int _MASK_A MEMBER mask_a CONSTANT);

  public:
  explicit Config(QObject *parent = nullptr);

  private:
  int maze_n = MAZE_N;
  double maze_dim_square = MAZE_DIM_SQUARE;
  double maze_dim_wall = MAZE_DIM_WALL;
  int mask_n = MASK_N;
  int mask_s = MASK_S;
  int mask_e = MASK_E;
  int mask_w = MASK_W;
  int mask_a = MASK_ALL;
  const QString NAME="config";
};

const Point StartingPoint = {
   .x = MAZE_DIM_CORRIDOR_HALF + MAZE_DIM_WALL,
   .y = MAZE_DIM_CORRIDOR_HALF + MAZE_DIM_WALL
};

#endif // CONFIG_H
