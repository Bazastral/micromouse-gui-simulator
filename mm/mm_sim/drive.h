#pragma once

#include <string>
#include <string.h>

#include "motor.h"
#include "encoder.h"

/**
 * @brief Drive is in fact a motor with encoder and a wheel
 */

class Drive{
public:
    Drive(std::string name="", double radius=0.1, uint32_t ticks_per_turn=100);
    virtual ~Drive();

    /**
     * @brief Makes one step in simulation
     *
     * @param time in seconds
     * @return double - distance the wheel did in this step
     */
    virtual double step(double time=0.1);

    void setTargetSpeed(double speed);
    int32_t getTicks(void);
    double getTotalDistance(void);
    void setSpeed(double speed);

    int setRadius(double radius);
    double getRadius(void);
    int setTicksPerTurn(uint32_t ticks_per_turn);
    uint32_t getTicksPerTurn(void);

private:
    Motor * _motor;
    Encoder *_encoder;
    double _radius;
    std::string _name;
    const double RADIUS_MAX = 200;
    const double RADIUS_MIN = 5;
};

