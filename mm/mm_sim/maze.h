#ifndef MAZE_H
#define MAZE_H

#include <QObject>
#include <QDir>
#include <QUrl>

#include "config.h"

typedef uint8_t maze_cell_t;
typedef maze_cell_t maze_t[MAZE_N][MAZE_N];

bool cellHasWalls(const maze_cell_t, const dir_t);

struct maze_cell_id
{
    uint8_t x;
    uint8_t y;
    bool isValid(void) const;
    struct maze_cell_id getNeighborCell(const dir_t) const;
    double getWallCoord(const dir_t, const bool) const;
    double getWallCoordOutside(const dir_t) const;
    double getWallCoordInside(const dir_t) const;
    Point getMiddleCoords(void) const;
};
typedef struct maze_cell_id maze_cell_id_t;


class Maze : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl mazefiles MEMBER m_mazefiles CONSTANT);

public:
    explicit Maze(QObject *parent = nullptr);

    Q_INVOKABLE int save(QUrl name);
    Q_INVOKABLE int load(QUrl name);
    int load(QString name, QJsonObject *ret);
    int load(QString name);
    int setPathPlanFromJson(QJsonArray in);

    Q_INVOKABLE maze_cell_t getCellState(uint8_t cellx, uint8_t celly) const;
    maze_cell_t getCellState(maze_cell_id_t id) const;
    static maze_cell_id_t getCellIdfromCoords(double realx, double realy);
    static maze_cell_id_t getCellIdfromCoords(Position x);
    QString cellStateToString(maze_cell_t cell);
    Q_INVOKABLE bool currMazeValid();
    Q_INVOKABLE void currMazePrint();
    Q_INVOKABLE void currMazeSetWall(uint8_t x, uint8_t y, uint8_t NSEW);
    Q_INVOKABLE void currMazeResetWall(uint8_t x, uint8_t y, uint8_t NSEW);
    Q_INVOKABLE void currMazeResetAll();
    Q_INVOKABLE void currMazesetAll();
    void changeWallMouseKnowledge(uint8_t x, uint8_t y, uint8_t NSEW, int exists);
    static QString maskToNSEW(maze_cell_t cellTmp);

signals:
    void mazeChanged();
    void mazeChangedMouseKnowledge(int x, int y, int NSEW, int exists);
    void mazeClearMouseKnowledge();

private:
    int loadFullName(QString);
    bool isValid(maze_t mazeTmp);
    void overrideCell(uint8_t x, uint8_t y, maze_cell_t cell);
    void setMaze(maze_t newMaze);
    void getMaze(maze_t mazeTmp);
    void printMaze(maze_t mazeTmp);
    void setWall(maze_t mazeTmp, uint8_t x, uint8_t y, uint8_t NSEW);
    void resetWall(maze_t mazeTmp, uint8_t x, uint8_t y, uint8_t NSEW);
    void resetAll(maze_t mazeTmp);
    void setAll(maze_t mazeTmp);
    bool suitsChallengeRequirements(maze_t mazeTmp);
    void printInHex(maze_t maze);

    const char char_stick = 'o';
    const char char_wall_hori = '-';
    const char char_wall_vert = '|';

    maze_t m_maze;
    QString m_path_basic_folder;
    QUrl m_mazefiles;
    QString NAME = "maze";
};

#endif // MAZE_H

