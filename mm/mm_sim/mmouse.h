#ifndef MMOUSE_H
#define MMOUSE_H


#include <QObject>
#include <QJsonArray>
#include <QJsonObject>
#include <QString>

#include "config.h"
#include "sensorDist.h"
#include "diffDrive.h"


#define M_SENSOR_N 5

struct MotorState{
    Q_GADGET

    Q_PROPERTY(double sAll MEMBER sAll);
    Q_PROPERTY(double sStep MEMBER sStep);
    Q_PROPERTY(double a MEMBER a);
    Q_PROPERTY(double aStep MEMBER aStep);
    Q_PROPERTY(double w MEMBER w);

    public:
    double sAll;        // distance driven. Decreases when driving back
    double sStep;       // distance driven in last step
    double a;           // current wheel position (alpha in rad) between 0 and 2PI
    double aStep;       // change of position in last step (may be bigger then 2PI)
    double w;           // current angular velocity
};

struct MouseState{

    Q_GADGET
    Q_PROPERTY(double x MEMBER x);
    Q_PROPERTY(double y MEMBER y);
    Q_PROPERTY(double t MEMBER t);
    Q_PROPERTY(double v MEMBER v);
    Q_PROPERTY(double w MEMBER w);

    public:
    double x;
    double y;
    double t;
    double v;
    double w;
    void dump(QString name);
    Position toPosition(void);
};



typedef struct MouseValues{
    double pwmL;
    double pwmR;
} MouseValues_t;

struct MouseCfg{
    Q_GADGET

    Q_PROPERTY(double width MEMBER width);
    Q_PROPERTY(double length MEMBER length);
    Q_PROPERTY(double opacity MEMBER opacity);
    Q_PROPERTY(QString color MEMBER color);
    Q_PROPERTY(QString name MEMBER name);
    Q_PROPERTY(bool showBorder MEMBER showBorder);
    Q_PROPERTY(QList<QObject*> sensorsDist MEMBER sensorsDist);

    public:
    double width;
    double length;
    bool showBorder;
    double opacity;
    QString color;
    QList<QObject*> sensorsDist;
    QString name;

};


class MMouse : public QObject
{
    Q_OBJECT

    Q_PROPERTY(MouseState state READ getState NOTIFY stateChanged);
    Q_PROPERTY(MouseCfg mMeta READ getmMeta NOTIFY metaChanged);
    Q_PROPERTY(QList<double> mouseSensorsDist MEMBER sensorsDistances NOTIFY stateChanged);
    Q_PROPERTY(QList<bool> leds READ getLeds NOTIFY ledsChanged);

    public:
    ~MMouse();
    MMouse(QObject* parent);

    //update position of the mouse in GUI
    void upGuiState();
    //update appearence of the mouse in GUI
    void upGuiMeta();
    void setTargetSpeed(double left, double right);
    void step(double time);

    /**
     * @brief distance between wheels
     */
    int setWheelsTrack(double track);
    Q_INVOKABLE double getWheelsTrack(void);

    /**
     * @brief Set radius of the wheels
     */
    int setWheelsRadius(double radius);
    Q_INVOKABLE double getWheelsRadius(void);

    /**
     * @brief Set ticks per turn of the wheels
     * Ticks per turn mean how many encoder ticks will occur wheel of the robot will fully rotate (360')
     */
    int setWheelsTicksPerTurn(double ticksPerTurn);
    Q_INVOKABLE double getWheelsTicksPerTurn(void);

    /**
     * @brief Enable pushing and releasing button from qml
     */
    Q_INVOKABLE void buttonPressed(void);
    Q_INVOKABLE void buttonReleased(void);
    bool buttonIsBeingPushed(void);

    /**
     * @brief Change configuration of distance sensors
     * Delete current configuration, create new
     *
     * @param json Array with configuration
     * @return int 0 for success, other otherwise
     */
    int setDistanceSensors(QJsonArray json);
    QJsonArray getDistanceSensors(void);

    /**
     * @brief Show distance sensors in GUI
     */
    void showDistanceSensors(void);

    /**
     * @brief Get the global position of an object which is located on the mouse
     */
    Position localToGlobal(const Position);

    int32_t getEncoderTicksLeft(void);
    int32_t getEncoderTicksRight(void);

    //current state variables
    QList<double> sensorsDistances;
    QList<int32_t> sensorsEncoders;

    DiffDrive *drive;
    MouseCfg mMeta;
    MouseCfg getmMeta(){return mMeta;}
    MouseState getState();
    QList<bool> getLeds() { return _leds_on;}
    void setLedState(int led, int state);
    int setState(MouseState state);
    int setState(double x, double y, double t, double v, double w);

    static MouseState getInitialState(void);

signals:
    void stateChanged();
    void metaChanged();
    void ledsChanged();

private:
    int init();
    // TODO set and get
    QString name;
    const double MAX_VELOCITY_LINEAR = 5000;
    const double MAX_VELOCITY_ANGULAR = 10*2*M_PI;
    bool _button_is_down = false;
    QList<bool> _leds_on;
};

#endif // MOUSESOFT_H
