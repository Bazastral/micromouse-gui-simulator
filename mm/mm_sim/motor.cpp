#include <string>

#include "motor.h"
#include "logger.h"

using namespace std;

Motor::Motor(std::string name, double T, double K) :
                                                _T(T),
                                                _K(K),
                                                _name(name)
{
    Logger::getInstance()->inf("motor", "New. " + QString::fromStdString(_name) +
                                            QString(" T %1, K %2")
                                            .arg(_T, 10, 'f', 6, ' ')
                                            .arg(_K, 10, 'f', 6, ' '));
    _w = 0;
    _goal_throttle = 0;
    _total_distance = 0;
}


double Motor::step(double time)
{
    _w = (_T * _w + _K * _goal_throttle * time) / (time + _T);
    _total_distance += _w * time;
    // Logger::getInstance()->dbg("motor", QString("rotated angle %1, total distance angle %2 step %3")
    //                                         .arg(_w * time, 10, 'f', 6, ' ')
    //                                         .arg(_total_distance, 10, 'f', 6, ' ')
    //                                         .arg(time, 10, 'f', 6, ' '));
    return _w * time;
    // TODO test this return
    // stNext.mL.aStep = step_s * stNow.mL.w;
    // stNext.mL.a = std::fmod(stNow.mL.a + stNext.mL.aStep, 2*M_PI);
    // stNext.mL.sStep = mouse_env->mMeta.driveL.r * stNext.mL.aStep;
    // stNext.mL.sAll += stNext.mL.sStep;

}

double Motor::getRotationVelocity(void)
{
    return _w;
}

void Motor::setTargetSpeed(double speed)
{
    _goal_throttle = speed;
}

double Motor::getMaxPossibleSpeed(void)
{
    return _K;
}

double Motor::getTotalDistance(void)
{
    return _total_distance;
}

void Motor::setRotationVelocity(double w)
{
    _w = w;
}