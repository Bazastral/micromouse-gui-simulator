#include <QtNetwork>

#include "broker.h"
#include "config.h"
#include "mmouse.h"
#include "mazeText.h"



Broker::Broker(QObject* parent, QString tcpPort, Env* env, Maze* maze, MMouse* mouse, MMouse* mouse_env) :
  QTcpServer(parent),
  env(env),
  maze(maze),
  mouse(mouse),
  mouse_env(mouse_env)
{
  const int port = tcpPort.toInt();
  if(port < 50000 || port > 65535)
  {
    Logger::getInstance()->err(NAME, "Wrong port value! " + tcpPort);
    Logger::getInstance()->err(NAME, "Must be > 50000 && < 65535");
    exit(1);
    return;
  }
  setMaxPendingConnections(2);
  client = 0;

  if (!listen(QHostAddress::Any, port)) {
    Logger::getInstance()->err(NAME, "Unable to start the server");
    exit(1);
    return;
  }

  QString ipAddress;
  QList<QHostAddress> ipAddressesList = QNetworkInterface::allAddresses();
  // use the first non-localhost IPv4 address
  for (int i = 0; i < ipAddressesList.size(); ++i) {
    if (ipAddressesList.at(i) != QHostAddress::LocalHost &&
        ipAddressesList.at(i).toIPv4Address()) {
      ipAddress = ipAddressesList.at(i).toString();
      break;
    }
  }
  // if we did not find one, use IPv4 localhost
  if (ipAddress.isEmpty())
    ipAddress = QHostAddress(QHostAddress::LocalHost).toString();
  Logger::getInstance()->dbg(NAME, tr("The server is running on\nIP: %1\nport: %2" )
      .arg(ipAddress).arg(serverPort()));

  connect(this, &Broker::newConnection, this, &Broker::handleNewConnection);
}



void Broker::receiveData()
{
  int ret;
  int tries = 10;
  int received = 0;
  QByteArray jsonData;


  while(--tries)
  {
    ret = client->read(data, sizeof(data)-1); //remember to add null at end
    if(ret == 0)
    {
      Logger::getInstance()->dbg(NAME, "no data in socket");
      return;
    }
    if(ret < 0)
    {
    Logger::getInstance()->dbg(NAME, 
        "invalid data, socket closed? code: " + QString::number(ret));
      emit client->disconnected();
      return;
    }
    received += ret;
    data[ret] = 0; //null termination
    jsonData.append(data);

    //check if json is valid
    QJsonParseError parseError;
    const QJsonDocument jsonDoc = QJsonDocument::fromJson(jsonData, &parseError);
    if (parseError.error == QJsonParseError::NoError)
    {
      if (jsonDoc.isObject()) // and is a JSON object
      {
        processJson(jsonDoc.object());
        return;
      }
      else
      {
        Logger::getInstance()->dbg(NAME, "Invalid message: " + QString::fromUtf8(jsonData));
      }
    }
    else
    {
      Logger::getInstance()->dbg(NAME, "Invalid message: " + QString::fromUtf8(jsonData));
      Logger::getInstance()->dbg(NAME, "Parse error: " + parseError.errorString());
    }
  }
}

void Broker::handleNewConnection(void)
{
  Logger::getInstance()->dbg(NAME, "new client");
  if(client)
  {
    Logger::getInstance()->dbg(NAME, "client exists, scheduling retry");
    QTimer::singleShot(100, this, &Broker::handleNewConnection);
    return;
  }
  if(!hasPendingConnections())
  {
    Logger::getInstance()->dbg(NAME, "no pending connectinons");
    return;
  }

  client = nextPendingConnection();
  connect(client, &QTcpSocket::readyRead, this, &Broker::receiveData);
  connect(client, &QTcpSocket::disconnected, this, &Broker::clientDisconnected);
  receiveData();
}


void Broker::clientDisconnected(void)
{
  Logger::getInstance()->dbg(NAME, "client disconnected");
  if(client)
  {
    client->deleteLater();
    client = 0;
  }
  else
  {
    Logger::getInstance()->dbg(NAME, "client did not exist?");
  }
}


QJsonObject Broker::processGet(const QJsonObject &json)
{
  QJsonObject ret;

  ret["type"] = "resp_get";
  ret["result"] = "success";

  QJsonArray arr;
  if(json.contains("param"))
  {
    QJsonValue param = json["param"];
    ret["param"] = param;
    if(param == "alive")
    {
      ret["value"] = 1;
    }
    else if(param == "motor_ticks")
    {
      arr.push_back(reinterpret_cast<int>(env->getEncoderDataLeft()));
      arr.push_back(reinterpret_cast<int>(env->getEncoderDataRight()));
      ret["value"] = arr;
    }
    else if(param == "state")
    {
      MouseState s = env->mouse_env->getState();
      QJsonObject state;
      state["x"] = s.x;
      state["y"] = s.y;
      state["t"] = s.t;
      state["v"] = s.v;
      state["w"] = s.w;
      ret["value"] = state;
    }
    else if(param == "dist_sensors")
    {
      ret["value"] = env->getMouseSensorDistancesJson();
    }
    else if(param == "config")
    {
      QJsonObject config;
      config["track"] = mouse_env->getWheelsTrack();
      config["radius"] = mouse_env->getWheelsRadius();
      config["ticksPerTurn"] = mouse_env->getWheelsTicksPerTurn();
      config["distanceSensors"] = mouse_env->getDistanceSensors();

      ret["value"] = config;
    }
    else if(param == "button_pressed")
    {
      ret["value"] = mouse_env->buttonIsBeingPushed();
    }
    else
    {
      Logger::getInstance()->err(NAME, "Invalid param to get: "+param.toString());
      ret["result"] = "Unknown parameter \"" + param.toString() + "\"";
    }
  }
  else
  {
    Logger::getInstance()->dbg(NAME, "json must contain field \"param\"");
    ret["result"] = "Json must containg \"param\" field";
  }

  return ret;
}

QJsonObject Broker::processSet(const QJsonObject &json)
{
  QJsonObject ret;
  QJsonValue param;
  int err = 0;
  /*
  TODO
  Generally it would be better to expose API from this class to register a 'key' and a callback to call
  when some client wants to change that 'key' value. But for now... this is simple implementation
  which works and is 'good enough'
  */

  ret["type"] = "resp_set";
  ret["result"] = "success";

  param = json.value(QString("param"));
  if(param == "map_name")
  {
    maze->load(json.value("value").toString(), &ret);
    // map changed, so the readings must be updated
    env->getSensorMeasurements();
  }
  else if(param == "path_plan")
  {
    QJsonArray arr = json.value("value").toArray();
    err = MazeTexts::getInstance()->setPathPlanFromJson(arr);
  }
  else if(param == "cell_text")
  {
    QJsonObject o = json.value("value").toObject();
    err = MazeTexts::getInstance()->setCellText(
        o["x"].toInt(),
        o["y"].toInt(),
        o["text"].toString());
  }
  else if(param == "notify_wall")
  {
    QJsonObject o = json.value("value").toObject();
    maze->changeWallMouseKnowledge(
        o["x"].toInt(),
        o["y"].toInt(),
        o["NSEW"].toInt(),
        o["exists"].toInt()
        );
  }
  else if(param == "notify_wall_clear_all")
  {
    maze->mazeClearMouseKnowledge();
  }
  else if(param == "clear_cell_texts")
  {
    err = MazeTexts::getInstance()->resetAllCellTexts();
  }
  else if(param == "motor_speed")
  {
    QJsonObject val = json.value("value").toObject();
    err = env->setMotorSpeed(
        val["left"].toDouble(),
        val["right"].toDouble()
        );
  }
  else if(param == "execute")
  {
    // milliseconds
    err = env->execute(json.value("value").toDouble());
  }
  else if(param == "led")
  {
    env->mouse_env->setLedState(
        json["value"]["id"].toInt(),
        json["value"]["on"].toInt());
    err = 0;
  }
  else if(param == "state" || param == "state_notify")
  {
    MouseState st = MMouse::getInitialState();
    if(json["value"].toObject().contains("x"))
    { st.x = json["value"]["x"].toDouble(); }
    if(json["value"].toObject().contains("y"))
    { st.y = json["value"]["y"].toDouble(); }
    if(json["value"].toObject().contains("t"))
    { st.t = json["value"]["t"].toDouble(); }
    if(json["value"].toObject().contains("v"))
    { st.v = json["value"]["v"].toDouble(); }
    if(json["value"].toObject().contains("w"))
    { st.w = json["value"]["w"].toDouble(); }

    if(param == "state")
    {
      err = env->mouse_env->setState(st);
      // state changed, so the readings must be updated
      env->getSensorMeasurements();
    }
    else if(param == "state_notify")
    {
      err = mouse->setState(st);
    }
  }
  else if(param == "config")
  {
    if(json["value"].toObject().contains("track") && err == 0)
    {
      err = mouse->setWheelsTrack(json["value"]["track"].toDouble());
      err = mouse_env->setWheelsTrack(json["value"]["track"].toDouble());
    }

    if(json["value"].toObject().contains("radius") && err == 0)
    {
      err = mouse->setWheelsRadius(json["value"]["radius"].toDouble());
      err = mouse_env->setWheelsRadius(json["value"]["radius"].toDouble());
    }

    if(json["value"].toObject().contains("ticksPerTurn") && err == 0)
    {
      err = mouse->setWheelsTicksPerTurn(json["value"]["ticksPerTurn"].toDouble());
      err = mouse_env->setWheelsTicksPerTurn(json["value"]["ticksPerTurn"].toDouble());
    }

    if(json["value"].toObject().contains("distanceSensors") && err == 0)
    {
      err = mouse_env->setDistanceSensors(json["value"]["distanceSensors"].toArray());
      // sensors changed, so the readings must be updated
      env->getSensorMeasurements();
    }

    mouse->metaChanged();
    mouse_env->metaChanged();
  }
  else {
    QString s;
    s = "invalid param name: "+json.value("param").toString();
    Logger::getInstance()->err(NAME, s);
    ret["result"] = s;
  }

  if(err != 0)
  {
    Logger::getInstance()->err(NAME, "some error while processing 'set' command");
    ret["result"] = "some error while setting value: "
      + json.value("value").toString();
  }

  return ret;
}


void Broker::processJson(const QJsonObject &json)
{
  QJsonObject json_ret;
  Logger::getInstance()->dbg(NAME, "<<" +
  QString(QJsonDocument(json).toJson(QJsonDocument::Compact).toStdString().c_str()));

  if(json.contains(QString("type")))
  {
    if(json.value(QString("type")) == "get") { json_ret = processGet(json); }
    else if(json.value(QString("type")) == "set") { json_ret = processSet(json); }
    else
    {
      Logger::getInstance()->dbg(NAME, "json does not support values other than type = get|set");
      json_ret["result"] = "json does not support values other than type = get|set";
    }
  }
  else
  {
    Logger::getInstance()->dbg(NAME, "json must contain field \"type\"");
    json_ret["result"] = "json must contain field \"type\"";
  }

  sendJson(json_ret);
}

void Broker::sendJson(const QJsonObject &json)
{
  Q_UNUSED(json);
  QByteArray send = QJsonDocument(json).toJson();
  client->write(send);
  client->waitForBytesWritten();
  Logger::getInstance()->dbg(NAME, ">>" + 
  QString(QJsonDocument(json).toJson(QJsonDocument::Compact).toStdString().c_str()));
}



int Broker::getSenDistances(float *distances){
  Q_UNUSED(distances);
  //todo
  return RET_ERR;
}
int Broker::getSenEncoders(float *enc){
  Q_UNUSED(enc);
  //todo
  return RET_ERR;

}
int Broker::getSenGyro(float *gyro){
  Q_UNUSED(gyro);
  //todo
  return RET_ERR;

}
int Broker::getSenAcc(float *acc){
  Q_UNUSED(acc);
  //todo
  return RET_ERR;

}
int Broker::getSenCompass(float *compass){
  Q_UNUSED(compass);
  //todo
  return RET_ERR;

}
int Broker::getBatteryLevel(float *battery){
  Q_UNUSED(battery);
  //todo
  return RET_ERR;
}


