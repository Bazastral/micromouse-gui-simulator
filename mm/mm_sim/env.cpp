
#include <QThread>
#include <qtimer.h>
#include <QElapsedTimer>
#include <QTime>

#include "env.h"
#include "config.h"
#include "QtMath"

using namespace std;


Env::Env(QObject *parent,
         Maze *maze,
         MMouse* mouse) :
        QObject(parent),
        mouse_env(mouse),
        m_maze(maze)
{
    this->init();
    QTimer *timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &Env::timeFlows);
    timer->start(timer_resolution_ms);
    mouse_env->showDistanceSensors();

    return;
}

void Env::timeFlows(){
    if(m_running)
    {
        double realtime_milliseconds_elapsed = real_time_elapsed_timer.restart();
        double execute_milliseconds = realtime_milliseconds_elapsed*eMeta.time_div;
        Logger::getInstance()->dbg(NAME, "elapsed ms in reality: " + QString::number(realtime_milliseconds_elapsed)
        + " so after divider simulate ms: " +QString::number(eMeta.time_div));
        execute(execute_milliseconds);
    }
}

int Env::start(void){
    m_running = true;
    real_time_elapsed_timer.restart();
    emit runningChanged();
    return RET_OK;
}
int Env::stop(void){
    m_running = false;
    emit runningChanged();
    return RET_OK;
}
int Env::reset(void){
  Logger::getInstance()->inf(NAME, "Environment reset");
    memset(&eSt, 0, sizeof(eSt));
    mouse_env->setState(MMouse::getInitialState());
    getSensorMeasurements();
    m_running = false;
    emit runningChanged();
    upGui();
    return RET_OK;
}

int Env::step(void){
    if(m_running)
    {
      Logger::getInstance()->wrn(NAME, "cannot step when running");
        return RET_WARN;
    }

    execute(eMeta.time_div * eMeta.step_s * 1000);

    return RET_OK;
}

int Env::setStep(double s){
    eMeta.step_s = s;
    return RET_OK;
}

int Env::setTimeDiv(double td){
    if(td < time_div_min)
    {
      Logger::getInstance()->wrn(NAME, "Setting time div smaller than "+
                 QString::number(time_div_min)+
                 "is forbidden!");
        eMeta.time_div = time_div_min;
        emit time_divChanged();
        return RET_WARN;
    }
    if(td > time_div_max)
    {
      Logger::getInstance()->wrn(NAME, "Setting time div greater than "+
                 QString::number(time_div_max)+
                 "is forbidden!");
        eMeta.time_div = time_div_max;
        emit time_divChanged();
        return RET_WARN;
    }
    if(eMeta.time_div != td)
    {
        eMeta.time_div = td;
        emit time_divChanged();
    }
    return RET_OK;
}

int Env::init(void){
  Logger::getInstance()->inf(NAME, "Environment initialization");
    eMeta.step_s = 0.001;
    eMeta.time_div = 1;
    emit time_divChanged();
    setMotorSpeed(0, 0);
    mouse_env->mMeta.name = "sim";

    return reset();
}

double Env::getTimeDivMin(void){
    return Env::time_div_min;
}
double Env::getTimeDivMax(void){
    return Env::time_div_max;
}
double Env::getTimeDiv(void){
    return eMeta.time_div;
}

int Env::setMotorSpeed(double left, double right)
{
    if(
            left < -1.0 ||
            right < -1.0 ||
            left > 1.0 ||
            right > 1.0
      )
    {
        Logger::getInstance()->err(NAME,
                QString("motor speed must be within <-1, 1>."
                "Provided values: left=%1, right=%2")
      .arg(left, 10, 'f', 3, ' ')
      .arg(right, 10, 'f', 3, ' '));
        return RET_ERR;
    }
    m_val.pwmL = left;
    m_val.pwmR = right;
    Logger::getInstance()->dbg(NAME,
            QString("Motor PWM set to: left=%1, right=%2").arg(m_val.pwmL, 10, 'f', 3, ' ')
            .arg(m_val.pwmR, 10, 'f', 3, ' '));
    emit targetSpeedChanged();
    return RET_OK;
}

void Env::getSensorMeasurements(){
    this->getDistances();
    mouse_env->upGuiState();
}

int32_t Env::getEncoderDataLeft()
{
    return mouse_env->getEncoderTicksLeft();
}

int32_t Env::getEncoderDataRight()
{
    return mouse_env->getEncoderTicksRight();
}

void Env::getDistances(){
    SensorDist* sDist;
    for(int i = 0; i < mouse_env->mMeta.sensorsDist.length(); i++){
        sDist = (SensorDist*)mouse_env->mMeta.sensorsDist.at(i);
        if(sDist->getExists() == true)
        {
            SensorDist::Measurement measurement;
            Position sensorPositionGlobal =
                mouse_env->localToGlobal(sDist->getPositionOffset());
            int status = measureSensorDistance(m_maze, &sensorPositionGlobal,
                                               sDist, measurement);
            if (status == RET_OK)
            {
                mouse_env->sensorsDistances[i] = measurement.distance;
            }
            else
            {
                Logger::getInstance()->err("env", QString("Problems with sensor distance measurement"));
            }
        }
    }
}

QList<double> Env::getMouseSensorDistances(void)
{
    QList<double> ret;
    for(int i = 0; i < mouse_env->mMeta.sensorsDist.length(); i++){
        SensorDist* sDist = (SensorDist*)mouse_env->mMeta.sensorsDist.at(i);
        if(sDist->getExists() == true)
        {
            ret.append(mouse_env->sensorsDistances[i]);
        }
    }
    return ret;
}

QJsonArray Env::getMouseSensorDistancesJson(void)
{
    QList<double> distances = getMouseSensorDistances();
    QJsonArray array;
    for (auto &dist : distances)
    {
        array.append(dist);
    }
    return array;
}

int Env::measureSensorDistance(const Maze *maze, const Position *sensor_position,
                                     const SensorDist *sensor, SensorDist::Measurement &measurement)
{
    double y_intersection_inside, y_intersection_outside;
    dir_t sen_dir = DIR_NONE;
    double distance = 0;
    double angle = 0;
    dir_t found = DIR_NONE;
    maze_cell_id_t cell_id = {};
    maze_cell_t cstate = {};
    cell_id = maze->getCellIdfromCoords(*sensor_position);

    if(cell_id.x == MAZE_N || cell_id.y == MAZE_N)
    {
        Logger::getInstance()->err("env", QString("Cannot measure distance outside of the maze! "));
        return RET_ERR;
    }

    if(IsSensorDistanceInsideExistingWall(maze, sensor_position))
    {
        measurement.distance = sensor->getMinDist();
        measurement.angle_rad = 0;
        return RET_OK;
    }

    if(sensor_position->t() == Utils::ORIENTATION_NORTH) sen_dir = DIR_NORTH;
    else if(sensor_position->t() == Utils::ORIENTATION_SOUTH) sen_dir = DIR_SOUTH;
    else if(sensor_position->t() > Utils::ORIENTATION_NORTH || sensor_position->t() < Utils::ORIENTATION_SOUTH) sen_dir = DIR_WEST;
    else sen_dir = DIR_EAST;

    /**
     * @brief The algorithm works in the following manner:
     * Direct North and South cases are handled separately.
     * All other cases are divided into two groups: East and West.
     * In each case firstly is calculated the point where laser crosses the closest wall.
     * Then (based on the crossing 'y' offset in the current cell) decision is made
     * if the laser is pointing walls or sticks (and appropriate sides of these).
     * If points to existing wall or post then the direction of this obstacle is saved
     * and based on it the measurement distance is calculated.
     * Otherwise, loop moves to the Neighbor cell and repeats steps.
     *
     * The functions is not well written, so at least it will be commented a lot
     */
    while((cell_id.isValid()) && (found == DIR_NONE))
    {
        cstate = maze->getCellState(cell_id);

        if(sen_dir == DIR_NORTH || sen_dir == DIR_SOUTH)
        {
            // pointing posts?
            if(sensor_position->x() - cell_id.getWallCoordOutside(DIR_WEST) <= MAZE_DIM_WALL)
            {
                if(sen_dir == DIR_NORTH) found = DIR_NORTH;
                else found = DIR_SOUTH;
            }
            else{
                if(cellHasWalls(cstate, sen_dir))
                {
                    found = sen_dir;
                }
                else
                {
                    cell_id = cell_id.getNeighborCell(sen_dir);
                }

            }
        }

        //east or west
        else{
            //firstly check the closer side of wall
            y_intersection_inside = sensor_position->y() +
                            (cell_id.getWallCoordInside(sen_dir) - sensor_position->x()) * qTan(sensor_position->t());

            //above upper stick
            if(y_intersection_inside > cell_id.getWallCoordOutside(DIR_NORTH))
            {
                // possibly it can point to the NW stick from the S side (but must point E)
                // must check intersection with W wall, inner side
                double raw_distance_from_wall = (cell_id.getWallCoordInside(DIR_WEST)) - sensor_position->x();
                y_intersection_inside = sensor_position->y() + (raw_distance_from_wall)*qTan(sensor_position->t());
                if ((sen_dir == DIR_EAST) &&
                    (y_intersection_inside > cell_id.getWallCoordInside(DIR_NORTH)))
                {
                    // post
                    found = DIR_NORTH;
                }
                // otherwise it points north wall
                else if (cellHasWalls(cstate, DIR_NORTH))
                {
                    found = DIR_NORTH;
                }
                else
                {
                    cell_id = cell_id.getNeighborCell(DIR_NORTH);
                }
            }
            //N stick, inner side
            else if(y_intersection_inside > cell_id.getWallCoordInside(DIR_NORTH))
            {
                //firstly hit north wall
                if(cellHasWalls(cstate, DIR_NORTH))
                {
                    found = DIR_NORTH;
                }
                // then hit the post (side the same as direction of laser)
                else
                {
                    found = sen_dir;
                }
            }

            //wall
            else if(y_intersection_inside > cell_id.getWallCoordInside(DIR_SOUTH))
            {
                // if wall exists, just hit it or the post, the result is the same
                if(cellHasWalls(cstate, sen_dir))
                {
                    found = sen_dir;
                }
                else{
                    // if wall does not exist, then check if the laser will hit the post from the inside
                    y_intersection_outside = sensor_position->y() +
                                    (-sensor_position->x() + cell_id.getWallCoordOutside(sen_dir))*qTan(sensor_position->t());
                    //NW stick, south side
                    if(y_intersection_outside > cell_id.getWallCoordInside(DIR_NORTH))
                    {
                        found = DIR_NORTH;
                    }
                    //SW stick, north side
                    else if(y_intersection_outside < cell_id.getWallCoordInside(DIR_SOUTH))
                    {
                        found = DIR_SOUTH;
                    }
                    else{
                        cell_id = cell_id.getNeighborCell(sen_dir);
                    }
                }
            }
            //S stick
            else if(y_intersection_inside > cell_id.getWallCoordOutside(DIR_SOUTH))
            {
                //firstly hit south wall
                if(cellHasWalls(cstate, DIR_SOUTH)){
                    found = DIR_SOUTH;
                }
                // then hit the post (side the same as direction of laser)
                else{
                    found = sen_dir;
                }
            }
            //below SW stick - south wall
            else
            {
                // there is possibility that the sensor is inside NS wall and points SW post from N side...
                if(sensor_position->x() - cell_id.getWallCoordOutside(DIR_WEST) < MAZE_DIM_WALL)
                {
                    if(sen_dir == DIR_EAST)
                    {

                        y_intersection_inside = sensor_position->y() +
                                                (-sensor_position->x() + cell_id.getWallCoordInside(DIR_WEST)) * qTan(sensor_position->t());

                        if (y_intersection_inside <= cell_id.getWallCoordInside(DIR_SOUTH))
                        {
                            // post
                            found = DIR_SOUTH;
                        }
                        else if (cellHasWalls(cstate, DIR_SOUTH))
                        {
                            found = DIR_SOUTH;
                        }
                        else
                        {
                            cell_id = cell_id.getNeighborCell(DIR_SOUTH);
                        }
                    }
                    else
                    {
                        // west
                        y_intersection_outside = sensor_position->y() +
                                                 (-sensor_position->x() + cell_id.getWallCoordOutside(DIR_WEST)) * qTan(sensor_position->t());

                        if (y_intersection_outside >= cell_id.getWallCoordInside(DIR_NORTH))
                        {
                            // post
                            found = DIR_NORTH;
                        }
                        else
                        {
                            cell_id = cell_id.getNeighborCell(DIR_WEST);
                        }
                    }
                }
                else if (cellHasWalls(cstate, DIR_SOUTH))
                {
                    found = DIR_SOUTH;
                }
                else
                {
                    cell_id = cell_id.getNeighborCell(DIR_SOUTH);
                }
            }
        }

        // the wall or post was found on the way of the laser
        // calculate the distance and the angle of the laser pointing the obstacle
        switch(found){
        case DIR_NORTH:
            distance = (cell_id.getWallCoordInside(found) - sensor_position->y())/qSin(sensor_position->t());
            angle = sensor_position->t();
            break;
        case DIR_SOUTH:
            distance = (-cell_id.getWallCoordInside(found) + sensor_position->y())/qSin(-sensor_position->t());
            angle = sensor_position->t() + M_PI;
            break;
        case DIR_EAST:
            distance = (cell_id.getWallCoordInside(found) -sensor_position->x())/qCos(sensor_position->t());
            angle = sensor_position->t() + M_PI_2;
            break;
        case DIR_WEST:
            distance = (cell_id.getWallCoordInside(found) -sensor_position->x())/qCos(sensor_position->t());
            angle = sensor_position->t() - M_PI_2;
            break;
        default:
            break;
        }
    }

    if(found != DIR_NONE){
        distance = std::max(distance, sensor->getMinDist());
        distance = std::min(distance, sensor->getMaxDist());
    }
    else{
        distance = sensor->getMaxDist();
    }


    measurement.distance = distance;
    double angle_tmp = M_PI_2 - abs(Utils::normalizeRadians(angle) - M_PI_2);
    measurement.angle_rad = Utils::normalizeRadians(angle_tmp);

    return RET_OK;
}

bool Env::IsSensorDistanceInsideExistingWall(const Maze *maze, const Position *sensor_position)
{
    maze_cell_id_t cell_id = maze->getCellIdfromCoords(*sensor_position);
    maze_cell_t cstate = maze->getCellState(cell_id);

    bool inside_wall = false;
    if(sensor_position->x() - cell_id.getWallCoordOutside(DIR_WEST) < MAZE_DIM_WALL)
    {
        if(cellHasWalls(cstate, DIR_WEST))
        {
            inside_wall = true;
        }
    }
    if(sensor_position->y() - cell_id.getWallCoordOutside(DIR_SOUTH) < MAZE_DIM_WALL)
    {
        if(cellHasWalls(cstate, DIR_SOUTH))
        {
            inside_wall = true;
        }
    }
    return inside_wall;
}

int Env::execute(double milliseconds)
{
  int steps = qCeil(milliseconds/1000 / eMeta.step_s);
  Logger::getInstance()->dbg(NAME, QString("Simulate %1 ms with %2 steps, %3 ms each ")
                                       .arg(milliseconds, 10, 'f', 6, ' ')
                                       .arg(steps, 10)
                                       .arg(1000*eMeta.step_s, 10, 'f', 6, ' '));
  mouse_env->setTargetSpeed(m_val.pwmL, m_val.pwmR);
  while(steps--)
  {
    mouse_env->step(eMeta.step_s);
    eSt.iter++;
    eSt.time = eSt.time+eMeta.step_s;
  }

  getSensorMeasurements();
  upGui();

  return RET_OK;
}


void Env::upGui(){
    mouse_env->upGuiState();
    emit stateChanged();
}

double Env::getTargetSpeedAngular(void)
{
    return m_val.pwmL-m_val.pwmR;
}
double Env::getTargetSpeedLinear(void)
{
    return m_val.pwmL+m_val.pwmR;
}
void Env::setTargetSpeedAngular(double speed)
{
    if(speed < -SPEED_ANGULAR_MAX || speed > SPEED_ANGULAR_MAX)
    {
        Logger::getInstance()->wrn(NAME,
                QString("speed angular must be within <-1, 1>."
                "Provided value=%1").arg(speed, 10, 'f', 3, ' '));
    }
    speed = max(speed, -SPEED_ANGULAR_MAX);
    speed = min(speed, +SPEED_ANGULAR_MAX);
    double linear = (m_val.pwmL + m_val.pwmR)/2;
    setMotorSpeed(linear + speed/2, linear - speed/2);
}

void Env::setTargetSpeedLinear(double speed)
{
    if(speed < -SPEED_LINEAR_MAX || speed > SPEED_LINEAR_MAX)
    {
        Logger::getInstance()->wrn(NAME,
                QString("speed linear must be within <-1, 1>."
                "Provided value=%1").arg(speed, 10, 'f', 3, ' '));
    }
    speed = max(speed, -SPEED_LINEAR_MAX);
    speed = min(speed, +SPEED_LINEAR_MAX);
    double angular = (m_val.pwmL - m_val.pwmR)/2;
    setMotorSpeed(speed/2+angular, speed/2-angular);
}


double Env::time_div_min = 0.1;
double Env::time_div_max = 10;
int Env::timer_resolution_ms = 20;
