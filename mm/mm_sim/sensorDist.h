#ifndef DISTSENSOR_H
#define DISTSENSOR_H

#include "QObject"
#include "QtMath"

#include "utils.h"
#include "logger.h"

class SensorDist : public QObject
{
    Q_OBJECT

    Q_PROPERTY(bool exists MEMBER m_exists NOTIFY dChanged)
    Q_PROPERTY(double x_offset MEMBER m_x_offset NOTIFY dChanged)
    Q_PROPERTY(double y_offset MEMBER m_y_offset NOTIFY dChanged)
    Q_PROPERTY(double t MEMBER m_t_deg NOTIFY dChanged)
    Q_PROPERTY(double min_dist MEMBER m_min_dist NOTIFY dChanged)
    Q_PROPERTY(double max_dist MEMBER m_max_dist NOTIFY dChanged)

public:
    explicit SensorDist(double x, double y, double tRad, double min, double max);
    explicit SensorDist();
    ~SensorDist();
    void setExists(bool e){m_exists = e;}
    void setXoffset(double x){m_x_offset = x;}
    void setYoffset(double y){m_y_offset = y;}
    void setTrad(double t){m_t_deg = qRadiansToDegrees(t);}
    void setTdeg(double t){m_t_deg = t;}
    void setMinDist(double d);
    void setMaxDist(double d);
    bool getExists(void){return m_exists;}
    double getXoffset(void){return m_x_offset;}
    double getYoffset(void){return m_y_offset;}
    double getTrad(void){return qDegreesToRadians(m_t_deg);}
    double getTdeg(void){return m_t_deg;}
    double getMinDist(void) const {return m_min_dist;}
    double getMaxDist(void) const {return m_max_dist;}
    void updateGUI(void) {emit dChanged();}
    void dump(QString description);
    Position getPositionOffset(void);

    /**
     * @brief Measurement of the distance sensor
     */
    struct Measurement
    {
        // distance from the sensor to the obstacle (wall / stick)
        double distance;
        // angle under which the sensor is pointing the obstacle in radians
        // it can have values from 0 to M_PI/2
        double angle_rad;
    };

signals:
    void dChanged();

private:
    bool m_exists = true;
    double m_x_offset = 0;
    double m_y_offset = 0;
    double m_t_deg = 0;
    double m_min_dist = 0;
    double m_max_dist = 9999;
    const QString NAME = "sen D";
};


#endif // DISTSENSOR_H
