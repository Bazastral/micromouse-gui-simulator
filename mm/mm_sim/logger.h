#ifndef LOGGER__H__
#define LOGGER__H__

#include <QString>
#include <mutex>

#define LOG_MSG_MAZE   QString("Maze cpp")
#define QML_MSG_MAZE   QString("Maze qml")
#define LOG_MSG_MOUSE  QString("Mous cpp")
#define LOG_MSG_ENV    QString("Env  cpp")
#define QML_MSG_ENV    QString("Env  qml")
#define LOG_MSG_SDIS   QString("Sen dist")

class Logger
{
  public:
    Logger();
    void operator=(const Logger &) = delete;
    Logger(Logger &other) = delete;
    static Logger* getInstance();

    typedef enum {
      LOG_DBG,
      LOG_INFO,
      LOG_WARN,
      LOG_ERR
    } log_enum_t;

    void dbg(QString, QString);
    void inf(QString, QString);
    void wrn(QString, QString);
    void err(QString, QString);
    void log(log_enum_t, QString, QString);


  private:
    typedef struct{
      QString color;
      QString typeStr;
    } lvl_cfg;
    lvl_cfg levels[4] =
    {
      [LOG_DBG] =
      {
        .color = "\033[36m",
        .typeStr = "debug",
      },
      [LOG_INFO] =
      {
        .color = "\033[39m",
        .typeStr = "info",
      },
      [LOG_WARN] =
      {
        .color = "\033[33m",
        .typeStr = "warn",
      },
      [LOG_ERR] =
      {
        .color = "\033[31m",
        .typeStr = "error",
      }
    };
    const int minTypeLen = 7;
    const int minSubjectLen = 10;
    QString colorDefault = "\033[0m";

  static std::mutex _mutex;
  static Logger* _instance;
};



#endif // LOGGER__H__
