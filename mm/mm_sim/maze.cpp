#include <QCoreApplication>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>
#include <QRandomGenerator>
#include <QUrl>
#include <QPointF>
#include <fstream>
#include <iostream>

#include "qdebug.h"
#include "qdiriterator.h"
#include "maze.h"
#include "config.h"
#include "utils.h"
#include "logger.h"

using namespace std;


Maze::Maze(QObject *parent) : QObject(parent)
{
  Logger::getInstance()->inf(NAME, "I'm ready!");

  QDir projectDir(QDir::cleanPath(QCoreApplication::applicationDirPath()));
  projectDir.cdUp();
  projectDir.cdUp();
  m_mazefiles = QUrl::fromLocalFile(projectDir.path());
  m_path_basic_folder = projectDir.path() + QDir::separator();

  resetAll(m_maze);
  load("mazefiles_my/test_arena.txt");
}


//*****************************************************************
// Private functions cpp
//  |
//  |
//  V


void Maze::printMaze(maze_t mazeTmp){
  for(int x = 0; x<MAZE_N; x++){
    for(int y = 0; y<MAZE_N; y++){
      Logger::getInstance()->dbg(NAME, "x: " + QString::number(x) + ",  " +
          "y: " + QString::number(y) + ",  " +
          "v: " + maskToNSEW(mazeTmp[x][y]) +
          " (" + QString::number(mazeTmp[x][y]) + ")");

    }
  }

}

bool Maze::suitsChallengeRequirements(maze_t mazeTmp){
  (void)(mazeTmp);
  //TODO
  bool suits = false;

  return suits;
}


maze_cell_t Maze::getCellState(uint8_t x, uint8_t y) const {
  if(x>MAZE_N-1){
    Logger::getInstance()->err(NAME, "getCellState out of bounds, x: "+QString::number(x));
    return 0;
  }
  if(y>MAZE_N-1){
    Logger::getInstance()->err(NAME, "getCellState out of bounds, y: "+QString::number(y));
    return 0;
  }
  return m_maze[x][y];
}

maze_cell_t Maze::getCellState(maze_cell_id_t id) const
{
  return getCellState(id.x, id.y);
}

bool cellHasWalls(const maze_cell_t cell, const dir_t dir)
{
  bool wallsExist = false;
  if((cell & dir) == dir)
  {
    wallsExist = true;
  }
  return wallsExist;
}

maze_cell_id_t Maze::getCellIdfromCoords(double realx, double realy)
{
  maze_cell_id_t id = {
      .x = MAZE_N,
      .y = MAZE_N,
  };
  if (
      realx < 0 ||
      realy < 0 ||
      realx > MAZE_N * MAZE_DIM_SQUARE ||
      realy > MAZE_N * MAZE_DIM_SQUARE)
  {
    Logger::getInstance()->wrn("MAZE", "getCellIdfromCoords out of bounds" +
                                         QString("Cell x=%1, y=%2")
                                             .arg(realx)
                                             .arg(realy));
  }
  else
  {
    id.x = realx / MAZE_DIM_SQUARE;
    id.y = realy / MAZE_DIM_SQUARE;
  }

  return id;
}

maze_cell_id_t Maze::getCellIdfromCoords(Position pos)
{
  return getCellIdfromCoords(pos.x(), pos.y());
}

maze_cell_id_t maze_cell_id_t::getNeighborCell(const dir_t dir) const
{
  maze_cell_id_t new_id = *this;
  switch (dir)
  {
  case DIR_NORTH:
    if (new_id.y >= MAZE_N - 1)
    {
      Logger::getInstance()->err("Maze", "getNeighborCell out of bounds north");
    }
    new_id.y++;
    break;
  case DIR_SOUTH:
    if (new_id.y == 0)
    {
      Logger::getInstance()->err("Maze", "getNeighborCell out of bounds south");
    }
    new_id.y--;
    break;
  case DIR_EAST:
    if (new_id.x >= MAZE_N - 1)
    {
      Logger::getInstance()->err("Maze", "getNeighborCell out of bounds east");
    }
    new_id.x++;
    break;
  case DIR_WEST:
    if (new_id.x == 0)
    {
      Logger::getInstance()->err("Maze", "getNeighborCell out of bounds west");
    }
    new_id.x--;
    break;
  default:
    Logger::getInstance()->err("Maze", "getNeighborCell wrong direction");
  }
  return new_id;
}

double maze_cell_id_t::getWallCoord(const dir_t dir, const bool inside) const
{
  double wall_coord = -100;

  if(isValid())
  {
    switch (dir)
    {
    case DIR_NORTH:
      if(inside == true) { wall_coord = (y+1) * MAZE_DIM_SQUARE; }
      else  { wall_coord = (y+1) * MAZE_DIM_SQUARE + MAZE_DIM_WALL; }
      break;
    case DIR_SOUTH:
      if(inside == true) { wall_coord = y * MAZE_DIM_SQUARE + MAZE_DIM_WALL; }
      else  { wall_coord = y * MAZE_DIM_SQUARE; }
      break;
    case DIR_EAST:
      if(inside == true) { wall_coord = (x+1) * MAZE_DIM_SQUARE; }
      else  { wall_coord = (x+1) * MAZE_DIM_SQUARE + MAZE_DIM_WALL; }
      break;
    case DIR_WEST:
      if(inside == true) { wall_coord = (x) * MAZE_DIM_SQUARE + MAZE_DIM_WALL; }
      else  { wall_coord = x * MAZE_DIM_SQUARE; }
      break;
    default:
      Logger::getInstance()->err("Maze", "getWallCoord wrong direction");
    }
  }
  return wall_coord;
}
double maze_cell_id_t::getWallCoordOutside(const dir_t dir) const
{
  return getWallCoord(dir, false);
}
double maze_cell_id_t::getWallCoordInside(const dir_t dir) const
{
  return getWallCoord(dir, true);
}
Point maze_cell_id_t::getMiddleCoords(void) const
{
  Point point = {
  .x = this->x * MAZE_DIM_SQUARE + MAZE_DIM_WALL + MAZE_DIM_CORRIDOR_HALF,
  .y = this->y * MAZE_DIM_SQUARE + MAZE_DIM_WALL + MAZE_DIM_CORRIDOR_HALF
  };
  return point;
}

QString Maze::cellStateToString(maze_cell_t cell){
  QString rets = "cell: ";
  if(cell&MASK_N) rets+="N";
  if(cell&MASK_S) rets+="S";
  if(cell&MASK_E) rets+="E";
  if(cell&MASK_W) rets+="W";
  rets+=" ";
  if(cell&MASK_visited) rets+="V";
  return rets;
}

void Maze::overrideCell(uint8_t x, uint8_t y, maze_cell_t cell){
  Logger::getInstance()->dbg(NAME, "overrideCell");
  if(x>MAZE_N-1 || y>MAZE_N-1){
    Logger::getInstance()->err(NAME, "overrideCell out of bounds");
    return;
  }
  m_maze[x][y] = cell;
}

void Maze::changeWallMouseKnowledge(uint8_t x, uint8_t y, uint8_t NSEW, int exists)
{
  if(x>MAZE_N-1){
    Logger::getInstance()->err(NAME, "out of bounds, "
        "x max available: "+ QString::number(MAZE_N)+ " "
        "x given: "+ QString::number(x));
    return;
  }

  if(y>MAZE_N-1){
    Logger::getInstance()->err(NAME, "out of bounds, "
        "y max available: "+ QString::number(MAZE_N)+ " "
        "y given: "+ QString::number(y));
    return;
  }

  for(uint8_t mask = 0x01; mask <= MASK_W; mask = (mask << 1u))
  {
    if(NSEW & mask)
    {
      // do not save the state. Directly change wall in QML
      mazeChangedMouseKnowledge(x, y, mask, exists);
    }
  }
}

void Maze::setWall(maze_t mazeTmp, uint8_t x, uint8_t y, uint8_t NSEW){
  if(x>MAZE_N-1){
    Logger::getInstance()->err(NAME, "setWall out of bounds, "
        "x max available: "+ QString::number(MAZE_N)+ " "
        "x given: "+ QString::number(x));
    return;
  }

  if(y>MAZE_N-1){
    Logger::getInstance()->err(NAME, "setWall out of bounds, "
        "y max available: "+ QString::number(MAZE_N)+ " "
        "y given: "+ QString::number(y));
    return;
  }
  if(!(NSEW & MASK_ALL)){
    Logger::getInstance()->err(NAME, "setWall wrong wall mask");
  }

  mazeTmp[x][y] |= NSEW;

  if(NSEW & MASK_N){
    if(y <= MAZE_N-2){
      mazeTmp[x][y+1] |= MASK_S;
    }
  }
  if(NSEW & MASK_S){
    if(y >= 1){
      mazeTmp[x][y-1] |= MASK_N;
    }
  }
  if(NSEW & MASK_E){
    if(x <= MAZE_N-2){
      mazeTmp[x+1][y] |= MASK_W;
    }
  }
  if(NSEW & MASK_W){
    if(x >= 1){
      mazeTmp[x-1][y] |= MASK_E;
    }
  }

  if(!isValid(mazeTmp)){
    Logger::getInstance()->wrn(NAME, "setWall failed!");
    return;
  }
}

void Maze::resetWall(maze_t mazeTmp, uint8_t x, uint8_t y, uint8_t NSEW){
  if(x>MAZE_N-1 || y>MAZE_N-1){
    Logger::getInstance()->err(NAME, "resetWall out of bounds");
    return;
  }
  if(!(NSEW & MASK_ALL)){
    Logger::getInstance()->err(NAME, "resetWall wrong wall mask");
  }

  mazeTmp[x][y] &= ~NSEW;

  if(NSEW & MASK_N){
    if(y <= MAZE_N-2){
      mazeTmp[x][y+1] &=~MASK_S;
    }
  }
  if(NSEW & MASK_S){
    if(y >= 1){
      mazeTmp[x][y-1] &=~MASK_N;
    }
  }
  if(NSEW & MASK_E){
    if(x <= MAZE_N-2){
      mazeTmp[x+1][y] &=~MASK_W;
    }
  }
  if(NSEW & MASK_W){
    if(x >= 1){
      mazeTmp[x-1][y] &=~MASK_E;
    }
  }

  if(!isValid(mazeTmp)){
    Logger::getInstance()->wrn(NAME, "resetWall failed!");
    return;
  }
}

void Maze::resetAll(maze_t mazeTmp){
  memset(mazeTmp, 0, MAZE_N*MAZE_N);

  if(!isValid(mazeTmp)){
    Logger::getInstance()->wrn(NAME, "Reset maze failed!");
    return;
  }
}

void Maze::setAll(maze_t mazeTmp){
  memset(mazeTmp, MASK_ALL, MAZE_N*MAZE_N);

  if(!isValid(mazeTmp)){
    Logger::getInstance()->wrn(NAME, "setAll failed!");
    return;
  }
}

void Maze::setMaze(maze_t newMaze){
  if(!isValid(newMaze)){
    Logger::getInstance()->wrn(NAME, "setMaze failed, maze to be set is invalid!");
    return;
  }

  bool changed = false;
  for(int x = 0; x<MAZE_N; x++){
    for(int y = 0; y<MAZE_N; y++){
      if(newMaze[x][y] != m_maze[x][y]){
        changed = true;
        m_maze[x][y] = newMaze[x][y];
        //                Logger::getInstance()->dbg(NAME, "change at x:"+QString::number(x) + " y:" + QString::number(y));
      }
    }
  }
  if(changed){
    emit mazeChanged();
  }
}

void Maze::getMaze(maze_t mazeTmp){
  for(int x = 0; x<MAZE_N; x++){
    for(int y = 0; y<MAZE_N; y++){
      mazeTmp[x][y] = m_maze[x][y];
    }
  }
}

bool Maze::isValid(maze_t mazeTmp){
  bool valid = true;

  //horizontal
  for(uint8_t row = 0; row<MAZE_N-1; row++){
    for(uint8_t col = 0; col<MAZE_N; col++){
      if(mazeTmp[col][row] & MASK_N){
        if(!(mazeTmp[col][row+1] & MASK_S))
        {
          valid = false;
          Logger::getInstance()->wrn(NAME, "Wall is invalid between  \n"
              "x:" + QString::number(col)+" "
              "y:" + QString::number(row)+" and "
              "x:" + QString::number(col)+" "
              "y:" + QString::number(row+1) );
          Logger::getInstance()->wrn(NAME, "lower cell:" + maskToNSEW(mazeTmp[col][row])+ " "
              "higher cell:" + maskToNSEW(mazeTmp[col][row+1]));
        }
      }
    }
  }

  //vertical
  for(uint8_t row = 0; row<MAZE_N; row++){
    for(uint8_t col = 0; col<MAZE_N-1; col++){
      if( (m_maze[col][row] & MASK_E))
      {
        if(!(m_maze[col+1][row] & MASK_W))
        {
          valid = false;
          Logger::getInstance()->wrn(NAME, "Wall is invalid between  \n"
              "x:" + QString::number(col)+" "
              "y:" + QString::number(row)+" and "
              "x:" + QString::number(col+1)+" "
              "y:" + QString::number(row) );
          Logger::getInstance()->wrn(NAME, "left cell:" + maskToNSEW(mazeTmp[col][row])+ " "
              "right cell:" + maskToNSEW(mazeTmp[col+1][row]));
        }
      }
    }
  }

  if(valid){
    //                Logger::getInstance()->dbg(NAME, "MAZE IS VALID YES");
  }
  else
  {
    //                this->Logger::getInstance()->dbg(NAME, "MAZE IS VALID NO");
  }
  return valid;
}



//*****************************************************************
// FILES
//  |
//  |
//  V

int Maze::save(QUrl name){
  QString filePath = name.toLocalFile();
  Logger::getInstance()->inf(NAME, "Saving: "+filePath);
  ofstream mazeFile;
  mazeFile.open(filePath.toStdString().c_str(), std::ofstream::trunc);
  maze_t mazeTmp;
  getMaze(mazeTmp);

  if(mazeFile.is_open()){
    int x, y;

    for(y = MAZE_N-1; y>=0; y--){
      //horizontal
      for(x = 0; x<MAZE_N; x++){
        mazeFile<<char_stick;
        if(mazeTmp[x][y]&MASK_N){
          mazeFile<<char_wall_hori;
          mazeFile<<char_wall_hori;
          mazeFile<<char_wall_hori;
        }
        else{
          mazeFile<<"   ";
        }
      }
      mazeFile<<"\n";

      //vertical
      for(x = 0; x<MAZE_N; x++){
        if(mazeTmp[x][y] & MASK_W){
          mazeFile<<char_wall_vert<<"   ";
        }
        else{
          mazeFile<<"    ";
        }
      }
      //last on right
      if(mazeTmp[MAZE_N-1][y] & MASK_E){
        mazeFile<<char_wall_vert;
      }
      else{
        mazeFile<<" ";
      }
      mazeFile<<"\n";
    }

    //bottom
    for(x=0, y=0; x<MAZE_N; x++){
      mazeFile<<char_stick;
      if(mazeTmp[x][y]&MASK_S){
        mazeFile<<char_wall_hori;
        mazeFile<<char_wall_hori;
        mazeFile<<char_wall_hori;
      }
      else{
        mazeFile<<"   ";
      }
    }
    mazeFile.close();
  }
  else{
    Logger::getInstance()->err(NAME, "Could not open the file");
  }


  return RET_OK;
}

int Maze::load(QString name, QJsonObject *ret)
{
  int rt;

  rt = loadFullName(m_path_basic_folder+QDir::separator()+name);
  if(rt != RET_OK)
  { (*ret)["error"] = "some error occured in Maze::load"; }
  return  rt;
}

int Maze::load(QString name)
{
  return loadFullName(m_path_basic_folder + QDir::separator() + name);
}

int Maze::load(QUrl name)
{
  return loadFullName(name.toLocalFile());
}

int Maze::loadFullName(QString file){
  file = QDir::cleanPath(file);
  Logger::getInstance()->inf(NAME, "Loading: "+file);
  ifstream mazeFile;
  mazeFile.open(file.toStdString().c_str());
  if(mazeFile.is_open()){
    try{
      string line;
      int idx;
      int x = 0, y = MAZE_N;
      bool parsing_horizontal = true;
      maze_t mazeTmp;
      resetAll(mazeTmp);
      int stick_to_stick = 0;

      while(getline(mazeFile, line)){
        x = 0;
        idx = 0;

        if(parsing_horizontal){
          y--;
          while(x<MAZE_N){
            idx = line.find_first_of(char_stick, idx);
            if(idx >= 0){
              if(stick_to_stick == 0 && idx > 0){
                stick_to_stick = idx;
              }
              if(line.at(idx+1) == char_wall_hori){
                if(y>=0){setWall(mazeTmp, x, y, MASK_N);}
                else{setWall(mazeTmp, x, 0, MASK_S);}
              }
              else{
                if(y>=0){resetWall(mazeTmp, x, y, MASK_N);}
                else{resetWall(mazeTmp, x, 0, MASK_S);}
              }
            }
            x++;
            idx++;
          }
        }
        else{
          //vertical
          while(x<=MAZE_N){
            if(y<0){break;}
            if(line.at(idx) == char_wall_vert){
              if(x<MAZE_N){setWall(mazeTmp, x, y, MASK_W);}
              else{setWall(mazeTmp, MAZE_N-1, y, MASK_E);}
            }
            else{
              if(x<MAZE_N){resetWall(mazeTmp, x, y, MASK_W);}
              else{resetWall(mazeTmp, MAZE_N-1, y, MASK_E);}
            }
            x++;
            idx += stick_to_stick;
          }
        }
        parsing_horizontal = !parsing_horizontal;
      }

      mazeFile.close();
      setMaze(mazeTmp);
    }

    catch (...) {
      Logger::getInstance()->err(NAME, "The file is broken (not valid).");
      return RET_ERR;
    }
  }
  else{
    Logger::getInstance()->err(NAME, "Could not load the mazefile, wrong file?");
    return RET_ERR;
  }
  printInHex(m_maze);
  return RET_OK;
}

void Maze::printInHex(maze_t maze)
{
  QDebug deb = qDebug();
  for(int x = 0; x<MAZE_N; x++){
    for(int y = 0; y<MAZE_N; y++){
      deb.noquote() << QString("0x%1, ").arg(maze[x][y], 2, 16, QLatin1Char('0'));
    }
    deb << "\n";
  }
}




//*****************************************************************
// QML
//  |
//  |
//  V

bool Maze::currMazeValid(){
  return isValid(m_maze);
}

void Maze::currMazePrint(){
  printMaze(m_maze);
}

void Maze::currMazeSetWall(uint8_t x, uint8_t y, uint8_t NSEW){
  maze_t mazeTmp;
  getMaze(mazeTmp);
  setWall(mazeTmp, x, y, NSEW);
  setMaze(mazeTmp);
}
void Maze::currMazeResetWall(uint8_t x, uint8_t y, uint8_t NSEW){
  maze_t mazeTmp;
  getMaze(mazeTmp);
  resetWall(mazeTmp, x, y, NSEW);
  setMaze(mazeTmp);
}
void Maze::currMazeResetAll(){
  resetAll(m_maze);
}
void Maze::currMazesetAll(){
  setAll(m_maze);
}

QString Maze::maskToNSEW(maze_cell_t cellTmp){
    QString ret;
    if(cellTmp & MASK_N){ret+="N";};
    if(cellTmp & MASK_S){ret+="S";};
    if(cellTmp & MASK_E){ret+="E";};
    if(cellTmp & MASK_W){ret+="W";};
    if(ret.isEmpty()){ret = "_";};
    return ret;
}


bool maze_cell_id::isValid(void) const
{
  bool valid = false;
  if (x < MAZE_N && y < MAZE_N)
  {
    valid = true;
  }
  return valid;
}
