#include <string>
#include <cmath>

#include "diffDrive.h"
#include "motor.h"
#include "logger.h"
#include "config.h"

using namespace std;

DiffDrive::DiffDrive(std::string name, double radius, double ticks_per_turn,
                     double distance_between_wheels)
{
    _drive_left = new Drive("L drive", radius, ticks_per_turn);
    _drive_right = new Drive("R drive", radius, ticks_per_turn);
    _x = _y = _t = _w = _v = 0;
    _dx = _dy = _dt = 0;
    _D = distance_between_wheels;
    _name = name;
}

DiffDrive::~DiffDrive()
{
    delete _drive_left;
    delete _drive_right;
}

void DiffDrive::step(double time)
{
    double distance_left = _drive_left->step(time);
    double distance_right = _drive_right->step(time);

    if((distance_left) == 0 && (distance_right == 0))
    {
        return;
    }

    // Logger::getInstance()->dbg(QString::fromStdString(_name), "dist left" + QString::number(distance_left));
    // Forward kinematics
    // https://www.cs.columbia.edu/~allen/F19/NOTES/icckinematics.pdf

    double distance_avg = (distance_left + distance_right) / 2;
    double _x_next;
    double _y_next;
    double _t_next;
    double _w_next;
    double _v_next;

    // go stright
    const double TREAT_SIMILLAR_DISTANCES_AS_EQUAL = 0.01;
    if (abs((distance_left - distance_right)/distance_left) < TREAT_SIMILLAR_DISTANCES_AS_EQUAL)
    {
        _x_next = _x + distance_avg * cos(_t);
        _y_next = _y + distance_avg * sin(_t);
        _t_next = _t;
        _w_next = 0;
        _v_next = distance_avg / time;
    }
    // turn
    else
    {
        double alfa = -(distance_left - distance_right) / _D;
        double R = -distance_avg / alfa;
        double csox = _x + R * sin(_t);
        double csoy = _y - R * cos(_t);

        _x_next = +cos(alfa) * (_x - csox) - sin(alfa) * (_y - csoy) + csox;
        _y_next = +sin(alfa) * (_x - csox) + cos(alfa) * (_y - csoy) + csoy;
        _t_next = Utils::normalizeRadians(_t + alfa);
        _w_next = alfa / time;
        _v_next = std::abs(_w_next * R);
    }

    _x = _x_next;
    _y = _y_next;
    _t = _t_next;
    _w = _w_next;
    _v = _v_next;
}

void DiffDrive::setState(double x, double y, double t, double v, double w)
{
    _x = x;
    _y = y;
    _t = t;
    _v = v;
    _w = w;
    // reset ticks
    _drive_left->getTicks();
    _drive_right->getTicks();
    if(v != 0 || w != 0)
    {
        Logger::getInstance()->wrn(QString::fromStdString(_name),
         "Not implemented: setting speed different than 0");
    }
    _drive_left->setSpeed(0);
    _drive_right->setSpeed(0);
}

void DiffDrive::setTargetSpeed(double left, double right)
{
    _drive_left->setTargetSpeed(left);
    _drive_right->setTargetSpeed(right);
}

double DiffDrive::getX(void)
{
    return _x;
}
double DiffDrive::getY(void)
{
    return _y;
}
double DiffDrive::getT(void)
{
    return _t;
}
double DiffDrive::getW(void)
{
    return _w;
}
double DiffDrive::getV(void)
{
    return _v;
}

int32_t DiffDrive::getEncoderTicksLeft(void)
{
    return _drive_left->getTicks();
}
int32_t DiffDrive::getEncoderTicksRight(void)
{
    return _drive_right->getTicks();
}
int DiffDrive::setRadius(double radius)
{
    int ret = _drive_left->setRadius(radius);
    if(ret == 0)
    {
        ret = _drive_right->setRadius(radius);
    }
    return ret;
}

double DiffDrive::getWheelsRadius(void)
{
    return _drive_left->getRadius();
}

int DiffDrive::setDistanceBetweenWheels(double distance)
{
    const double DISTANCE_MAX = 0.95 * MAZE_DIM_SQUARE;
    const double DISTANCE_MIN = 10;
    if (distance <= DISTANCE_MIN || distance >= DISTANCE_MAX)
    {
        Logger::getInstance()->err(QString::fromStdString(_name),
                                   QString("Distance between wheels invalid. Should be in (%1, %2). Trying to set %3")
                                       .arg(DISTANCE_MIN, 10, 'f', 3, ' ')
                                       .arg(DISTANCE_MAX, 10, 'f', 3, ' ')
                                       .arg(distance, 10, 'f', 3, ' '));
        return -1;
    }
    Logger::getInstance()->inf(QString::fromStdString(_name),
                               "Setting distance between wheels to: " + QString::number(distance));
    _D = distance;
    return 0;
}

double DiffDrive::getDistanceBetweenWheels(void)
{
    return _D;
}

int DiffDrive::setTicksPerTurn(uint32_t ticks_per_turn)
{
    int ret = _drive_left->setTicksPerTurn(ticks_per_turn);
    if(ret == 0)
    {
        ret = _drive_right->setTicksPerTurn(ticks_per_turn);
    }
    return ret;
}

uint32_t DiffDrive::getTicksPerTurn(void)
{
    return _drive_left->getTicksPerTurn();
}