import QtQuick 2.10
import QtQuick.Window 2.10
import QtQml 2.11
import QtQuick.Controls
import QtQuick.Layouts 1.0
import QtQuick.Dialogs 6.2

Window {
  id: root
  visible: true
  width: 1280
  height: 800
  title: qsTr("Micromouse simulator")

  readonly property string buttonColorNormal: "#3838ad"
  readonly property string buttonColorDown: "#19194d"
  readonly property string buttonColorBorder: "#ececf9"

  Component.onCompleted: {
    // set fullscreen
    root.width = screen.width
    root.height = screen.height
  }

  Rectangle{
    id: rootBcg
    anchors.fill: parent
    color: "#030303"
  }

  function openLoadBrowser(){
    fd.fileMode= FileDialog.OpenFile
    fd.load = true
    fd.nameFilters  = ["Text files (*.txt)"]
    fd.open()
  }

  function openSaveBrowser(){
    fd.fileMode= FileDialog.SaveFile
    fd.save = true
    fd.nameFilters  = ["Text files (*.txt)"]
    fd.open()
  }

  FileDialog{
    id: fd
    property bool load: false
    property bool save: false
    onAccepted: {
      if(save){ _maze.save(fd.selectedFile) }
      else if(load){ _maze.load(fd.selectedFile) }
      load = false
      save = false
    }
  }

  SplitView{
    orientation: Qt.Horizontal
    anchors.fill: parent
    focus: true

    Maze{
      id: maze
      SplitView.minimumWidth: 200
      SplitView.fillWidth: true
    }


    SplitView{
      orientation: Qt.Vertical
      SplitView.minimumWidth: 200
      SplitView.preferredWidth: 400
      SplitView.maximumWidth: 800

      InfoEnv{
        id: iEnv
        SplitView.minimumHeight: 200
      }

      InfoMouse{
        id: iMouse
        SplitView.minimumHeight: 200
        SplitView.fillHeight: true
      }

      JoyStick{
        id: joy_stick
        SplitView.minimumHeight: 300

      }

      Section{
        id: maze_buttons
        name: "Maze"
        SplitView.minimumHeight: 100

        Column{
          leftPadding: 20
          spacing: 0
          topPadding: 40

          Row
          {
            CustomButton{
              id: mazeLoadButton
              text: "Load"
              enabled: mazeChangeButton.state === "Freeze"
              onClicked: openLoadBrowser()
            }

            CustomButton{
              id: mazeChangeButton
              text: ""
              readonly property string color_set: "red"
              readonly property string color_reset: "white"
              readonly property string color_toggle: "black"
              Component.onCompleted: state = "Freeze"

              states: [
                State {
                  name: "Freeze"
                  PropertyChanges { target: mazeChangeButton; colorOverwrite: ""}
                  PropertyChanges { target: mazeChangeButton; text: "Freeze"}
                  PropertyChanges { target: maze; changeWallToggle: false}
                },
                State {
                  name: "Toggle"
                  PropertyChanges { target: mazeChangeButton; colorOverwrite: color_toggle}
                  PropertyChanges { target: mazeChangeButton; text: "Toggle"}
                  PropertyChanges { target: maze; changeWallToggle: true}
                }
              ]
              onClicked: {
                if(mazeChangeButton.state == "Freeze"){ mazeChangeButton.state =  "Toggle"}
                else { mazeChangeButton.state =  "Freeze"}
              }
            }
            CustomButton{
              id: mazeSaveButton
              text: "Save"
              enabled: mazeChangeButton.state === "Freeze"
              onClicked: openSaveBrowser()
            }
          }
        }
      }
      Section{
        id: mouse_buttons
        name: "Mouse"
        SplitView.minimumHeight: 100

        Column{
          leftPadding: 20
          spacing: 0
          topPadding: 40
          Row{
            spacing: 20

            CustomButton{
              id: mouse_user_button
              text: "tact"
              enabled: true
              onReleased: _m_env.buttonReleased()
              onPressed:  _m_env.buttonPressed()
            }

            Rectangle{
              id: led_left
              property bool turned_on: _m_env.leds[0]
              color: turned_on ? "blue" : "gray"
              width: 20
              radius: width
              height: width
            }

            Rectangle{
              id: led_right
              property bool turned_on: _m_env.leds[1]
              color: turned_on ? "blue" : "gray"
              width: 20
              radius: width
              height: width
            }

            Rectangle{
              id: led_mid
              property bool turned_on: _m_env.leds[2]
              color: turned_on ? "green" : "gray"
              width: 20
              radius: width
              height: width
            }
          }
        }
      }
    }
    Keys.onPressed: (event)=> {
      if (event.key == Qt.Key_S) {
        _env.step()
      }
      else if (event.key == Qt.Key_Space) {
        _env.running ? _env.stop() : _env.start()
      }
      else if (event.key == Qt.Key_R) {
        _env.reset()
      }
      else if (event.key == Qt.Key_T) {
             _m_env.buttonPressed()
      }
    }
    Keys.onReleased: (event)=> {
      if (event.key == Qt.Key_T) {
        _m_env.buttonReleased()
      }
    }
  }
}
