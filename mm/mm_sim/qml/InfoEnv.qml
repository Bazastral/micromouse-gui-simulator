import QtQuick 2.11
import QtQml 2.11
import QtQuick.Controls 2.4

Item {
    id: iEnv

    property string txColor: "white"
    property double font_pointsize: 14

    Section{
        id: iEnvSection
        anchors.fill: parent
        name: "Simulator"

        Column{
            leftPadding: 20
            spacing: 0
            topPadding: 40

            Repeater{
                model: [
                    ["i    ",           _env.state.iter,     " "],
                    ["t    ",           _env.state.time,    "s"],
                ]
                delegate:
                Component{
                    Text{
                        color: txColor
                        font.pointSize: font_pointsize
                        font.family: "Consolas"
                        property string name: modelData[0]
                        property string value: modelData[1]
                        property string unit: modelData[2]
                        text:
                        name + ": " +
                        Number(value).toFixed(6) + "     " +
                        unit
                    }
                }
            }

            Row
            {
                Slider{
                    id: slider_speed
                    width: parent.width*0.8

                    from: _env.getTimeDivMin()
                    to: _env.getTimeDivMax()
                    value: _env.time_div
                    snapMode: Slider.SnapOnRelease
                    stepSize: (from-to)/100

                    handle: Rectangle {
                        x: slider_speed.leftPadding + slider_speed.visualPosition * (slider_speed.availableWidth - width)
                        y: slider_speed.topPadding + slider_speed.availableHeight / 2 - height / 2
                        implicitWidth: 26
                        implicitHeight: 26
                        radius: 13
                        color: slider_speed.pressed ? buttonColorDown : buttonColorNormal
                        border.color: buttonColorBorder
                    }
                    onPressedChanged: {
                        if(!pressed)    //released
                        _env.setTimeDiv(value)
                    }
                }
                Label{
                    text: slider_speed.value.toFixed(2)
                    color: "white"
                }
            }
            Row
            {
                CustomButton{
                    id: but_step
                    text: "step"
                    onClicked: _env.step()
                }
                CustomButton{
                    id: but_start_stop
                    text: _env.running ? "stop" : "run"
                    onClicked: _env.running ? _env.stop() : _env.start()
                }
                CustomButton{
                    id: but_reset
                    text: "reset"
                    onClicked: _env.reset()
                }
            }
        }
    }
}





