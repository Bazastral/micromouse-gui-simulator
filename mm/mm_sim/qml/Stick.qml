import QtQuick 2.0

Item {
    readonly property string colorStick: "#101010"

    function resize(){
        width= dim_wall * pointPerM
        height= width
        stick.width= dim_wall * pointPerM
        stick.height= stick.width
    }
    Component.onCompleted: {
        resize()
    }


    Rectangle{
        id: stick
        anchors.centerIn: parent
        color: colorStick
    }

    Connections{
        target: maze
        function onPointPerMChanged() { resize() }
    }
}

