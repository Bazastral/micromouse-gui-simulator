import QtQuick 2.1
import QtQuick.Shapes 1.11

Item {
    id: sensor

    property bool exists: false
    property double x_offset: 0
    property double y_offset: 0
    property double t: 0
    property double dist: 0
    property string color: "white"

    readonly property double width_mm: 7
    readonly property double height_mm: 14
    visible: exists
    x: x_offset*pointPerM - width/2 + m_mouse.width/2
    y: -y_offset*pointPerM - height/2 + m_mouse.height/2
    width:  width_mm*pointPerM
    height: height_mm*pointPerM
    z: 50
    rotation: -t

    Rectangle{
        id: rect
        anchors.fill: parent
        color: "black"
        radius: 3
        border.width: 1
        border.color: "white"

    }
    Shape {
        anchors.fill: parent
        z: 20

        ShapePath {
            strokeColor: sensor.color
            strokeWidth: 2
            startX: sensor.width/2
            startY: sensor.height/2
            PathLine {
                relativeY: dist ? -dist*pointPerM : 0
                relativeX: 0
            }
        }
    }
}
