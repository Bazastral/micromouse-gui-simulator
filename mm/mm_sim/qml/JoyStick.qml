import QtQuick 2.11

Item {
    id: joystick
    readonly property string buttonColorNormal: "#3838ad"
    readonly property double envSpeedMaxValue: 1.0
    readonly property double speedStepButton: 0.2
    property string tx_color: "white"
    property double font_pointsize: 14


    function onArrowPressed(action){
        var linear_rounded = Math.round(_env.getTargetSpeedLinear()/speedStepButton) * speedStepButton
        var angular_rounded = Math.round(_env.getTargetSpeedAngular()/speedStepButton) * speedStepButton
        if(action == "up") { _env.setTargetSpeedLinear(linear_rounded+speedStepButton) }
        else if(action == "down") { _env.setTargetSpeedLinear(linear_rounded - speedStepButton) }
        else if(action == "left") { _env.setTargetSpeedAngular(angular_rounded - speedStepButton) }
        else if(action == "right") { _env.setTargetSpeedAngular(angular_rounded + speedStepButton) }
        else{ console.error("JoyStick internal error") }
    }

    Section{
        id: joystick_section
        anchors.fill: parent
        name: ""

        Text{
            id: speed_linear_text
            anchors.top: parent.top
            anchors.left: parent.left
            color: tx_color
            font.pointSize: font_pointsize
            font.family: "Consolas"
            text: "target speed linear: " + Number(_env.targetSpeedLinear).toFixed(2)
        }
        Text{
            id: speed_angular_text
            anchors.top: speed_linear_text.bottom
            anchors.left: parent.left
            color: tx_color
            font.pointSize: font_pointsize
            font.family: "Consolas"
            text: "target speed angular: " + Number(_env.targetSpeedAngular).toFixed(2)
        }
        Connections{
            target: _env
            function onTargetSpeedChanged()
            {
                speed_angular_text.text= "target speed angular: " + Number(_env.targetSpeedAngular).toFixed(2);
                pin.getPositionFromEnv()
            }
        }

        Repeater
        {
            model: ListModel {
                id: myModel
                ListElement { offset_horizontal: -1; offset_vertical:  0; rotate_angle: 45; action: "left" }
                ListElement { offset_horizontal:  0; offset_vertical: -1; rotate_angle:135; action: "up" }
                ListElement { offset_horizontal:  1; offset_vertical:  0; rotate_angle:225; action: "right" }
                ListElement { offset_horizontal:  0; offset_vertical:  1; rotate_angle:315; action: "down" }
            }

            delegate: Rectangle{
                required property int offset_horizontal
                required property int offset_vertical
                required property int rotate_angle
                required property string action
                width: parent.width/6
                height: width
                color: "transparent"
                anchors.centerIn: parent
                anchors.horizontalCenterOffset: width*offset_horizontal
                anchors.verticalCenterOffset: width*offset_vertical
                transformOrigin: Item.Center
                rotation: rotate_angle

                Canvas{
                    anchors.fill:parent

                    onPaint:{
                        var context = getContext("2d");

                        // the triangle
                        context.beginPath();
                        context.moveTo(0, 0);
                        context.lineTo(0, width);
                        context.lineTo(width, width);
                        context.closePath();

                        // the fill color
                        context.fillStyle = buttonColorNormal;
                        context.fill();
                    }
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed:
                    {
                        onArrowPressed(action)
                    }
                }
            }
        }


        Rectangle{
            id: joystick_bottom
            anchors.centerIn: parent
            width: parent.width/4
            height: width
            radius: width/3
            color: "#a5a5a5"

            Rectangle{
                id: pin
                function getFreeMotionWidth()
                {
                    return joystick_bottom.width - pin.width;
                }
                function getPositionFromEnv()
                {
                    pin.y= getFreeMotionWidth() * (-_env.getTargetSpeedLinear()+envSpeedMaxValue)/(2*envSpeedMaxValue)
                    pin.x= getFreeMotionWidth() * (_env.getTargetSpeedAngular()+envSpeedMaxValue)/(2*envSpeedMaxValue)
                }
                function setEnvSpeedValuesFromPosition()
                {
                    var linear = -(pin.y/getFreeMotionWidth()) * (2*envSpeedMaxValue) + envSpeedMaxValue
                    var angular = (pin.x/getFreeMotionWidth()) * (2*envSpeedMaxValue) - envSpeedMaxValue
                    _env.setTargetSpeedLinear(linear)
                    _env.setTargetSpeedAngular(angular)
                }
                width: parent.width*0.7
                height: width
                color: "#252525"
                radius: width/3
                x: getFreeMotionWidth()/2
                y: x
                border.width: width/10
                border.color: "#101010"

                MouseArea {
                    anchors.fill: parent
                    drag.target: parent
                    drag.axis: Drag.XAndYAxis
                    drag.minimumX: 0
                    drag.maximumX: pin.getFreeMotionWidth()
                    drag.minimumY: 0
                    drag.maximumY: pin.getFreeMotionWidth()
                    onPressedChanged:{
                        if(!pressed)
                        {
                            pin.setEnvSpeedValuesFromPosition()
                        }
                    }
                }
            }
        }
    }
}