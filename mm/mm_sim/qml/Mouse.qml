import QtQuick 2.3
import QtQuick.Shapes 1.11

Item {
    id: m_mouse
    property string col: "red"
    property var mouse_ctx: undefined

    function init(){
        getBuild()
        getPos()
    }

    function getPos(){
        m_mouse.x = getXmid(mouse_ctx.state.x, m_mouse.width)
        m_mouse.y = getYmid(mouse_ctx.state.y, m_mouse.height)
        m_mouse.rotation = 90 - mouse_ctx.state.t * 180 / 3.14159265
        for(var i = 0; i<mouse_ctx.mMeta.sensorsDist.length; i++){
            dist_sensors.itemAt(i).dist = mouse_ctx.mouseSensorsDist[i]
        }
    }

    function mouseGetX(x_m, obj_w){
        return x_m*pointPerM - obj_w/2 + m_mouse.width/2
    }
    function mouseGetY(y_m, obj_h){
        return -y_m*pointPerM - obj_h/2 + m_mouse.height/2
    }

    function getBuild(){
        m_mouse.width = mouse_ctx.mMeta.width*pointPerM
        m_mouse.height = mouse_ctx.mMeta.length*pointPerM
        m_mouse.col = mouse_ctx.mMeta.color
        m_mouse.opacity = mouse_ctx.mMeta.opacity
        if(m_mouse.showBorder) { mouse_background.border.width = 1 }
        else { mouse_background.border.width = 0 }

        m_mouse_lw.height = mouse_ctx.getWheelsRadius() * 2 * pointPerM
        m_mouse_lw.width = m_mouse_lw.heigh/5
        m_mouse_lw.x = mouseGetX(-mouse_ctx.getWheelsTrack()/2, m_mouse_lw.width)
        m_mouse_lw.y = mouseGetY(0, m_mouse_lw.height)

        m_mouse_rw.height = mouse_ctx.getWheelsRadius() * 2 * pointPerM
        m_mouse_rw.width = m_mouse_rw.heigh/5
        m_mouse_rw.x = mouseGetX(mouse_ctx.getWheelsTrack()/2, m_mouse_rw.width)
        m_mouse_rw.y = mouseGetY(0, m_mouse_rw.height)
    }

    Component.onCompleted: {
        m_mouse.getBuild()
        m_mouse.getPos()
    }


    Rectangle{
        id: mouse_background
        color: "transparent"
        anchors.fill: parent
        border.width: 1
        border.color: "blue"

        Rectangle{
            id: mouse_mid
            color: m_mouse.col
            anchors.fill: parent
            radius: height/5
        }

        Rectangle{
            id: m_mouse_lw
            color: "black"
            radius: 1
            border.width: 1
            border.color: "#AAAAAA"
        }

        Rectangle{
            id: m_mouse_rw
            color: m_mouse_lw.color
            border.width: m_mouse_lw.border.width
            border.color: m_mouse_lw.border.color
            radius: m_mouse_lw.radius
        }

        Repeater{
            id: dist_sensors
            model: mouse_ctx.mMeta.sensorsDist
            delegate: MouseSensor {
                exists:  modelData.exists
                x_offset: modelData.x_offset
                y_offset: modelData.y_offset
                t: modelData.t
                color: m_mouse.col
            }
        }
    }

    Connections{
        target: _m_env
        function onStateChanged() {
            m_mouse.getPos() }
        function onMetaChanged() {
            m_mouse.getBuild() }
    }

    Connections{
        target: parent
        function onWidthChanged ()
        {
            getPos()
            getBuild()
        }
        function onHeightChanged()
        {
            getPos()
            getBuild()
        }
    }
}
