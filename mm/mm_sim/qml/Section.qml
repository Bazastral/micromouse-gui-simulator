import QtQuick 2.0

Item {
    property alias name: sectionName.text
    Rectangle{
        id: background
        anchors.fill: parent
        color: "black"
        border.width: 1
        border.color: "white"
        radius: 15

        Text{
            id: sectionName
            anchors.top: parent.top
            anchors.topMargin: 5
            anchors.horizontalCenter: parent.horizontalCenter
            color: "white"
        }
    }
}
