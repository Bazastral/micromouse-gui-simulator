import QtQuick 2.0
import QtQuick.Controls 2.4

Item {
    id: cb
    signal clicked()
    signal pressed()
    signal released()
    property alias text: but.text
    property bool automaticResize: false
    property string colorOverwrite: ""
    width: 120
    height: 50

    function resize(){
        if(automaticResize){
            width = root.width / 13
            height = root.height / 13
        }
    }

    Connections{
        target: root
        function onWidthChanged() { resize() }

        function onHeightChanged() { resize() }
    }
    Component.onCompleted: {
        resize()
    }

    Button{
        id: but
        anchors.fill: parent
        onClicked: cb.clicked()
        onPressed: cb.pressed()
        onReleased: cb.released()
        background: Rectangle{
            id: bugBackground
            anchors.fill: parent
            radius: 3
            color: colorOverwrite ?
                       colorOverwrite : (parent.down ? buttonColorDown : buttonColorNormal)
            border.width: 3
            border.color: buttonColorBorder
        }
    }
}



