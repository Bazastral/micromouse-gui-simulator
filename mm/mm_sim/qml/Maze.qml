import QtQuick 2.12
import QtQuick.Shapes 1.11
import QtQuick.Controls 2.4

Item {
    id: maze

    readonly property double width_occupy: 0.99
    readonly property double heigh_occupy: 0.99
    property double pointPerM: 1

    //all dimensions in millimeters
    readonly property double maze_square_n: _config._MAZE_N
    readonly property double dim_square: _config._MAZE_DIM_SQUARE
    readonly property double dim_wall: _config._MAZE_DIM_WALL
    readonly property double maze_margin: dim_square
    property bool changeWallToggle: false

    //text
    readonly property string text_col: "#AA0000"


    function getX(x, w){
        return x*pointPerM
    }
    function getY(y, h){
        return mazeReal.height - y*pointPerM - h
    }
    function getXmid(x, w){
        return x*pointPerM - w/2
    }
    function getYmid(y, h){
        return mazeReal.height - y*pointPerM - h/2
    }

    function init(){
        var i=0;
        //draw stics
        for(i=0; i<sticks.count; i++){
            sticks.itemAt(i).x_m = (i%(maze_square_n+1))*dim_square
            sticks.itemAt(i).y_m = (Math.floor(i/(maze_square_n+1)))*dim_square
        }
        //draw walls
        for(i=0; i<wallsHori.count; i++){
            wallsHori.itemAt(i).x_m = (i%maze_square_n)*dim_square + dim_square/2 + dim_wall/2
            wallsHori.itemAt(i).y_m = (Math.floor(i/maze_square_n))*dim_square + dim_wall/2
        }
        for(i=0; i<wallsVert.count; i++){
            wallsVert.itemAt(i).x_m = (i%(maze_square_n+1))*dim_square + dim_wall/2
            wallsVert.itemAt(i).y_m = (Math.floor(i/(maze_square_n+1)))*dim_square + dim_square/2 + dim_wall/2
        }
        //draw numbers
        for(i=0; i<numbersHori.count; i++){
            numbersHori.itemAt(i).x_m = (i%maze_square_n)*dim_square + dim_square/2
            numbersHori.itemAt(i).y_m = maze_square_n*dim_square + maze_margin/2
        }
        for(i=0; i<numbersVert.count; i++){
            numbersVert.itemAt(i).x_m = maze_square_n*dim_square + maze_margin/2
            numbersVert.itemAt(i).y_m = (i%maze_square_n)*dim_square + dim_square/2
        }
        mouse_env.init()
    }

    function showMazeFromCpp(){
        if(!_maze.currMazeValid()){
            console.log("cannot show invalid maze")
            return
        }

        var x, y

        //horizontal (south)
        for(x = 0; x<_config._MAZE_N; x++){
            for(y = 0; y<_config._MAZE_N; y++){
                wallsHori.itemAt(y*_config._MAZE_N + x).existsReality =
                (_maze.getCellState(x, y) & _config._MASK_S)
            }
        }
        //horizontal highest layer (north)
        y = _config._MAZE_N-1
        for(x = 0; x<_config._MAZE_N; x++){
            wallsHori.itemAt((y+1)*_config._MAZE_N + x).existsReality =
            (_maze.getCellState(x, y) & _config._MASK_N)
        }

        //vertical (west)
        for(x = 0; x<_config._MAZE_N; x++){
            for(y = 0; y<_config._MAZE_N; y++){
                wallsVert.itemAt(y*(_config._MAZE_N+1) + x).existsReality =
                (_maze.getCellState(x, y) & _config._MASK_W)
            }
        }
        //vertical rightest column (east)
        x = _config._MAZE_N-1
        for(y = 0; y<_config._MAZE_N; y++){
            wallsVert.itemAt(y*(_config._MAZE_N+1) + x+1).existsReality =
            (_maze.getCellState(x, y) & _config._MASK_E)
        }
    }

    function changeWallVertical(index, setExist){
        var x, y, mask

        if(!((index+1)%(_config._MAZE_N+1))){
            x = _config._MAZE_N-1
            y = Math.floor(index/(_config._MAZE_N+1))
            mask = _config._MASK_E
        }
        else{
            x = index%(_config._MAZE_N+1)
            y = Math.floor(index/(_config._MAZE_N+1))
            mask = _config._MASK_W
        }
        if(setExist){ _maze.currMazeSetWall(x, y, mask) }
        else{ _maze.currMazeResetWall(x, y, mask) }
    }

    function changeWallHorizontal(index, setExist){
        var x, y, mask
        if(index > _config._MAZE_N*_config._MAZE_N-1){
            x = index%_config._MAZE_N
            y = _config._MAZE_N-1
            mask = _config._MASK_N
        }
        else{
            x = index%_config._MAZE_N
            y = Math.floor(index/_config._MAZE_N)
            mask = _config._MASK_S
        }
        if(setExist){ _maze.currMazeSetWall(x, y, mask) }
        else{ _maze.currMazeResetWall(x, y, mask) }
    }


    Component.onCompleted: {
        showMazeFromCpp()
        init()
    }

    Connections{
        target: _maze
        function onMazeChanged() {
            showMazeFromCpp()
        }

        function onMazeChangedMouseKnowledge(x, y, NSEW, exists) {
            var new_state = "y_ne"
            if(exists) { new_state = "y_e"}

            if(NSEW == _config._MASK_N)
            {
                wallsHori.itemAt((y+1)*(_config._MAZE_N) + x).state = new_state
            }
            else if(NSEW == _config._MASK_S)
            {
                wallsHori.itemAt(y*(_config._MAZE_N) + x).state = new_state
            }
            else if(NSEW == _config._MASK_E)
            {
                wallsVert.itemAt(y*(_config._MAZE_N+1) + x + 1).state = new_state
            }
            else if(NSEW == _config._MASK_W)
            {
                wallsVert.itemAt(y*(_config._MAZE_N+1) + x).state = new_state
            }
            else
            {
                console.error("Notify wall : invalid configuration", x, y, NSEW, exists)
            }
        }
    }


    Flickable{
        id: flick
        property double zoomFactor: slider_zoom.value

        anchors.centerIn: parent
        width: (parent.width < parent.height) ? parent.width : parent.height
        height: width
        contentWidth: width * zoomFactor
        contentHeight: contentWidth
        function printDebug(){
            console.log("flickable width:", width, " height:", height, " content width:", contentWidth)
            console.log("flickable parent width:", parent.width, " height:", parent.height)
            console.log("zomm ", zoomFactor)
        }
        onContentWidthChanged:
        {
            flick.contentY = flick.contentHeight - flick.height
        }

        Rectangle{
            id: mazeBackground
            color: "#101010"
            anchors.fill: parent

        Rectangle{
            id: scalingToRealDimensions
            color: "transparent"
            anchors.centerIn: parent
            width: maze_square_n * dim_square + 2*maze_margin
            height: width
            scale: parent.width / width

            Rectangle{
                id: mazeReal
                color: "#202020"
                x: maze_margin*pointPerM
                y: maze_margin*pointPerM
                width: (maze_square_n*dim_square + dim_wall) * pointPerM
                height: width

                Repeater{
                    id: wallsHori
                    model: _config._MAZE_N*(_config._MAZE_N+1)
                    Wall{
                        horizontal: true
                        x: getXmid(x_m, width)
                        y: getYmid(y_m, height)
                        property double x_m: 0
                        property double y_m: 0
                        existsReality: false
                        onChangedManually: {
                            changeWallHorizontal(index, existsReality)
                        }
                    }
                }

                Repeater{
                    id: wallsVert
                    model: _config._MAZE_N*(_config._MAZE_N+1)
                    Wall{
                        horizontal: false
                        x: getXmid(x_m, width)
                        y: getYmid(y_m, height)
                        property double x_m: 0
                        property double y_m: 0
                        existsReality: false
                        onChangedManually: {
                            changeWallVertical(index, existsReality)
                        }
                    }
                }


                Repeater{
                    id: sticks
                    model: (_config._MAZE_N+1)*(_config._MAZE_N+1)
                    Stick{
                        x: getX(x_m, width)
                        y: getY(y_m, height)
                        property double x_m: 0
                        property double y_m: 0
                    }
                }

                Repeater{
                    id: numbersHori
                    model: _config._MAZE_N
                    Text{
                        text: index.toString()
                        font.family: "Consolas"
                        color: text_col
                        x: getXmid(x_m, width)
                        y: getYmid(y_m, height)
                        property double x_m: 0
                        property double y_m: 0
                    }
                }

                Repeater{
                    id: numbersVert
                    model: _config._MAZE_N
                    Text{
                        text: index.toString()
                        font.family: "Consolas"
                        color: text_col
                        x: getXmid(x_m, width)
                        y: getYmid(y_m, height)
                        property double x_m: 0
                        property double y_m: 0
                    }
                }

                Mouse{
                    id: mouse_env
                    mouse_ctx: _m_env
                }

                Mouse{
                    id: mouse_program
                    mouse_ctx: _m_prog
                }

                Repeater{
                    id: pathText
                    model: _maze_texts.pathModel
                    Text{
                        text: modelData.text
                        font.family: "Consolas"
                        color: "magenta"
                        width: _config._MAZE_DIM_SQUARE*pointPerM
                        height: width
                        x: getXmid(modelData.x, width)
                        y: getYmid(modelData.y, height)
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment:   Text.AlignVCenter
                    }
                }
                Repeater{
                    id: cellsText
                    model: _maze_texts.cellTextsModel
                    Text{
                        text: modelData.text
                        font.family: "Consolas"
                        color: text_col
                        width: _config._MAZE_DIM_SQUARE*pointPerM
                        height: width
                        x: getXmid(modelData.x, width)
                        y: getYmid(modelData.y, height)
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment:   Text.AlignVCenter
                    }
                }
            }
        }
        }
    }

    Slider{
        id: slider_zoom
        width: parent.width*0.3
        anchors.bottom: parent.bottom
        anchors.right: parent.right

        from: 1
        to: 8
        value: 4
        snapMode: Slider.SnapOnRelease
        stepSize: (from-to)/100

        handle: Rectangle {
            x: parent.leftPadding + parent.visualPosition * (parent.availableWidth - width)
            y: parent.topPadding + parent.availableHeight / 2 - height / 2
            implicitWidth: 26
            implicitHeight: 26
            radius: 13
            color: slider_zoom.pressed ? buttonColorDown : buttonColorNormal
            border.color: buttonColorBorder
        }
    }
    Rectangle{
        id: mouse_position_rect
        width: mouse_position_label.implicitWidth
        height: mouse_position_label.implicitHeight
        visible: false
        color: "black"
        anchors.bottom: parent.bottom
        anchors.left: parent.left

        Text{
            id: mouse_position_label
            property double xx: 0
            property double yy: 0
            property int precision: 0
            text: Number(xx).toLocaleString(Qt.locale(), 'f', precision) + ", " +
            Number(yy).toLocaleString(Qt.locale(), 'f', precision)
            color: "#B0B0B0"
        }
    }
}
