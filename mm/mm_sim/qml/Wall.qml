import QtQuick 2.0

Item {
    id: wall_item
    readonly property string colorWhileChangable: "#200000FF"
    readonly property string colorWallSim: _m_env.mMeta.color
    readonly property string colorWallMouse: _m_prog.mMeta.color
    readonly property double dim_pick_margin: dim_wall*0.5

    property bool horizontal: true
    property bool existsReality: true
    property bool manually: false
    signal changedManually()

    states: [
        State {
            name: "n"
            PropertyChanges { target: wall; color: "#333333"}
        },
        State {
            name: "y_e"
            PropertyChanges { target: wall; color: colorWallMouse}
        },
        State {
            name: "y_ne"
            PropertyChanges { target: wall; color: "transparent"}
        }
    ]


    function resize(){
        if(horizontal){
            width= (dim_square) * pointPerM
            height = (dim_wall + dim_pick_margin*2) * pointPerM
            wall.width = (dim_square - dim_wall) * pointPerM
            wall.height = dim_wall * pointPerM
        }
        else{
            height= (dim_square) * pointPerM
            width = (dim_wall + dim_pick_margin*2) * pointPerM
            wall.height = (dim_square - dim_wall) * pointPerM
            wall.width = dim_wall * pointPerM
        }
    }

    Component.onCompleted: {
        resize()
        state = "n"
    }

    Rectangle{
        id: clickArea
        anchors.fill: parent
        color: changeWallToggle ? colorWhileChangable : "transparent"

        MouseArea{
            anchors.fill: parent
            hoverEnabled: true
            acceptedButtons: Qt.AllButtons
            propagateComposedEvents: true
            onClicked: {
                if(changeWallToggle){
                    existsReality = !existsReality
                    changedManually()
                }
            }
        }

        Rectangle{
            id: wall
            anchors.centerIn: parent
            border.width: 2
            border.color: existsReality ? colorWallSim : "transparent"
        }
    }

    Connections{
        target: maze
        function onPointPerMChanged(){
            resize()
        }
    }

    Connections{
        target: _maze
        function onMazeClearMouseKnowledge()
        {
            state = "n"
        }
    }
}

