import QtQuick 2.11
import QtQml 2.11
import QtQuick.Controls 2.4

Item {
    id: iMouse

    property string tx_color: "white"
    property double font_pointsize: 14

    Section{
        id: iMouseSection
        anchors.fill: parent
        name: "State"

        Column{
            leftPadding: 20
            spacing: 0
            topPadding: 40
            Row{
                Text{
                    color: _m_env.mMeta.color
                    font.pointSize: font_pointsize
                    font.family: "Consolas"
                    text: "        " + _m_env.mMeta.name
                }
                Text{
                    color: _m_prog.mMeta.color
                    font.pointSize: font_pointsize
                    font.family: "Consolas"
                    text: "        " + _m_prog.mMeta.name
                }
            }

            Repeater{
                model: [
                    ["x   ",           _m_env.state.x, _m_prog.state.x,    "mm"],
                    ["y   ",           _m_env.state.y, _m_prog.state.y,    "mm"],
                    ["t   ",           _m_env.state.t, _m_prog.state.t,    "rad"],
                    ["v   ",           _m_env.state.v, _m_prog.state.v,    "mm/s"],
                    ["\u03c9   ",      _m_env.state.w, _m_prog.state.w,    "rad/s"],
                    //left here maybe for future, may be deleted
                    //["\u03C9    ",      _infoprovider.envW,     "rad/s"],
                    //["mls  ",           _infoprovider.envMLS,   "m"],
                    //["ml\u03c9  ",      _infoprovider.envMLW,   "rad/s"],
                    //["mrs  ",           _infoprovider.envMRS,   "m"],
                    //["mr\u03c9  ",      _infoprovider.envMRW,   "rad/s"],
                ]
                delegate:
                Component{
                    Text{
                        color: tx_color
                        font.pointSize: font_pointsize
                        font.family: "Consolas"
                        property string name: modelData[0]
                        property string valueA: modelData[1]
                        property string valueB: modelData[2]
                        property string unit: modelData[3]
                        text:
                        name + ": " +
                        Number(valueA).toFixed(2) + "  " +
                        Number(valueB).toFixed(2) + "  " +
                        "[" + unit + "]"
                    }
                }
            }
        }
    }
}




