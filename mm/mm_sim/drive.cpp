#include <string>
#include <cmath>

#include "drive.h"
#include "motor.h"
#include "logger.h"

using namespace std;

Drive::Drive(std::string name, double radius, uint32_t ticks_per_turn)
{
    _motor = new Motor("DMOT");
    _encoder = new Encoder(ticks_per_turn);
    _radius = radius;
    _name = name;

    Logger::getInstance()->inf(QString::fromStdString(_name),
                               QString(" New Drive. radius: %1")
                                   .arg(_radius, 10, 'f', 6, ' '));
}

Drive::~Drive()
{
    delete _motor;
    delete _encoder;
}

double Drive::step(double time)
{
    double rotation = _motor->step(time);
    _encoder->update(rotation);
    double distance = rotation * _radius;
    // Logger::getInstance()->dbg(QString::fromStdString(_name), QString("distance: %1")
    //                                         .arg(distance, 10, 'f', 6, ' '));
    return distance;
}

void Drive::setTargetSpeed(double speed)
{
    _motor->setTargetSpeed(speed);
}

int32_t Drive::getTicks(void)
{
    return _encoder->get_ticks();
}

double Drive::getTotalDistance(void)
{
    return _motor->getTotalDistance() * _radius;
}

int Drive::setRadius(double radius)
{
    if(radius <= RADIUS_MIN || radius >= RADIUS_MAX)
    {
      Logger::getInstance()->err(QString::fromStdString(_name),
                               QString("Radius invalid. Should be in (%1, %2). Trying to set %3")
                                   .arg(RADIUS_MIN, 10, 'f', 3, ' ')
                                   .arg(RADIUS_MAX, 10, 'f', 3, ' ')
                                   .arg(radius, 10, 'f', 3, ' '));
      return -1;
    }
    Logger::getInstance()->inf(QString::fromStdString(_name),
                               "Setting radius to: " + QString::number(radius));
    _radius = radius;
    return 0;
}

double Drive::getRadius(void)
{
    return _radius;
}

int Drive::setTicksPerTurn(uint32_t ticks_per_turn)
{
    return _encoder->setTicksPerTurn(ticks_per_turn);
}

uint32_t Drive::getTicksPerTurn(void)
{
    return _encoder->getTicksPerTurn();
}

void Drive::setSpeed(double speed)
{
    _motor->setRotationVelocity(speed);
}