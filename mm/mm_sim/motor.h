#pragma once

#include <string>

const double MAX_SPEED = 1.0;

class Motor{
public:
    Motor(std::string name="", double T=0.038, double K=140);

    /**
     * @brief The most important function.
     * Calculates velocity of the motor given internal 'goal speed' and time
     *
     * @param time      in seconds
     * @return double   angular distance done in this step in radians
     */
    double step(double time=0.01);

    /**
     * @brief Get total angular distance the motor did
     *
     * @return double angular distance in radians
     */
    double getTotalDistance(void);

    double getRotationVelocity(void);
    void setRotationVelocity(double);
    void setTargetSpeed(double speed);
    double getMaxPossibleSpeed(void);

private:
    double _w;
    double _T;
    double _K;

    double _goal_throttle;

    double _total_distance; //in fact, distance here is in radians

    std::string _name;
};


struct MotorState_t{
    public:
    double sAll;        // distance driven. Decreases when driving back
    double sStep;       // distance driven in last step
    double a;           // current wheel position (alpha in rad) between 0 and 2PI
    double aStep;       // change of position in last step (may be bigger then 2PI)
    double w;           // current angular velocity
};

class MotorConfig{
private:
    double x_shift;   //dist from middle of mouse in m in x axis
    double y_shift;   //dist from middle of mouse in m in y axis
    double r;         //radius of the wheel in millimeters
    double w;         //width of the wheel in millimeters
    double ticksPerTurn;    //encoder resolution
    /**
        assume that motor is a first order system
        https://val-sagrario.github.io/Dynamics%20of%20First%20Order%20Systems%20for%20game%20devs%20-%20Jan%202020.pdf
        G(s) = k / (1+s*T)
        y = k * x - y' * T
        k is a gain, so maximal velocity of the motor
        t is time constant. If motor is still, and at time=0 desired velocity is set to 'k',
            then after 't' the velocity will be equal to 0.632*k. 0.632 is taken from (1-1/e)
        The pure analythical output of motor should be y(t) = k * (1 - e^(-t/T))*x(t)
        @see motor.py in scripts directory
    */
    double t;         //constant T
    double k;         //constant K
};
