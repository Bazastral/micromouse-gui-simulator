TEMPLATE = subdirs
CONFIG += ordered

SUBDIRS += \
    mm_sim \
    mm_sim_runner \
    mm_test

mm_sim_runner.depends = mm_sim
mm_test.depends = mm_sim