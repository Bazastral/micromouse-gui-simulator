#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import motor
import common


"""
There are two models:
diff = differential. Based on velocity of left and right wheel
unic = unicycle. Based on the linear and angular velocity of the robot
"""

class Mouse:
    def __init__(self,
            r=0.01,         # radius of the robot's wheel
            L=0.1,          # distance between two wheels [m]
            step_s=0.01,    # simulation step [s]
            ):
        self.step_s = step_s
        self.r = r
        self.L = L
        self.x = 0
        self.y = 0
        self.t = 0
        self.motorL = motor.Motor(step_time_s=step_s, k=100, t=0.1)
        self.motorR = motor.Motor(step_time_s=step_s, k=100, t=0.1)
        print("Mouse config: step_s {} seconds, r {} meters, l {} meters".format(
            self.step_s, self.r, self.L))

    """
    Defferential model of the robot
    u = [vleft, vright]'
    x = [x, y, theta]'
    """
    def step_differential(self, u):
        vl, vr = u
        vl = self.motorL.step(vl)
        vr = self.motorR.step(vr)
        # print(u, "{:7.3f} {:7.3f}".format(vl, vr))
        self.x += (self.r/2) * (vl + vr) * np.cos(self.t) * self.step_s
        self.y += (self.r/2) * (vl + vr) * np.sin(self.t) * self.step_s
        self.t += (self.r/self.L) * (vr - vl) * self.step_s

    """
    Unicycle model of the robot
    u = [velocity, omega]'
    x = [x, y, theta]'
    """
    def step_unicycle(self, u):
        u = [t/100 for t in u]
        self.step_differential(self.unic2diff(u))
        print(np.array(u), np.array(self.unic2diff(u)), np.array(self.state()), np.array(self.stateMotors()))
        # self.x += v * np.cos(self.t) * self.step_s
        # self.y += v * np.sin(self.t) * self.step_s
        # self.t += w * self.step_s

    def state(self):
        return [self.x, self.y, self.t]

    def stateMotors(self):
        return [self.motorL.velocity, self.motorR.velocity]

    """
    Convert
    u = [velocity, omega]'
    to
    u = [vleft, vright]'
    """
    def unic2diff(self, u):
        d = []
        v, w = u
        d.append((2 * v - w * self.L)/(2*self.r))  # v left
        d.append((2 * v + w * self.L)/(2*self.r))  # v right
        return d


    """
    Convert
    u = [vleft, vright]'
    to
    u = [velocity, omega]'
    """
    def diff2unic(self, d):
        u = []
        vl, vr = d
        u.append((self.r/2)*(vl + vr))
        u.append((self.r/self.L)*(vr - vl))
        return u


def drive_forward_left():
    mice = Mouse()
    traj = [mice.state()]
    for _ in range (10):
        mice.step_unicycle([1, 10])
        traj.append(mice.state())

    plt.plot([s[0] for s in traj], [s[1] for s in traj], marker='o', label="trajectory")
    plt.legend()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()

def turn_around():
    mice = Mouse()
    traj = [mice.state()]
    for _ in range (100):
        mice.step_unicycle([0.0, 1000])
        traj.append(mice.state())

    plt.plot([s[0] for s in traj], [s[1] for s in traj], marker='o', label="trajectory")
    plt.legend()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()

def forward():
    mice = Mouse()
    traj = [mice.state()]
    for _ in range (1000):
        mice.step_unicycle([1.0, 0])
        traj.append(mice.state())

    plt.plot([s[0] for s in traj], [s[1] for s in traj], marker='o', label="trajectory")
    plt.legend()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()




if __name__ == '__main__':
    common.setPrintOpts()
    forward()
    # drive_forward_left()
    turn_around()


