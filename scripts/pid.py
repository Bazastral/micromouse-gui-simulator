#!/bin/python3

import numpy as np

class Pid:
    def __init__(self, kp, ki, kd, step_s):
        self.elast = 0
        self.ei = 0
        self.kp = kp
        self.ki = ki
        self.kd = kd
        self.step_s = step_s

    def step(self, e):
        edot = (e - self.elast)/self.step_s
        self.ei = self.ei + e * self.step_s
        self.ei = np.maximum(self.ei, -10.0)
        self.ei = np.minimum(self.ei, 10.0)
        u = self.kp * e + self.ki * self.ei + self.kd * edot
        # print("PID e = {: 10.5f}   ei = {: 10.5f}   e^ = {: 10.5f}   u = {: 10.5f}    ".format(e, self.ei, edot, u))
        self.eold = e
        return u

