#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import motor
import pid
import mouse as mice
import common


def distance(a, b):
    return np.sqrt(np.power(a[0] - b[0], 2) + np.power(a[1] - b[1], 2))



class Controll:
    def __init__(self, step_s):
        self.step_s = step_s
        self.accuracy = 0.01
        self.pid = pid.Pid(kp=10, ki=0.1, kd=0.2, step_s=self.step_s)
        self.route = []

    def setRoute(self, route):
        self.route = route

    """
    aim = [x, y]
    curr_state = [x, y, t]
    """
    def to_point(self, aim, curr_state):
        posx, posy, post = curr_state
        destx, desty = aim

        theta = - post + np.arctan2(desty - posy, destx - posx)
        theta = common.normalize_rad(theta)
        theta = self.pid.step(theta)
        # theta = common.round_rad(theta)

        # speed = np.abs(1/(theta))/100
        speed = distance(aim, curr_state)
        speed = np.minimum(speed, 10.0)
        speed = np.maximum(speed, 0.01)
        u = [speed, theta]
        # print(np.array(u))
        status = "again"

        if distance([posx, posy], [destx, desty]) < self.accuracy:
            status = "done"
        return u, status


    """
    Drives through the path
    """
    def drive(self, curr_state):
        if len(self.route) == 0:
            return "done", [0,0]

        u, status = self.to_point(self.route[0], curr_state)
        if status == "done":
            print("target done ", self.route[0])
            self.route.pop(0)
        return "again", u



def main():
    # route = [
            # [1, -1],
            # [1, 1],
            # [-1, 1],
            # [-1, -1],
            # ]
    route = [[-1, 0], [-1,1]]
    step_s = 0.05
    maxIter = 10000
    mouse = mice.Mouse(step_s=step_s)
    ctrl = Controll(step_s=step_s)
    ctrl.setRoute(route.copy())
    traj = []

    status = "again"
    for _ in range(maxIter):
        status, u = ctrl.drive(mouse.state())
        mouse.step_unicycle(u)
        traj.append(mouse.state())
        if status == "done":
            break

    plt.plot([s[0] for s in traj], [s[1] for s in traj], marker='o', label="trajectory")
    plt.scatter([pos[0] for pos in route], [pos[1] for pos in route], marker='x', c="g", s=200, label="waypoints")
    plt.scatter(0, 0, c="r", marker='x', s=300, label="begin")
    plt.legend()
    plt.gca().set_aspect('equal', adjustable='box')
    plt.show()


def test():
    for _ in range(1000):
        r = np.random.rand()
        n = common.normalize_rad(r)
        if n > np.pi or n <= -np.pi:
            print("error", r, n)



if __name__ == '__main__':
    common.setPrintOpts()
    test()
    main()


