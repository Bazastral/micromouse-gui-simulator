#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt



def main():
    T = 0.1
    K = 2.0
    step = 0.01        # in seconds
    sim_time = 2      # in seconds

    ###########################
    leng = int(sim_time/step) + 1
    t = np.arange(leng)*step
    ya = np.zeros(leng)
    y = np.zeros(leng)
    y2 = np.zeros(leng)
    u = np.ones(leng)
    u[0:int(leng/3)] = 1
    u[int(leng/3):int(2*leng/3)] = 0
    u[int(2*leng/3):] = 1

    for i in range(leng):
        if i == 0:
            print("{:4d}: skip".format(i))
            continue
        y[i] = (T*y[i-1]/step + K*u[i]) / (1 + T/step)
        y2[i] = (T*y[i-1] + step*K*u[i]) / (step + T)

        ya[i] = K*u[i]*(1. - np.exp(-t[i]/T))
        print("{:4d}: t={:7.4f}, ya={:9.4f}, dyt={:9.4f}, y={:9.4f}, dY={:9.4f}"
              .format(i, t[i], ya[i], ya[i]-ya[i-1], y[i], y[i]-y[i-1]))
        print("{:4d}: y={:9.4f}, dY={:9.4f}"
              .format(i, y[i], y[i]-y[i-1]))

    plt.plot(t, ya, label="analitics")
    plt.plot(t, y, label="simulation")
    #plt.plot(t, y2, label="sim2")
    plt.plot(t, u, label="u")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
