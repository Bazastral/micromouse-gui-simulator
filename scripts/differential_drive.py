#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt

"""

There are two models:
diff = differential. Based on velocity of left and right wheel
unic = unicycle. Based on the linear and angular velocity of the robot
"""

cfg = {
    "r": 0.01,    # radius of the robot's wheel
    "L": 0.1,     # distance between two wheels
    "step": 0.1,  # simulation step in seconds
    "time": 200,    # simulation time in seconds
    "kp": 10,       # PID constant
    "ki": 1.1,  # PID constant
    "kd": 0.1,  # PID constant
    "accuracy": 0.1,  # how close robot should be near the target point
}


"""
Defferential model of the robot
u = [vleft, vright]'
x = [x, y, theta]'
"""
def kinematics_diff(x, u):
    xnext = x.copy()
    vl, vr = u
    xnext[0] += (cfg["r"]/2) * (vl + vr) * np.cos(x[2]) * cfg["step"]
    xnext[1] += (cfg["r"]/2) * (vl + vr) * np.sin(x[2]) * cfg["step"]
    xnext[2] += (cfg["r"]/cfg["L"]) * (vr - vl) * cfg["step"]
    return xnext


def normalize_rad(rad):
    while rad > np.pi:
        rad -= 2*np.pi
    while rad <= -np.pi:
        rad += 2*np.pi
    return rad

"""
Unicycle model of the robot
u = [velocity, omega]'
x = [x, y, theta]'
"""
def kinematics_unic(x, u):
    xnext = x.copy()
    v, w = u
    xnext[0] += v * np.cos(x[2]) * cfg["step"]
    xnext[1] += v * np.sin(x[2]) * cfg["step"]
    xnext[2] += w * cfg["step"]
    return xnext

"""
Convert
u = [velocity, omega]'
to
u = [vleft, vright]'
"""
def unic2diff(u):
    d = []
    v, w = u
    d.append((2 * v + w * cfg["L"])/(2*cfg["r"]))  # v left
    d.append((2 * v - w * cfg["L"])/(2*cfg["r"]))  # v right
    return d


"""
Convert
u = [vleft, vright]'
to
u = [velocity, omega]'
"""
def diff2unic(d):
    u = []
    vl, vr = d
    u.append((cfg["r"]/2)*(vl + vr))
    u.append((cfg["r"]/cfg["L"])*(vr - vl))
    return u


def pid(e):
    if "eold" not in pid.__dict__:
        pid.eold = e
    if "E" not in pid.__dict__:
        pid.E = 0
    edot = (e - pid.eold)/cfg["step"]
    pid.E = pid.E + e * cfg["step"]
    pid.E = np.maximum(pid.E, -10.0)
    pid.E = np.minimum(pid.E, 10.0)
    u = cfg["kp"] * e + cfg["ki"] * pid.E + cfg["kd"] * edot
    print("PID e = {: 10.5f}   E = {: 10.5f}   e^ = {: 10.5f}   u = {: 10.5f}    ".format(e, pid.E, edot, u))
    pid.eold = e
    return u


def distance(a, b):
    return np.sqrt(np.power(a[0] - b[0], 2) + np.power(a[1] - b[1], 2))


def controll(y, route):
    if "point" not in controll.__dict__:
        controll.point = 0
    if controll.point == -1:
        return [0, 0], "done"
    posx, posy, post = y
    destx, desty = route[controll.point]
    theta = - post + np.arctan2(desty - posy, destx - posx)
    theta = normalize_rad(theta)
    theta = pid(theta)
    speed = distance([destx, desty], y)
    speed = np.minimum(speed, 10.0)
    speed = np.maximum(speed, 0.01)
    u = [speed, theta]
    ret = "doing"
    if distance([posx, posy], [destx, desty]) < cfg["accuracy"]:
        print("controll next")
        controll.point += 1
        pid.__dict__.pop('eold', None)
        if len(route) <= controll.point:
            u = [0, 0]
            controll.point = -1
            print("controll done")
            ret = "done"
    return u, ret


# for now assume, that we have all we want
def output(x):
    return x


def main():
    time = np.arange(0, cfg["time"], cfg["step"])
    start = [0, 0, 0]
    route = [
        [1, 2],
        [1, 8],
        [-3, 3],
        [0, -1],
    ]

    x = start.copy()
    traj = []
    for _ in time[1:]:
        y = output(x)
        u, ret = controll(y, route)
        x = kinematics_unic(x, u)
        traj.append(x)
        if ret == "done":
            break

    plt.plot([s[0] for s in traj], [s[1] for s in traj], marker='o', label="trajectory")
    plt.scatter([pos[0] for pos in route], [pos[1] for pos in route], c="g", label="waypoints")
    plt.scatter(start[0], start[1], c="r", label="begin")
    plt.legend()
    plt.show()


def test():
    for _ in range(1000):
        r = np.random.rand()
        n = normalize_rad(r)
        if n > np.pi or n <= -np.pi:
            print("error", r, n)



if __name__ == '__main__':
    # test()
    main()


