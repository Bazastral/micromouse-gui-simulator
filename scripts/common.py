#!/bin/python3

import numpy as np

def setPrintOpts():
    np.set_printoptions( formatter={'all':lambda x: "{: 10.4f}".format(x)})


def normalize_rad(rad):
    while rad > np.pi:
        rad -= 2*np.pi
    while rad <= -np.pi:
        rad += 2*np.pi
    return rad

def round_rad(rad):
    if rad > np.pi:
        rad = np.pi
    if rad <= -np.pi:
        rad = -np.pi
    return rad


