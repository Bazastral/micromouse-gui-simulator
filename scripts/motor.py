#!/bin/python3

import numpy as np
import matplotlib.pyplot as plt




class Motor:
    def __init__(self, t=0.1, k=100.0, step_time_s=0.01):
        self.T = t
        self.K = k
        self.velocity = 0
        self.step_s = step_time_s
        print("config: T={:7.6f}, k={:9.6f}, step_s={:9.6f}"
                .format(self.T, self.K, self.step_s))

    def step(self, u=0):
        # assert u >= -1.0 and u <= 1.0, "set velocity must be within <-1, 0>"
        u = np.maximum(u, -1.0)
        u = np.minimum(u, 1.0)
        self.velocity = \
                (self.T*self.velocity/self.step_s + self.K*u) \
                / (1 + self.T/self.step_s)
        return self.velocity



def main():
    sim_time_s = 1.0
    step_s = 0.1
    motor = Motor(t=1, k = 1, step_time_s=step_s, )
    time = np.arange(step_s, sim_time_s+step_s, step_s)
    trajectory = []
    ya = []
    distances = []
    distance = 0


    for t in time:
        u = 1
        motor.step(u)
        trajectory.append(motor.velocity)

        ya.append(motor.K*u*(1. - np.exp(-t/motor.T)))
        distance += motor.velocity*step_s
        distances.append(distance)
        print("t={:7.6f}, vel analitics={:9.6f}, vel sim={:9.6f}, sim vel={:9.6f}"
                .format(t, ya[-1], trajectory[-1], distance))

    plt.plot(time, ya, label="vel analitics")
    plt.plot(time, trajectory, label="vel simulation")
    plt.plot(time, distances, label="dist simulation")
    plt.legend()
    plt.show()


if __name__ == '__main__':
    main()
